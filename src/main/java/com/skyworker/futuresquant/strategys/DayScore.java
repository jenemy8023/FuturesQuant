package com.skyworker.futuresquant.strategys;

import cn.hutool.core.date.DateTime;
import cn.hutool.db.Entity;
import cn.hutool.db.handler.EntityListHandler;
import cn.hutool.db.sql.SqlExecutor;
import cn.hutool.log.Log;
import com.skyworker.futuresquant.quant.GetFinanceData;
import com.skyworker.futuresquant.utils.QuantUtil;
import com.skyworker.futuresquant.utils.SingletonConn;
import com.skyworker.futuresquant.utils.SingletonLog;
import com.tictactec.ta.lib.Core;
import com.tictactec.ta.lib.MInteger;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class DayScore {
    private static final Log log = SingletonLog.getInstance().log;
    private static final Connection conn = SingletonConn.getInstance().conn;
    private DateTime tradeDate;

    public DayScore(DateTime _tradeDate) {
        tradeDate = _tradeDate;
    }

    public void runDayScore() {
        String sDate = tradeDate.toString("yyyy-MM-dd");
        GetFinanceData getFinanceData = new GetFinanceData();
        try {
            List<Entity> dayscore = SqlExecutor.query(conn, "select * from t_futures_dayscore where date='"
                    + sDate + "'", new EntityListHandler());
            if (dayscore.size() == 0) {
                List<Entity> futures = SqlExecutor.query(conn, "select * from t_futures_varieties", new EntityListHandler());
                Core core = new Core();
                for (Entity future : futures) {
                    String sql = "select tradeday,dominant_code from t_sys_dominantchange where code='"
                            + future.getStr("code") + "' and tradeday<='" + tradeDate + "' order by tradeday desc limit 1";
                    List<Entity> dominantchange = SqlExecutor.query(conn, sql, new EntityListHandler());
                    String dominant_code = dominantchange.get(0).getStr("dominant_code");
                    sql = "select * from t_sys_tradeday where trade_day<'" + tradeDate + "' order by trade_day desc limit 16";
                    List<Entity> tradedays = SqlExecutor.query(conn, sql, new EntityListHandler());
                    String startday = tradedays.get(tradedays.size() - 1).getStr("trade_day");
                    ArrayList<HashMap<String, String>> price = getFinanceData.GetPricePeriod(dominant_code, "1d", startday, sDate);
                    double[] close = QuantUtil.getDataList(price, "close");
                    double[] open = QuantUtil.getDataList(price, "open");
                    double[] ma5 = new double[17];
                    double[] ma10 = new double[17];
                    double[] ma15 = new double[17];
                    core.sma(0, 16, close, 5, new MInteger(), new MInteger(), ma5);
                    core.sma(0, 16, close, 10, new MInteger(), new MInteger(), ma10);
                    core.sma(0, 16, close, 15, new MInteger(), new MInteger(), ma15);

                    int score = 0;
                    if (ma5[0] > ma5[1] & ma5[1] > ma5[2]) {
                        score = 1;
                        if (ma5[0] > ma10[0]) score++;
                        if (ma5[0] > ma10[0] & ma10[0] > ma15[0]) score++;
                        if (ma10[0] > ma10[1] & ma10[1] > ma10[2]) score++;
                        if (ma15[0] > ma15[1] & ma15[1] > ma15[2]) score++;
                        if (ma15[0] > ma15[1] & ma15[1] > ma15[2]) score++;
                        if (close[0] > ma5[0] & open[0] > ma5[0]) score++;
                        if (close[0] > open[0] & close[1] > open[1]) score++;
                        if (close[0] > open[0] & close[1] > open[1] & close[2] > open[2]) score++;
                    }
                    if (ma5[0] < ma5[1] & ma5[1] < ma5[2]) {
                        score = -1;
                        if (ma5[0] < ma10[0]) score--;
                        if (ma5[0] < ma10[0] & ma10[0] < ma15[0]) score--;
                        if (ma10[0] < ma10[1] & ma10[1] < ma10[2]) score--;
                        if (ma15[0] < ma15[1] & ma15[1] < ma15[2]) score--;
                        if (ma15[0] < ma15[1] & ma15[1] < ma15[2]) score--;
                        if (close[0] < ma5[0] & open[0] < ma5[0]) score--;
                        if (close[0] < open[0] & close[1] < open[1]) score--;
                        if (close[0] < open[0] & close[1] < open[1] & close[2] < open[2]) score--;
                    }
                    sql = "insert into t_futures_dayscore values('" + tradeDate + "','" + dominant_code + "'," + score + ")";
                    SqlExecutor.execute(conn, sql);
                }
            }
        } catch (SQLException e) {
            log.error(e);
        }
    }
}
