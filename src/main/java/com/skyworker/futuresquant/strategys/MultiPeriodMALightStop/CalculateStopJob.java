package com.skyworker.futuresquant.strategys.MultiPeriodMALightStop;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.date.DateUtil;
import cn.hutool.db.Entity;
import cn.hutool.db.handler.EntityListHandler;
import cn.hutool.db.sql.SqlExecutor;
import cn.hutool.log.Log;
import cn.hutool.setting.Setting;
import com.skyworker.futuresquant.quant.GetFinanceData;
import com.skyworker.futuresquant.utils.ExcelUtil;
import com.skyworker.futuresquant.utils.QuantUtil;
import com.skyworker.futuresquant.utils.SingletonConn;
import com.skyworker.futuresquant.utils.SingletonLog;
import com.tictactec.ta.lib.Core;
import com.tictactec.ta.lib.MInteger;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class CalculateStopJob {
    private static final Connection conn = SingletonConn.getInstance().conn;
    String tradeDateTime;
    int SimulateId;
    private Log log = SingletonLog.getInstance().log;

    public CalculateStopJob(String _tradeDateTime, int _SimulateId) {
        tradeDateTime = _tradeDateTime;
        SimulateId = _SimulateId;
    }

    public void runCalculateStopJob() {
//        ExecutorService singleThreadExecutor = Executors.newSingleThreadExecutor();
//        singleThreadExecutor.submit(() -> {
        log.info("启动止损线计算任务");
        CalculateStop();
//        });
    }

    private void CalculateStop() {
        try {
            List<Entity> positions = SqlExecutor.query(conn, "select * from t_simulate_position where simulate_id = " + SimulateId, new EntityListHandler());
            List<Entity> afstops = SqlExecutor.query(conn, "select * from t_simulate_afstop where simulate_id = " + SimulateId, new EntityListHandler());
            GetFinanceData getFinanceData = new GetFinanceData();
            String LastTradeDay = getFinanceData.getLastTradeDay(DateUtil.parseDateTime(tradeDateTime));
            List<List<String>> writeDate = new ArrayList<>();
            Setting setting = new Setting("system.setting");
            double AFStart = Double.valueOf(setting.getByGroup("AFStart", "AFSetting"));
            double AFStep = Double.valueOf(setting.getByGroup("AFStep", "AFSetting"));
            double AFLimit = Double.valueOf(setting.getByGroup("AFLimit", "AFSetting"));

            for (Entity position : positions) {
                String dominant_code = position.getStr("dominant_code");
                Boolean hasStop = false;
                double floatProfit = 0;
                double stop = 0;
                double peak = 0;
                ArrayList<HashMap<String, String>> Price = getFinanceData.GetPricePeriod(dominant_code, 15, "5m", LastTradeDay, tradeDateTime);
                double[] close = QuantUtil.getDataList(Price, "close");
                double[] high = QuantUtil.getDataList(Price, "high");
                double[] low = QuantUtil.getDataList(Price, "low");
                double[] atr = new double[15];
                Core core = new Core();
                core.atr(0, 14, high, low, close, 14, new MInteger(), new MInteger(), atr);

                for (Entity afstop : afstops) {
                    if (afstop.getStr("dominant_code").equals(dominant_code)) {
                        hasStop = true;
                        //更新AF
                        peak = afstop.getDouble("peak");
                        stop = afstop.getDouble("stop");
                        if (position.getStr("type").equals("BK")) {
                            double currentHigh = new Double(Price.get(0).get("high"));
                            if (currentHigh > peak) {
                                peak = currentHigh;
                            }
                            stop = peak - atr[0] * 2;

                            floatProfit = (Double.valueOf(Price.get(0).get("close")) - position.getDouble("price")) * position.getDouble("amount") * position.getDouble("lot_amount");
                        } else {
                            double currentLow = new Double(Price.get(0).get("low"));
                            if (currentLow < peak) {
                                peak = currentLow;
                            }
                            stop = peak + 2 * atr[0];

                            floatProfit = (position.getDouble("price") - Double.valueOf(Price.get(0).get("close"))) * position.getDouble("amount") * position.getDouble("lot_amount");
                        }
                        String sql = "update t_simulate_afstop set af=0,stop=" + stop + ",peak=" + peak + " where simulate_id = " + SimulateId + " and dominant_code='" + dominant_code + "'";
                        SqlExecutor.execute(conn, sql);
                        sql = "update t_simulate_position set float_profit=" + floatProfit + " where id=" + position.getStr("id");
                        SqlExecutor.execute(conn, sql);
                        break;
                    }
                }

                if (!hasStop) {
                    //新增AF
                    if (position.getStr("type").equals("BK")) {
                        stop = low[0] - atr[0];
                        peak = low[0];
                        floatProfit = (close[0] - position.getDouble("price")) * position.getDouble("amount") * position.getDouble("lot_amount");
                    } else {
                        stop = high[0] + atr[0];
                        peak = high[0];
                        floatProfit = (position.getDouble("price") - close[0]) * position.getDouble("amount") * position.getDouble("lot_amount");
                    }
                    String sql = "insert into t_simulate_afstop values(null," + SimulateId + ",'" + dominant_code + "'," + AFStart + "," + stop + "," + peak + ",1)";
                    SqlExecutor.execute(conn, sql);
                    sql = "update t_simulate_position set float_profit=" + floatProfit + " where id=" + position.getStr("id");
                    SqlExecutor.execute(conn, sql);
                }

                List<String> writeDateItem = new ArrayList<>();
                writeDateItem.add(tradeDateTime);
                writeDateItem.add(position.getStr("dominant_code"));
                writeDateItem.add(position.getStr("type"));
                writeDateItem.add(position.getStr("amount"));
                writeDateItem.add(position.getStr("price"));
                writeDateItem.add(position.getStr("lot_amount"));
                writeDateItem.add(position.getStr("margin"));
                writeDateItem.add(String.valueOf(floatProfit));
                writeDateItem.add(String.valueOf(stop));
                writeDateItem.add(String.valueOf(peak));
                writeDateItem.add(String.valueOf(atr[0]));
                writeDateItem.add(Price.get(0).get("open"));
                writeDate.add(writeDateItem);
            }
            try {
                writeProfit(writeDate);
            } catch (IOException e) {
                log.error(e);
            }
        } catch (SQLException e) {
            log.error(e);
        }

    }

    private void writeProfit(List<List<String>> writeDate) throws IOException {
        String tradeDate = DateUtil.parseDateTime(tradeDateTime).toString("yyyy-MM-dd");
        String FileName = DateUtil.today() + "profit" + SimulateId + ".xlsx";
        String Path = "c:\\logs\\profit\\" + FileName;
        File file = new File(Path);
        ExcelUtil excelUtil;
        if (!file.exists()) {
            excelUtil = new ExcelUtil("xlsx");
            excelUtil.createSheet();
            List<String> ls = CollUtil.newArrayList("日期", "编码", "持仓类型", "持仓手数", "持仓价格", "每手数量", "占用保证金", "浮动盈亏", "止损价格", "峰值", "ATR", "当前价格");
            excelUtil.setRowValue(0, ls, 0);
        } else {
            excelUtil = new ExcelUtil(file);
        }
        if (writeDate.size() > 0)
            excelUtil.write(0, writeDate, true);
        excelUtil.saveExcel(Path);


    }
}
