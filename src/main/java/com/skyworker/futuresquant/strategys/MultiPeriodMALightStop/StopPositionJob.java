package com.skyworker.futuresquant.strategys.MultiPeriodMALightStop;

import cn.hutool.core.date.DateField;
import cn.hutool.core.date.DateTime;
import cn.hutool.core.date.DateUtil;
import cn.hutool.db.Entity;
import cn.hutool.db.handler.EntityListHandler;
import cn.hutool.db.sql.SqlExecutor;
import cn.hutool.log.Log;
import com.skyworker.futuresquant.quant.GetFinanceData;
import com.skyworker.futuresquant.quant.SimulateTrade;
import com.skyworker.futuresquant.utils.SingletonConn;
import com.skyworker.futuresquant.utils.SingletonLog;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static com.skyworker.futuresquant.utils.QuantUtil.isTradeTime;

public class StopPositionJob {
    private static final Log log = SingletonLog.getInstance().log;
    private static final Connection conn = SingletonConn.getInstance().conn;
    String tradeDateTime;
    int SimulateId;
    SimulateTrade simulateTrade;

    public StopPositionJob(String _tradeDateTime, int _SimulateId) {
        tradeDateTime = _tradeDateTime;
        SimulateId = _SimulateId;
    }

    public ArrayList<HashMap<String, String>> runStopPositionJob() {
//        ExecutorService singleThreadExecutor = Executors.newSingleThreadExecutor();
//        singleThreadExecutor.submit(() -> {
        log.info("启动平仓任务");
        return StopPosition();
//        });
    }

    private ArrayList<HashMap<String, String>> StopPosition() {
        GetFinanceData getFinanceData = new GetFinanceData();
        simulateTrade = new SimulateTrade(SimulateId, tradeDateTime);
        String LastTradeDay = getFinanceData.getLastTradeDay(DateUtil.parseDateTime(tradeDateTime));
        ArrayList<HashMap<String, String>> StopList = new ArrayList<>();
        try {
            List<Entity> positions = SqlExecutor.query(conn, "select * from t_simulate_position where simulate_id = " + SimulateId, new EntityListHandler());
            for (Entity position : positions) {
                String dominant_code = position.getStr("dominant_code");
                //如果当前品种不在交易时间范围内，则不考虑平仓
                if (!isTradeTime(DateUtil.parseDateTime(tradeDateTime), dominant_code))
                    continue;

                List<Entity> afstop = SqlExecutor.query(conn, "select * from t_simulate_afstop where simulate_id = " + SimulateId + " and dominant_code='" + dominant_code + "'", new EntityListHandler());
                ArrayList<HashMap<String, String>> Price = getFinanceData.GetPricePeriod(dominant_code, 1, "5m", LastTradeDay, tradeDateTime);
                double close = new Double(Price.get(0).get("close"));
                if (position.getStr("type").equals("BK")) {
                    if (close < afstop.get(0).getDouble("stop")) {
                        HashMap<String, String> hm = new HashMap<>();
                        hm.put("code", dominant_code);
                        hm.put("price", String.valueOf(close));
                        hm.put("type", "SP");
                        hm.put("isYest", position.getStr("isYest"));
                        hm.put("vol", position.getStr("amount"));
                        StopList.add(hm);
                        simulateTrade.ClosePosition(dominant_code);
                        SqlExecutor.execute(conn, "delete from t_simulate_afstop where id=" + afstop.get(0).getStr("id"));
                        makeBreaker(dominant_code);
                        log.info("平仓：" + dominant_code);
                    }
                } else {
                    if (close > afstop.get(0).getDouble("stop")) {
                        HashMap<String, String> hm = new HashMap<>();
                        hm.put("code", dominant_code);
                        hm.put("price", String.valueOf(close));
                        hm.put("type", "BP");
                        hm.put("isYest", position.getStr("isYest"));
                        hm.put("vol", position.getStr("amount"));
                        StopList.add(hm);
                        simulateTrade.ClosePosition(dominant_code);
                        SqlExecutor.execute(conn, "delete from t_simulate_afstop where id=" + afstop.get(0).getStr("id"));
                        makeBreaker(dominant_code);
                        log.info("平仓：" + dominant_code);
                    }
                }
            }
        } catch (SQLException e) {
            log.error(e);
        }
        return StopList;
    }

    private void makeBreaker(String dominant_code) {
        try {
            //这里的熔断时间逻辑是有问题的，不能保证真的熔断，以后改
            //另外，反手应该是允许的，但是这里也没有做反手判断。不过在我目前的策略里，一般不会出现反手，先不考虑。
            DateTime dtBreak = DateUtil.parseDateTime(tradeDateTime).offset(DateField.HOUR, 1);
            SqlExecutor.execute(conn, "insert into t_simulate_breaker values(null," + SimulateId + ",'"
                    + dominant_code + "','closestop','" + tradeDateTime + "','" + dtBreak.toString() + "')");
        } catch (SQLException e) {
            log.error(e);
        }
    }
}
