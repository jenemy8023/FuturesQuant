package com.skyworker.futuresquant.strategys.MultiPeriodMA;

import cn.hutool.core.date.DateUtil;
import cn.hutool.db.Entity;
import cn.hutool.db.handler.EntityListHandler;
import cn.hutool.db.sql.SqlExecutor;
import cn.hutool.log.Log;
import com.skyworker.futuresquant.quant.GetFinanceData;
import com.skyworker.futuresquant.quant.SimulateTrade;
import com.skyworker.futuresquant.utils.QuantUtil;
import com.skyworker.futuresquant.utils.SingletonConn;
import com.skyworker.futuresquant.utils.SingletonLog;
import com.tictactec.ta.lib.Core;
import com.tictactec.ta.lib.MInteger;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.*;

import static com.skyworker.futuresquant.utils.QuantUtil.MaxMin;
import static com.skyworker.futuresquant.utils.QuantUtil.isTradeTime;

public class OpenPositionJob {
    private static final Log log = SingletonLog.getInstance().log;
    private static final Connection conn = SingletonConn.getInstance().conn;
    String tradeDateTime;
    int SimulateId;
    private int BuyLimit = 5;
    private ArrayList<HashMap<String, String>> SelectUnit;

    public OpenPositionJob(String _tradeDateTime, int _SimulateId, ArrayList<HashMap<String, String>> _SelectUnit) {
        tradeDateTime = _tradeDateTime;
        SimulateId = _SimulateId;
        SelectUnit = _SelectUnit;
    }

    public void runOpenPositionJob() {
//        ExecutorService singleThreadExecutor = Executors.newSingleThreadExecutor();
//        singleThreadExecutor.submit(() -> {
        log.info("启动开仓任务");
        log.info("获取所有当前可交易的品种");
        getAllTradeVarieties();
        log.info("跳空高低开暂停开仓处理");
        //BreakerForJump();
//        });
    }

    private void getAllTradeVarieties() {
        try {
            List<Entity> dominants = SqlExecutor.query(conn, "select * from t_futures_dominant where is_trade = 1 and simulate_id = " + SimulateId, new EntityListHandler());
            List<Entity> positions = SqlExecutor.query(conn, "select * from t_simulate_position where simulate_id = " + SimulateId, new EntityListHandler());
            BuyLimit = BuyLimit - positions.size();
            if (BuyLimit == 0) {
                log.info(tradeDateTime + "已达最大持仓数量，无法购买");
                return;
            }
            //可交易品种中去除已持仓品种
            Iterator<Entity> tradeVarieties = dominants.iterator();
            while (tradeVarieties.hasNext()) {
                Entity dominant = tradeVarieties.next();
                for (Entity position : positions)
                    if (dominant.getStr("dominant_code").equals(position.getStr("dominant_code"))) {
                        tradeVarieties.remove();
                        break;
                    }
            }

            //可交易品种中除去熔断中的交易品种，去除不在交易时间的品种
            tradeVarieties = dominants.iterator();
            List<Entity> breakers = SqlExecutor.query(conn, "select * from t_simulate_breaker where simulate_id = "
                    + SimulateId + " and breaker_to>'" + tradeDateTime + "'", new EntityListHandler());
            while (tradeVarieties.hasNext()) {
                Entity dominant = tradeVarieties.next();
                if (!isTradeTime(DateUtil.parseDateTime(tradeDateTime), dominant.getStr("dominant_code"))) {
                    tradeVarieties.remove();
                    continue;
                }
                for (Entity breaker : breakers)
                    if (dominant.getStr("dominant_code").equals(breaker.getStr("dominant_code"))) {
                        tradeVarieties.remove();
                        break;
                    }
            }


            //5分钟线 多头或者空头排列，否则不在交易品种列表中。
            ArrayList<HashMap<String, String>> tradeList = new ArrayList<>();
            GetFinanceData getFinanceData = new GetFinanceData();
            Core core = new Core();
            String LastTradeDay = tradeDateTime;
            LastTradeDay = getFinanceData.getTradeDayOffset(DateUtil.parseDate(tradeDateTime), Integer.valueOf(SelectUnit.get(0).get("offset")));
            for (Entity entity : dominants) {
                String dominant_code = entity.getStr("dominant_code");
                ArrayList<HashMap<String, String>> Price = getFinanceData.GetPricePeriod(dominant_code, 21, SelectUnit.get(0).get("unit"), LastTradeDay, tradeDateTime);
                double[] close = QuantUtil.getDataList(Price, "close");
                double[] open = QuantUtil.getDataList(Price, "open");

                double[] ma5 = new double[20];
                double[] ma10 = new double[20];
                double[] ma20 = new double[20];
                core.sma(0, 19, close, 5, new MInteger(), new MInteger(), ma5);
                core.sma(0, 19, close, 10, new MInteger(), new MInteger(), ma10);
                core.sma(0, 19, close, 20, new MInteger(), new MInteger(), ma20);

                double[] close10 = Arrays.copyOfRange(close, 1, 11);
                double[] open10 = Arrays.copyOfRange(open, 1, 11);
                double[] closeMaxMin = MaxMin(close10);
                double[] openMaxMin = MaxMin(open10);

                if (ma5[0] > ma10[0] & ma10[0] > ma20[0]) {
                    log.warn("UpArray:" + tradeDateTime + "-" + dominant_code);
                    if (close[0] > closeMaxMin[0] & close[0] > openMaxMin[0]) {
                        HashMap<String, String> hm = new HashMap<>();
                        hm.put("dominant_code", dominant_code);
                        hm.put("price", entity.getStr("price"));
                        hm.put("type", "BK");
                        hm.put("score", "0");
                        tradeList.add(hm);
                    }
                } else if (ma5[0] < ma10[0] & ma10[0] < ma20[0]) {
                    log.warn("DownArray:" + tradeDateTime + "-" + dominant_code);
                    if (close[0] < closeMaxMin[1] & close[0] > openMaxMin[1]) {
                        HashMap<String, String> hm = new HashMap<>();
                        hm.put("dominant_code", dominant_code);
                        hm.put("price", entity.getStr("price"));
                        hm.put("type", "SK");
                        hm.put("score", "0");
                        tradeList.add(hm);
                    }
                }
            }

            //日线判断过滤
            LastTradeDay = getFinanceData.getTradeDayOffset(DateUtil.parseDate(tradeDateTime), Integer.valueOf(SelectUnit.get(2).get("offset")));
            for (int i = 0; i < tradeList.size(); i++) {
                HashMap<String, String> hm = tradeList.get(i);
                int score = Integer.valueOf(hm.get("score"));
                if (score >= 0) {
                    String dominant_code = hm.get("dominant_code");
                    ArrayList<HashMap<String, String>> Price = getFinanceData.GetPricePeriod(dominant_code, 21, SelectUnit.get(2).get("unit"), LastTradeDay, tradeDateTime);
                    double[] close = QuantUtil.getDataList(Price, "close");
                    double[] ma5 = new double[20];
                    double[] ma10 = new double[20];
                    double[] ma20 = new double[20];
                    core.sma(0, 19, close, 5, new MInteger(), new MInteger(), ma5);
                    core.sma(0, 19, close, 10, new MInteger(), new MInteger(), ma10);
                    core.sma(0, 19, close, 20, new MInteger(), new MInteger(), ma20);
                    if (hm.get("type").equals("BK")) {
                        if (ma5[0] > ma5[1] & ma5[1] > ma5[2]) score = score + 1;
                        if (ma5[0] > ma10[0]) score = score + 1;
                        if (ma5[0] > ma10[0] & ma10[0] > ma20[0]) score = score + 1;
                        if (ma5[0] < ma5[1] & ma5[1] < ma5[2]) score = -1;
                        if (ma5[0] < ma10[0]) score = -1;
                        if (ma5[0] < ma10[0] & ma10[0] < ma20[0]) score = -1;

                    } else {
                        if (ma5[0] < ma5[1] & ma5[1] < ma5[2]) score = score + 1;
                        if (ma5[0] < ma10[0]) score = score + 1;
                        if (ma5[0] < ma10[0] & ma10[0] < ma20[0]) score = score + 1;
                        if (ma5[0] > ma5[1] & ma5[1] > ma5[2]) score = -1;
                        if (ma5[0] > ma10[0]) score = -1;
                        if (ma5[0] > ma10[0] & ma10[0] > ma20[0]) score = -1;
                    }
                }
                hm.put("score", String.valueOf(score));
                tradeList.set(i, hm);
            }

            //30分钟线判断过滤
            LastTradeDay = getFinanceData.getTradeDayOffset(DateUtil.parseDate(tradeDateTime), Integer.valueOf(SelectUnit.get(1).get("offset")));
            for (int i = 0; i < tradeList.size(); i++) {
                HashMap<String, String> hm = tradeList.get(i);
                int score = Integer.valueOf(hm.get("score"));
                if (score >= 0) {
                    String dominant_code = hm.get("dominant_code");
                    ArrayList<HashMap<String, String>> Price = getFinanceData.GetPricePeriod(dominant_code, 21, SelectUnit.get(1).get("unit"), LastTradeDay, tradeDateTime);
                    double[] close = QuantUtil.getDataList(Price, "close");
                    double[] ma5 = new double[20];
                    double[] ma10 = new double[20];
                    double[] ma20 = new double[20];
                    core.sma(0, 19, close, 5, new MInteger(), new MInteger(), ma5);
                    core.sma(0, 19, close, 10, new MInteger(), new MInteger(), ma10);
                    core.sma(0, 19, close, 20, new MInteger(), new MInteger(), ma20);
                    if (hm.get("type").equals("BK")) {
                        if (ma5[0] > ma5[1] & ma5[1] > ma5[2]) score = score + 1;
                        if (ma5[0] > ma10[0]) score = score + 1;
                        if (ma5[0] > ma10[0] & ma10[0] > ma20[0]) score = score + 1;
                        if (ma5[0] < ma5[1] & ma5[1] < ma5[2]) score = -1;
                        if (ma5[0] < ma10[0]) score = -1;
                        if (ma5[0] < ma10[0] & ma10[0] < ma20[0]) score = -1;

                    } else {
                        if (ma5[0] < ma5[1] & ma5[1] < ma5[2]) score = score + 1;
                        if (ma5[0] < ma10[0]) score = score + 1;
                        if (ma5[0] < ma10[0] & ma10[0] < ma20[0]) score = score + 1;
                        if (ma5[0] > ma5[1] & ma5[1] > ma5[2]) score = -1;
                        if (ma5[0] > ma10[0]) score = -1;
                        if (ma5[0] > ma10[0] & ma10[0] > ma20[0]) score = -1;
                    }
                }
                hm.put("score", String.valueOf(score));
                tradeList.set(i, hm);
            }

            //清理可交易产品列表
            Iterator<HashMap<String, String>> tradeLists = tradeList.iterator();
            while (tradeLists.hasNext()) {
                HashMap<String, String> tradeItem = tradeLists.next();
                int score = Integer.valueOf(tradeItem.get("score"));
                if (score < 0) {
                    tradeLists.remove();
                }
            }

            if (tradeList.size() == 0)
                return;

            if (tradeList.size() > BuyLimit) {
                Collections.sort(tradeList, new ScoreComparator());
                tradeList = (ArrayList<HashMap<String, String>>) tradeList.subList(0, BuyLimit - 1);
            }

            //循环执行买入操作
            SimulateTrade simulateTrade = new SimulateTrade(SimulateId, tradeDateTime);
            for (HashMap<String, String> hm : tradeList) {
                String dominant_code = hm.get("dominant_code");
                String type = hm.get("type");
                BigDecimal amount = BigDecimal.valueOf(0);
                if (Double.valueOf(hm.get("price")) > 10000) {
                    amount = BigDecimal.valueOf(1);
                } else if (Double.valueOf(hm.get("price")) > 5000) {
                    amount = BigDecimal.valueOf(2);
                } else if (Double.valueOf(hm.get("price")) < 5000) {
                    amount = BigDecimal.valueOf(4);
                }
                log.info(tradeDateTime + ": 买入：" + dominant_code + " 买入数量：" + amount);
                simulateTrade.OpenPosition(dominant_code, type, amount);
            }
        } catch (
                SQLException e) {
            log.error(e);
        }

    }

    static class ScoreComparator implements Comparator {
        public int compare(Object object1, Object object2) {// 实现接口中的方法
            HashMap<String, String> hm1 = (HashMap<String, String>) object1;
            HashMap<String, String> hm2 = (HashMap<String, String>) object1;
            return new Integer(hm1.get("score")).compareTo(new Integer(hm2.get("score")));
        }
    }

//   1、循环获取交易中的品种，
//   2、去除所有已持仓的品种。
//   3、去除所有处于熔断期的品种（同时清理已过期的熔断记录）
//   4、判断5分钟线的走势打标签：
//      ma5，ma10，ma15，空头或多头排列时，打上标签：卖开或买开。
//   5、判断日线的走势并打分：
//      日线的MA5连续2天上升，或者下降时，打分：买入1分/卖出1分。
//      日线的MA5在MA10的上方或下方时，分数加1分。如果方向错误，则清理出列表
//      日线的MA5，MA10，MA15呈多头或空头排列，分数加1分，如果方向错误，则清理出列表
//   6、判断60分钟的走势并打分：
//      60分钟同上打分处理
//   7、取分数大于4分的品种，如果品种数量小于可买入品种，全部买入。
//      如果品种数量大于可买入品种，则按成交量排序，取可买入的数量。
//      可买入品种：10-持仓品种数。

}
