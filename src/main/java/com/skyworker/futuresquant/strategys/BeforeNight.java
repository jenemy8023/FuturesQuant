package com.skyworker.futuresquant.strategys;

import cn.hutool.core.date.DateTime;
import cn.hutool.db.handler.EntityListHandler;
import cn.hutool.db.sql.SqlExecutor;
import cn.hutool.log.Log;
import com.skyworker.futuresquant.utils.SingletonConn;
import com.skyworker.futuresquant.utils.SingletonLog;

import java.sql.Connection;
import java.sql.SQLException;

public class BeforeNight {
    private DateTime tradeDate;
    private int SimulateId;
    private int StrategyId;
    private static final Connection conn = SingletonConn.getInstance().conn;
    private static final Log log = SingletonLog.getInstance().log;

    public BeforeNight(DateTime _tradeDate, int _SimulateId, int _StrategyId) {
        tradeDate = _tradeDate;
        SimulateId = _SimulateId;
        StrategyId = _StrategyId;
    }

    public void runBeforeNight() {
        //更新上期所期货为平昨
        try {
            SqlExecutor.execute(conn, "update t_simulate_position set isYest=1 where simulate_id=" + SimulateId);
        } catch (SQLException e) {
            log.error(e);
        }

    }
}
