package com.skyworker.futuresquant.strategys.MA5minDropLight;

import cn.hutool.core.date.DateUtil;
import cn.hutool.db.Entity;
import cn.hutool.db.handler.EntityListHandler;
import cn.hutool.db.sql.SqlExecutor;
import cn.hutool.log.Log;
import com.skyworker.futuresquant.quant.GetFinanceData;
import com.skyworker.futuresquant.quant.SimulateTrade;
import com.skyworker.futuresquant.utils.QuantUtil;
import com.skyworker.futuresquant.utils.SingletonConn;
import com.skyworker.futuresquant.utils.SingletonLog;
import com.tictactec.ta.lib.Core;
import com.tictactec.ta.lib.MInteger;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.*;

import static com.skyworker.futuresquant.utils.QuantUtil.MaxMin;
import static com.skyworker.futuresquant.utils.QuantUtil.isTradeTime;

public class OpenPositionJob {
    private static final Log log = SingletonLog.getInstance().log;
    private static final Connection conn = SingletonConn.getInstance().conn;
    String tradeDateTime;
    int SimulateId;
    private int BuyLimit = 5;
    private ArrayList<HashMap<String, String>> SelectUnit;

    public OpenPositionJob(String _tradeDateTime, int _SimulateId, ArrayList<HashMap<String, String>> _SelectUnit) {
        tradeDateTime = _tradeDateTime;
        SimulateId = _SimulateId;
        SelectUnit = _SelectUnit;
    }

    public void runOpenPositionJob() {
//        ExecutorService singleThreadExecutor = Executors.newSingleThreadExecutor();
//        singleThreadExecutor.submit(() -> {
        log.info("启动开仓任务");
        log.info("获取所有当前可交易的品种");
        getAllTradeVarieties();
        log.info("跳空高低开暂停开仓处理");
        //BreakerForJump();
//        });
    }

    private void getAllTradeVarieties() {
        try {
            List<Entity> dominants = SqlExecutor.query(conn, "select * from t_futures_dominant where is_trade = 1 and simulate_id = " + SimulateId, new EntityListHandler());
            List<Entity> positions = SqlExecutor.query(conn, "select * from t_simulate_position where simulate_id = " + SimulateId, new EntityListHandler());
            BuyLimit = BuyLimit - positions.size();
            if (BuyLimit == 0) {
                log.info(tradeDateTime + "已达最大持仓数量，无法购买");
                return;
            }
            //可交易品种中去除已持仓品种
            Iterator<Entity> tradeVarieties = dominants.iterator();
            while (tradeVarieties.hasNext()) {
                Entity dominant = tradeVarieties.next();
                for (Entity position : positions)
                    if (dominant.getStr("dominant_code").equals(position.getStr("dominant_code"))) {
                        tradeVarieties.remove();
                        break;
                    }
            }

            //可交易品种中除去熔断中的交易品种，去除不在交易时间的品种
            tradeVarieties = dominants.iterator();
            List<Entity> breakers = SqlExecutor.query(conn, "select * from t_simulate_breaker where simulate_id = "
                    + SimulateId + " and breaker_to>'" + tradeDateTime + "'", new EntityListHandler());
            while (tradeVarieties.hasNext()) {
                Entity dominant = tradeVarieties.next();
                if (!isTradeTime(DateUtil.parseDateTime(tradeDateTime), dominant.getStr("dominant_code"))) {
                    tradeVarieties.remove();
                    continue;
                }
                for (Entity breaker : breakers)
                    if (dominant.getStr("dominant_code").equals(breaker.getStr("dominant_code"))) {
                        tradeVarieties.remove();
                        break;
                    }
            }


            //5分钟线 多头或者空头排列
            ArrayList<HashMap<String, String>> tradeList = new ArrayList<>();
            GetFinanceData getFinanceData = new GetFinanceData();
            Core core = new Core();
            String LastTradeDay = tradeDateTime;
            LastTradeDay = getFinanceData.getTradeDayOffset(DateUtil.parseDate(tradeDateTime), Integer.valueOf(SelectUnit.get(0).get("offset")));
            for (Entity entity : dominants) {
                String dominant_code = entity.getStr("dominant_code");
                ArrayList<HashMap<String, String>> Price = getFinanceData.GetPricePeriod(dominant_code, 25, SelectUnit.get(0).get("unit"), LastTradeDay, tradeDateTime);
                double[] close = QuantUtil.getDataList(Price, "close");
                double[] open = QuantUtil.getDataList(Price, "open");

                double[] ma5 = new double[24];
                double[] ma10 = new double[24];
                double[] ma20 = new double[24];
                core.sma(0, 23, close, 5, new MInteger(), new MInteger(), ma5);
                core.sma(0, 23, close, 10, new MInteger(), new MInteger(), ma10);
                core.sma(0, 23, close, 20, new MInteger(), new MInteger(), ma20);

                double[] close10 = Arrays.copyOfRange(close, 1, 11);
                double[] open10 = Arrays.copyOfRange(open, 1, 11);
                double[] closeMaxMin = MaxMin(close10);
                double[] openMaxMin = MaxMin(open10);

                //多头排起，且 突破最高价 且 ma5和ma10连续2天上升 ma20一天上升, 均线张开
                if (ma5[0] > ma10[0] & ma10[0] > ma20[0]) {
                    if (close[0] > closeMaxMin[0] & close[0] > openMaxMin[0]) {
                        if (ma5[0] > ma5[1] & ma5[1] > ma5[2] & ma10[0] > ma10[1] & ma10[1] > ma10[2] & ma20[0] > ma20[1]) {
                            if ((ma5[0] - ma20[0]) > (ma5[1] - ma20[1])) {
                                HashMap<String, String> hm = new HashMap<>();
                                hm.put("dominant_code", dominant_code);
                                hm.put("price", entity.getStr("price"));
                                hm.put("type", "BK");
                                hm.put("score", "0");
                                tradeList.add(hm);
                            }
                        }
                    }
                } else if (ma5[0] < ma10[0] & ma10[0] < ma20[0]) {
                    if (close[0] < closeMaxMin[1] & close[0] < openMaxMin[1]) {
                        if (ma5[0] < ma5[1] & ma5[1] < ma5[2] & ma10[0] < ma10[1] & ma10[1] < ma10[2] & ma20[0] < ma20[1]) {
                            if ((ma20[0] - ma5[0]) > (ma20[1] - ma5[1])) {
                                HashMap<String, String> hm = new HashMap<>();
                                hm.put("dominant_code", dominant_code);
                                hm.put("price", entity.getStr("price"));
                                hm.put("type", "SK");
                                hm.put("score", "0");
                                tradeList.add(hm);
                            }
                        }
                    }
                }
            }

            if (tradeList.size() == 0)
                return;

            if (tradeList.size() > BuyLimit) {
                ArrayList<HashMap<String, String>> tradetemp = new ArrayList<>();
                List<HashMap<String, String>> lh = tradeList.subList(0, BuyLimit);
                for (HashMap<String, String> hm : lh) {
                    tradetemp.add(hm);
                }
                tradeList = tradetemp;
                //Collections.sort(tradeList, new ScoreComparator());
                //tradeList = (ArrayList<HashMap<String, String>>) tradeList.subList(0, BuyLimit - 1);
                //tradeList = (ArrayList<HashMap<String, String>>) tradeList.subList(0, BuyLimit - 1);
            }

            //循环执行买入操作
            SimulateTrade simulateTrade = new SimulateTrade(SimulateId, tradeDateTime);
            for (HashMap<String, String> hm : tradeList) {
                String dominant_code = hm.get("dominant_code");
                String type = hm.get("type");
                BigDecimal amount = BigDecimal.valueOf(0);
                if (Double.valueOf(hm.get("price")) > 5000) {
                    amount = BigDecimal.valueOf(2);
                } else if (Double.valueOf(hm.get("price")) < 5000) {
                    amount = BigDecimal.valueOf(4);
                }
                log.info(tradeDateTime + ": 买入：" + dominant_code + " 买入数量：" + amount);
                simulateTrade.OpenPosition(dominant_code, type, amount);
            }
        } catch (
                SQLException e) {
            log.error(e);
        }

    }

    static class ScoreComparator implements Comparator {
        public int compare(Object object1, Object object2) {// 实现接口中的方法
            HashMap<String, String> hm1 = (HashMap<String, String>) object1;
            HashMap<String, String> hm2 = (HashMap<String, String>) object1;
            return new Integer(hm1.get("score")).compareTo(new Integer(hm2.get("score")));
        }
    }
}
