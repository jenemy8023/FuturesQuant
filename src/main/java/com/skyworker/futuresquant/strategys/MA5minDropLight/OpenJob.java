package com.skyworker.futuresquant.strategys.MA5minDropLight;

import cn.hutool.core.date.DateField;
import cn.hutool.core.date.DateTime;
import cn.hutool.core.date.DateUtil;
import cn.hutool.db.Entity;
import cn.hutool.db.handler.EntityListHandler;
import cn.hutool.db.sql.SqlExecutor;
import cn.hutool.log.Log;
import com.skyworker.futuresquant.quant.GetFinanceData;
import com.skyworker.futuresquant.quant.SimulateTrade;
import com.skyworker.futuresquant.utils.SingletonConn;
import com.skyworker.futuresquant.utils.SingletonLog;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class OpenJob {
    private static final Log log = SingletonLog.getInstance().log;
    private static final Connection conn = SingletonConn.getInstance().conn;
    String tradeDateTime = "";
    int SimulateId = 0;

    public OpenJob(String _tradeDateTime, int _SimulateId) {
        tradeDateTime = _tradeDateTime;
        SimulateId = _SimulateId;
    }

    public void runOpenJob() {
//        ExecutorService singleThreadExecutor = Executors.newSingleThreadExecutor();
//        singleThreadExecutor.submit(new Runnable() {
//            @Override
//            public void run() {
        log.info("启动开盘任务");
        log.info("移仓换月：");
        DominantChange();
        log.info("不在交易范围内的品种平仓");
        closeNotTrade();
        log.info("跳空高低开暂停开仓处理");
        BreakerForJump();
//            }
//        });
    }

    private void DominantChange() {
        SimulateTrade simulateTrade = new SimulateTrade(SimulateId, tradeDateTime);
        try {
            List<Entity> positions = SqlExecutor.query(conn, "select * from t_simulate_position where simulate_id = " + SimulateId, new EntityListHandler());
            for (Entity position : positions) {
                String code = position.getStr("code");
                String dominant_code = position.getStr("dominant_code");
                List<Entity> dominant = SqlExecutor.query(conn, "select * from t_futures_dominant where simulate_id = " + SimulateId + " and code='" + code + "'", new EntityListHandler());
                if (!dominant_code.equals(dominant.get(0).getStr("dominant_code"))) {
                    simulateTrade.ClosePosition(dominant_code);
                    log.info("移仓换月：" + dominant_code);
                }
            }
        } catch (SQLException e) {
            log.error(e);
        }
    }

    public void closeNotTrade() {
        SimulateTrade simulateTrade = new SimulateTrade(SimulateId, tradeDateTime);
        List<Entity> positions = simulateTrade.getPositions();
        List<Entity> tradeDominants = simulateTrade.getTradeDominants();

        for (Entity entity : positions) {
            String dominant_code = entity.getStr("dominant_code");
            Boolean isClose = true;
            for (Entity entity1 : tradeDominants) {
                if (entity1.getStr("dominant_code").equals(dominant_code)) {
                    isClose = false;
                    break;
                }
            }
            if (isClose) {
                if (entity.getStr("type").equals("BK"))
                    simulateTrade.ClosePosition(dominant_code, "SP", entity.getBigDecimal("amount"));
                else
                    simulateTrade.ClosePosition(dominant_code, "BP", entity.getBigDecimal("amount"));
            }
        }
    }

    public void BreakerForJump(BigDecimal jumpPercent) {
        SimulateTrade simulateTrade = new SimulateTrade(SimulateId, tradeDateTime);
        GetFinanceData getFinanceData = new GetFinanceData();
        List<Entity> tradeDominants = simulateTrade.getTradeDominants();
        String sDate = DateUtil.parseDate(tradeDateTime).toString("yyyy-MM-dd");
        ArrayList<String> al = new ArrayList<>();
        for (Entity entity : tradeDominants) {
            ArrayList<HashMap<String, String>> ah = getFinanceData.GetPrice(entity.getStr("dominant_code"), 2, "1d", sDate);
            BigDecimal open = new BigDecimal(ah.get(0).get("open"));
            BigDecimal lastClose = new BigDecimal(ah.get(1).get("close"));
            BigDecimal realJump = open.subtract(lastClose).divide(lastClose, BigDecimal.ROUND_HALF_DOWN).multiply(new BigDecimal(100)).abs();
            if (realJump.compareTo(jumpPercent) == 1) {
                //执行暂停交易操作
                al.add(entity.getStr("dominant_code"));
            }
        }

        DateTime dtBreak = DateUtil.parseDateTime(tradeDateTime).offset(DateField.HOUR, 1);
        try {
            SqlExecutor.execute(conn, "delete from t_simulate_breaker where simulate_id=" + SimulateId);
            for (String dominant_code : al) {
                SqlExecutor.execute(conn, "insert into t_simulate_breaker values(null," + SimulateId + ",'"
                        + dominant_code + "','openjump','" + tradeDateTime + "','" + dtBreak.toString() + "')");
            }
        } catch (SQLException e) {
            log.error(e);
        }
    }

    public void BreakerForJump() {
        BreakerForJump(new BigDecimal(1));
    }
}
