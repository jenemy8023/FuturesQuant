package com.skyworker.futuresquant.strategys;

import cn.hutool.core.date.DateTime;

import java.util.ArrayList;
import java.util.HashMap;

public class StopPosition {
    private DateTime tradeDate;
    private int SimulateId;
    private int StrategyId;

    public StopPosition(DateTime _tradeDate, int _SimulateId, int _StrategyId) {
        tradeDate = _tradeDate;
        SimulateId = _SimulateId;
        StrategyId = _StrategyId;
    }

    public ArrayList<HashMap<String, String>> runStopPosition() {
        switch (StrategyId) {
            case 1: {
                com.skyworker.futuresquant.strategys.MultiPeriodMA.StopPositionJob stopPositionJob;
                stopPositionJob = new com.skyworker.futuresquant.strategys.MultiPeriodMA.StopPositionJob(tradeDate.toString("yyyy-MM-dd HH:mm:ss"), SimulateId);
                return stopPositionJob.runStopPositionJob();
            }
            case 2: {
                com.skyworker.futuresquant.strategys.MultiPeriodMALightStop.StopPositionJob stopPositionJob;
                stopPositionJob = new com.skyworker.futuresquant.strategys.MultiPeriodMALightStop.StopPositionJob(tradeDate.toString("yyyy-MM-dd HH:mm:ss"), SimulateId);
                return stopPositionJob.runStopPositionJob();
            }
            case 3: {
                com.skyworker.futuresquant.strategys.MA5minDropLight.StopPositionJob stopPositionJob;
                stopPositionJob = new com.skyworker.futuresquant.strategys.MA5minDropLight.StopPositionJob(tradeDate.toString("yyyy-MM-dd HH:mm:ss"), SimulateId);
                return stopPositionJob.runStopPositionJob();
            }
            case 4: {
                com.skyworker.futuresquant.strategys.MA5Based.StopPositionJob stopPositionJob;
                stopPositionJob = new com.skyworker.futuresquant.strategys.MA5Based.StopPositionJob(tradeDate.toString("yyyy-MM-dd HH:mm:ss"), SimulateId);
                return stopPositionJob.runStopPositionJob();
            }
            case 5: {
                com.skyworker.futuresquant.strategys.MA5MACDDual.StopPositionJob stopPositionJob;
                stopPositionJob = new com.skyworker.futuresquant.strategys.MA5MACDDual.StopPositionJob(tradeDate.toString("yyyy-MM-dd HH:mm:ss"), SimulateId);
                return stopPositionJob.runStopPositionJob();
            }
            case 6: {
                com.skyworker.futuresquant.strategys.MA5MACDDualCloseInDay.StopPositionJob stopPositionJob;
                stopPositionJob = new com.skyworker.futuresquant.strategys.MA5MACDDualCloseInDay.StopPositionJob(tradeDate.toString("yyyy-MM-dd HH:mm:ss"), SimulateId);
                return stopPositionJob.runStopPositionJob();
            }
        }
        ArrayList<HashMap<String, String>> temp = new ArrayList<>();
        return temp;

    }
}
