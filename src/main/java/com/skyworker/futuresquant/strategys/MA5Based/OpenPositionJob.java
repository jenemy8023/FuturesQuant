package com.skyworker.futuresquant.strategys.MA5Based;

import cn.hutool.core.date.DateUtil;
import cn.hutool.db.Entity;
import cn.hutool.db.handler.EntityListHandler;
import cn.hutool.db.sql.SqlExecutor;
import cn.hutool.log.Log;
import com.skyworker.futuresquant.quant.GetFinanceData;
import com.skyworker.futuresquant.quant.SimulateTrade;
import com.skyworker.futuresquant.utils.QuantUtil;
import com.skyworker.futuresquant.utils.SingletonConn;
import com.skyworker.futuresquant.utils.SingletonLog;
import com.tictactec.ta.lib.Core;
import com.tictactec.ta.lib.MInteger;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.*;

import static com.skyworker.futuresquant.utils.QuantUtil.MaxMin;
import static com.skyworker.futuresquant.utils.QuantUtil.isTradeTime;

public class OpenPositionJob {
    private static final Log log = SingletonLog.getInstance().log;
    private static final Connection conn = SingletonConn.getInstance().conn;
    private final int BuyCounts = 5;
    private final ArrayList<HashMap<String, String>> SelectUnit;
    String tradeDateTime;
    int SimulateId;

    public OpenPositionJob(String _tradeDateTime, int _SimulateId, ArrayList<HashMap<String, String>> _SelectUnit) {
        tradeDateTime = _tradeDateTime;
        SimulateId = _SimulateId;
        SelectUnit = _SelectUnit;
    }

    public ArrayList<HashMap<String, String>> runOpenPositionJob() {
        log.info("启动开仓任务");
        log.info("获取所有当前可交易的品种");
        return getAllTradeVarieties();
        //log.info("跳空高低开暂停开仓处理");
    }

    private ArrayList<HashMap<String, String>> getAllTradeVarieties() {
        ArrayList<HashMap<String, String>> tradeList = new ArrayList<>();
        try {
            List<Entity> dominants = SqlExecutor.query(conn, "select * from t_futures_dominant where is_trade = 1 and simulate_id = " + SimulateId + " order by money desc", new EntityListHandler());
            List<Entity> positions = SqlExecutor.query(conn, "select * from t_simulate_position where simulate_id = " + SimulateId, new EntityListHandler());
            Entity simulateMain = SqlExecutor.query(conn, "select * from t_simulate_main where id = " + SimulateId, new EntityListHandler()).get(0);
            int BuyLimit = BuyCounts - positions.size();
            if (BuyLimit <= 0) {
                log.info(tradeDateTime + "已达最大持仓数量，无法购买");
                return tradeList;
            }
            //可交易品种中去除已持仓品种
            Iterator<Entity> tradeVarieties = dominants.iterator();
            while (tradeVarieties.hasNext()) {
                Entity dominant = tradeVarieties.next();
                for (Entity position : positions)
                    if (dominant.getStr("dominant_code").equals(position.getStr("dominant_code"))) {
                        tradeVarieties.remove();
                        break;
                    }
            }

            //可交易品种中除去熔断中的交易品种，去除不在交易时间的品种
            tradeVarieties = dominants.iterator();
            List<Entity> breakers = SqlExecutor.query(conn, "select * from t_simulate_breaker where simulate_id = "
                    + SimulateId + " and breaker_to>'" + tradeDateTime + "'", new EntityListHandler());
            while (tradeVarieties.hasNext()) {
                Entity dominant = tradeVarieties.next();
                if (!isTradeTime(DateUtil.parseDateTime(tradeDateTime), dominant.getStr("dominant_code"))) {
                    tradeVarieties.remove();
                    continue;
                }
                for (Entity breaker : breakers)
                    if (dominant.getStr("dominant_code").equals(breaker.getStr("dominant_code"))) {
                        tradeVarieties.remove();
                        break;
                    }
            }

            //5分钟线 多头或者空头排列
            GetFinanceData getFinanceData = new GetFinanceData();
            Core core = new Core();
            String LastTradeDay;
            LastTradeDay = getFinanceData.getTradeDayOffset(DateUtil.parseDate(tradeDateTime), Integer.parseInt(SelectUnit.get(0).get("offset")));
            for (Entity entity : dominants) {
                double damount = simulateMain.getDouble("total_amount") / (BuyCounts + 0.5) / Double.parseDouble(entity.getStr("margin"));
                int amount = (int) damount;
                if (amount == 0)
                    continue;

                String dominant_code = entity.getStr("dominant_code");
                ArrayList<HashMap<String, String>> Price = getFinanceData.GetPricePeriod(dominant_code, 20, SelectUnit.get(0).get("unit"), LastTradeDay, tradeDateTime);
                double[] close = QuantUtil.getDataList(Price, "close");
                double[] open = QuantUtil.getDataList(Price, "open");
                double[] volume = QuantUtil.getDataList(Price, "volume");

                double[] ma5 = new double[19];
                double[] ma10 = new double[19];
                double[] ma15 = new double[19];
                double[] volma19 = new double[19];
                core.sma(0, 18, close, 5, new MInteger(), new MInteger(), ma5);
                core.sma(0, 18, close, 10, new MInteger(), new MInteger(), ma10);
                core.sma(0, 18, close, 15, new MInteger(), new MInteger(), ma15);
                core.sma(0, 18, volume, 15, new MInteger(), new MInteger(), volma19);


                double[] closetemp = Arrays.copyOfRange(close, 1, 10);
                double[] opentemp = Arrays.copyOfRange(open, 1, 10);
                double[] closeMaxMin = MaxMin(closetemp);
                double[] openMaxMin = MaxMin(opentemp);


                if (ma5[0] > ma10[0] & ma10[0] > ma15[0]) {
                    //开仓基础条件：均线多头排列
                    //开仓条件1：收盘价突破20根K线的高点。（数量可以考虑调整优化）
                    //开仓条件2：ma5连续两天上升，ma10连续两天上升，ma15连续1天上升（需要调整）
                    //开仓条件3：k线主体在ma10上方
                    //开仓条件4：均线开口变大（调整）
                    //开仓条件5：当前两根阴线不开仓（未完成）
                    //开仓条件6：下影线长于主体不开仓（未完成）
                    boolean condition1 = close[0] > closeMaxMin[0] & close[0] > openMaxMin[0];
                    boolean condition2 = ma5[0] > ma5[1] & ma5[1] > ma5[2] & ma10[0] > ma10[1] & ma10[1] > ma10[2] & ma15[0] > ma15[1];
                    boolean condition3 = open[0] > ma10[0] & close[0] > ma10[0];
                    boolean condition4 = (ma5[0] - ma15[0]) > (ma5[1] - ma15[1]);
                    boolean condition5 = close[0] >= open[0] & close[1] >= open[1];
                    boolean condition6 = (volume[0] + volume[1]) / (volma19[0] * 2) > 1.8;
                    if (condition2 & condition3 & condition4 & condition6) {
                        HashMap<String, String> hm = new HashMap<>();
                        hm.put("dominant_code", dominant_code);
                        hm.put("price", entity.getStr("price"));
                        hm.put("type", "BK");
                        hm.put("score", "0");
                        hm.put("vol", String.valueOf(amount));
                        tradeList.add(hm);
                    }
                } else if (ma5[0] < ma10[0] & ma10[0] < ma15[0]) {
                    boolean condition1 = close[0] < closeMaxMin[1] & close[0] < openMaxMin[1];
                    boolean condition2 = ma5[0] < ma5[1] & ma5[1] < ma5[2] & ma10[0] < ma10[1] & ma10[1] < ma10[2] & ma15[0] < ma15[1];
                    boolean condition3 = open[0] < ma10[0] & close[0] < ma10[0];
                    boolean condition4 = (ma15[0] - ma5[0]) > (ma15[1] - ma5[1]);
                    boolean condition5 = close[0] <= open[0] & close[1] <= open[1];
                    boolean condition6 = (volume[0] + volume[1]) / (volma19[0] * 2) > 1.8 ;

                    if (condition2 & condition3 & condition4 & condition6) {
                        HashMap<String, String> hm = new HashMap<>();
                        hm.put("dominant_code", dominant_code);
                        hm.put("price", entity.getStr("price"));
                        hm.put("type", "SK");
                        hm.put("score", "0");
                        hm.put("vol", String.valueOf(amount));
                        tradeList.add(hm);
                    }
                }
            }

            if (tradeList.size() == 0)
                return tradeList;

            if (tradeList.size() > BuyLimit) {
                //                Collections.sort(tradeList, new ScoreComparator());
                List<HashMap<String, String>> lh = tradeList.subList(0, BuyLimit);
                //ArrayList<HashMap<String, String>> tradetemp = new ArrayList<>(lh);
                tradeList = new ArrayList<>(lh);
                //Collections.sort(tradeList, new ScoreComparator());
                //tradeList = (ArrayList<HashMap<String, String>>) tradeList.subList(0, BuyLimit - 1);
            }

            //循环执行买入操作
            SimulateTrade simulateTrade = new SimulateTrade(SimulateId, tradeDateTime);
            for (HashMap<String, String> hm : tradeList) {
                String dominant_code = hm.get("dominant_code");
                String type = hm.get("type");
                log.info(tradeDateTime + ": 买入：" + dominant_code + " 买入数量：" + hm.get("vol"));
                simulateTrade.OpenPosition(dominant_code, type, BigDecimal.valueOf(Double.valueOf(hm.get("vol"))));
            }
        } catch (
                SQLException e) {
            log.error(e);
        }
        return tradeList;
    }

    static class ScoreComparator implements Comparator {
        public int compare(Object object1, Object object2) {// 实现接口中的方法
            HashMap<String, String> hm1 = (HashMap<String, String>) object1;
            HashMap<String, String> hm2 = (HashMap<String, String>) object1;
            Integer i1 = Math.abs(new Integer(hm1.get("score")));
            Integer i2 = Math.abs(new Integer(hm2.get("score")));
            return i1.compareTo(i2);
        }
    }


}
