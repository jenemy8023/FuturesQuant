package com.skyworker.futuresquant.strategys;

import cn.hutool.core.date.DateTime;
import com.skyworker.futuresquant.strategys.MA5MACDDualCloseInDay.BeforeOpenJob;

public class CalculateStop {
    private DateTime tradeDate;
    private int SimulateId;
    private int StrategyId;

    public CalculateStop(DateTime _tradeDate, int _SimulateId, int _StrategyId) {
        tradeDate = _tradeDate;
        SimulateId = _SimulateId;
        StrategyId = _StrategyId;
    }

    public void runCalculateStop() {
        switch (StrategyId) {
            case 1: {
                com.skyworker.futuresquant.strategys.MultiPeriodMA.CalculateStopJob calculateStopJob;
                calculateStopJob = new com.skyworker.futuresquant.strategys.MultiPeriodMA.CalculateStopJob(tradeDate.toString("yyyy-MM-dd HH:mm:ss"), SimulateId);
                calculateStopJob.runCalculateStopJob();
                break;
            }
            case 2: {
                com.skyworker.futuresquant.strategys.MultiPeriodMALightStop.CalculateStopJob calculateStopJob;
                calculateStopJob = new com.skyworker.futuresquant.strategys.MultiPeriodMALightStop.CalculateStopJob(tradeDate.toString("yyyy-MM-dd HH:mm:ss"), SimulateId);
                calculateStopJob.runCalculateStopJob();
                break;
            }
            case 3: {
                com.skyworker.futuresquant.strategys.MA5minDropLight.CalculateStopJob calculateStopJob;
                calculateStopJob = new com.skyworker.futuresquant.strategys.MA5minDropLight.CalculateStopJob(tradeDate.toString("yyyy-MM-dd HH:mm:ss"), SimulateId);
                calculateStopJob.runCalculateStopJob();
                break;
            }
            case 4: {
                com.skyworker.futuresquant.strategys.MA5Based.CalculateStopJob calculateStopJob;
                calculateStopJob = new com.skyworker.futuresquant.strategys.MA5Based.CalculateStopJob(tradeDate.toString("yyyy-MM-dd HH:mm:ss"), SimulateId);
                calculateStopJob.runCalculateStopJob();
                break;
            }
            case 5: {
                com.skyworker.futuresquant.strategys.MA5MACDDual.CalculateStopJob calculateStopJob;
                calculateStopJob = new com.skyworker.futuresquant.strategys.MA5MACDDual.CalculateStopJob(tradeDate.toString("yyyy-MM-dd HH:mm:ss"), SimulateId);
                calculateStopJob.runCalculateStopJob();
                break;
            }
            case 6: {
                com.skyworker.futuresquant.strategys.MA5MACDDualCloseInDay.CalculateStopJob calculateStopJob;
                calculateStopJob = new com.skyworker.futuresquant.strategys.MA5MACDDualCloseInDay.CalculateStopJob(tradeDate.toString("yyyy-MM-dd HH:mm:ss"), SimulateId);
                calculateStopJob.runCalculateStopJob();
            }
        }

    }
}
