package com.skyworker.futuresquant.strategys;

import cn.hutool.core.date.DateTime;

public class Open {
    private DateTime tradeDate;
    private int SimulateId;
    private int StrategyId;

    public Open(DateTime _tradeDate, int _SimulateId, int _StrategyId) {
        tradeDate = _tradeDate;
        SimulateId = _SimulateId;
        StrategyId = _StrategyId;
    }

    public void runOpen() {
        switch (StrategyId) {
            case 1: {
                com.skyworker.futuresquant.strategys.MultiPeriodMA.OpenJob openJob;
                openJob = new com.skyworker.futuresquant.strategys.MultiPeriodMA.OpenJob(tradeDate.toString("yyyy-MM-dd HH:mm:ss"), SimulateId);
                openJob.runOpenJob();
                break;
            }
            case 2: {
                com.skyworker.futuresquant.strategys.MultiPeriodMALightStop.OpenJob openJob;
                openJob = new com.skyworker.futuresquant.strategys.MultiPeriodMALightStop.OpenJob(tradeDate.toString("yyyy-MM-dd HH:mm:ss"), SimulateId);
                openJob.runOpenJob();
                break;
            }
            case 3: {
                com.skyworker.futuresquant.strategys.MA5minDropLight.OpenJob openJob;
                openJob = new com.skyworker.futuresquant.strategys.MA5minDropLight.OpenJob(tradeDate.toString("yyyy-MM-dd HH:mm:ss"), SimulateId);
                openJob.runOpenJob();
                break;
            }
            case 4: {
                com.skyworker.futuresquant.strategys.MA5Based.OpenJob openJob;
                openJob = new com.skyworker.futuresquant.strategys.MA5Based.OpenJob(tradeDate.toString("yyyy-MM-dd HH:mm:ss"), SimulateId);
                openJob.runOpenJob();
                break;
            }
            case 5: {
                com.skyworker.futuresquant.strategys.MA5MACDDual.OpenJob openJob;
                openJob = new com.skyworker.futuresquant.strategys.MA5MACDDual.OpenJob(tradeDate.toString("yyyy-MM-dd HH:mm:ss"), SimulateId);
                openJob.runOpenJob();
                break;
            }
            case 6: {
                com.skyworker.futuresquant.strategys.MA5MACDDualCloseInDay.OpenJob openJob;
                openJob = new com.skyworker.futuresquant.strategys.MA5MACDDualCloseInDay.OpenJob(tradeDate.toString("yyyy-MM-dd HH:mm:ss"), SimulateId);
                openJob.runDayOpenJob();
                break;
            }
        }

    }
}
