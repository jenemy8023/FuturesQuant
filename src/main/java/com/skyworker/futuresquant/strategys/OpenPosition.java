package com.skyworker.futuresquant.strategys;

import cn.hutool.core.date.DateTime;

import java.util.ArrayList;
import java.util.HashMap;

public class OpenPosition {
    private DateTime tradeDate;
    private int SimulateId;
    private int StrategyId;
    private ArrayList<HashMap<String, String>> SelectUnit;

    public OpenPosition(DateTime _tradeDate, int _SimulateId, int _StrategyId, ArrayList<HashMap<String, String>> _SelectUnit) {
        tradeDate = _tradeDate;
        SimulateId = _SimulateId;
        StrategyId = _StrategyId;
        SelectUnit = _SelectUnit;
    }

    public ArrayList<HashMap<String, String>>  runOpenPosition() {
        ArrayList<HashMap<String, String>> temp = new ArrayList<>();
        switch (StrategyId) {
            case 1: {
                com.skyworker.futuresquant.strategys.MultiPeriodMA.OpenPositionJob openPositionJob;
                openPositionJob = new com.skyworker.futuresquant.strategys.MultiPeriodMA.OpenPositionJob(tradeDate.toString("yyyy-MM-dd HH:mm:ss"), SimulateId, SelectUnit);
                openPositionJob.runOpenPositionJob();
                break;
            }
            case 2: {
                com.skyworker.futuresquant.strategys.MultiPeriodMALightStop.OpenPositionJob openPositionJob;
                openPositionJob = new com.skyworker.futuresquant.strategys.MultiPeriodMALightStop.OpenPositionJob(tradeDate.toString("yyyy-MM-dd HH:mm:ss"), SimulateId, SelectUnit);
                openPositionJob.runOpenPositionJob();
                break;
            }
            case 3: {
                com.skyworker.futuresquant.strategys.MA5minDropLight.OpenPositionJob openPositionJob;
                openPositionJob = new com.skyworker.futuresquant.strategys.MA5minDropLight.OpenPositionJob(tradeDate.toString("yyyy-MM-dd HH:mm:ss"), SimulateId, SelectUnit);
                openPositionJob.runOpenPositionJob();
                break;
            }
            case 4: {
                com.skyworker.futuresquant.strategys.MA5Based.OpenPositionJob openPositionJob;
                openPositionJob = new com.skyworker.futuresquant.strategys.MA5Based.OpenPositionJob(tradeDate.toString("yyyy-MM-dd HH:mm:ss"), SimulateId, SelectUnit);
                openPositionJob.runOpenPositionJob();
                break;
            }
            case 5: {
                com.skyworker.futuresquant.strategys.MA5MACDDual.OpenPositionJob openPositionJob;
                openPositionJob = new com.skyworker.futuresquant.strategys.MA5MACDDual.OpenPositionJob(tradeDate.toString("yyyy-MM-dd HH:mm:ss"), SimulateId, SelectUnit);
                openPositionJob.runOpenPositionJob();
                break;
            }
            case 6: {
                com.skyworker.futuresquant.strategys.MA5MACDDualCloseInDay.OpenPositionJob openPositionJob;
                openPositionJob = new com.skyworker.futuresquant.strategys.MA5MACDDualCloseInDay.OpenPositionJob(tradeDate.toString("yyyy-MM-dd HH:mm:ss"), SimulateId, SelectUnit);
                openPositionJob.runOpenPositionJob();
                break;
            }
        }
        return temp;

    }
}
