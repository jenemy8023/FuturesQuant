package com.skyworker.futuresquant.strategys;

import cn.hutool.core.date.DateTime;
import cn.hutool.db.Entity;
import cn.hutool.db.handler.EntityListHandler;
import cn.hutool.db.sql.SqlExecutor;
import cn.hutool.log.Log;
import com.skyworker.futuresquant.quant.SimulateTrade;
import com.skyworker.futuresquant.utils.SingletonConn;
import com.skyworker.futuresquant.utils.SingletonLog;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

public class CloseAll {
    private static final Log log = SingletonLog.getInstance().log;
    private static final Connection conn = SingletonConn.getInstance().conn;
    SimulateTrade simulateTrade;
    private final int SimulateId;
    String tradeDateTime;


    public CloseAll(int _SimulateId,String _tradeDateTime) {
        SimulateId = _SimulateId;
        tradeDateTime = _tradeDateTime;
    }

    public void runCloseAll() {
        try {
            SimulateTrade simulateTrade = new SimulateTrade(SimulateId, tradeDateTime);
            List<Entity> positions = SqlExecutor.query(conn, "select * from t_simulate_position where simulate_id = "
                    + SimulateId, new EntityListHandler());
            for (Entity position : positions) {
                String dominant_code = position.getStr("dominant_code");
                double profit = simulateTrade.ClosePosition(dominant_code);
                log.info("日终平仓：" + dominant_code);
            }
        } catch (SQLException e) {
            log.error(e);
        }


    }
}
