package com.skyworker.futuresquant.strategys.MA5MACDDualCloseInDay;

import cn.hutool.core.date.DateUtil;
import cn.hutool.db.Entity;
import cn.hutool.db.handler.EntityListHandler;
import cn.hutool.db.sql.SqlExecutor;
import cn.hutool.log.Log;
import com.skyworker.futuresquant.quant.GetFinanceData;
import com.skyworker.futuresquant.quant.SimulateTrade;
import com.skyworker.futuresquant.utils.IndicatorsUtil;
import com.skyworker.futuresquant.utils.QuantUtil;
import com.skyworker.futuresquant.utils.SingletonConn;
import com.skyworker.futuresquant.utils.SingletonLog;
import com.tictactec.ta.lib.Core;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import static com.skyworker.futuresquant.utils.QuantUtil.isTradeTime;

public class OpenPositionJob {
    private static final Log log = SingletonLog.getInstance().log;
    private static final Connection conn = SingletonConn.getInstance().conn;
    private final int BuyCounts = 7;
    private final ArrayList<HashMap<String, String>> SelectUnit;
    String tradeDateTime;
    int SimulateId;

    public OpenPositionJob(String _tradeDateTime, int _SimulateId, ArrayList<HashMap<String, String>> _SelectUnit) {
        tradeDateTime = _tradeDateTime;
        SimulateId = _SimulateId;
        SelectUnit = _SelectUnit;
    }

    public ArrayList<HashMap<String, String>> runOpenPositionJob() {
        log.info("启动开仓任务");
        log.info("获取所有当前可交易的品种");
        return getAllTradeVarieties();
        //log.info("跳空高低开暂停开仓处理");
    }

    private ArrayList<HashMap<String, String>> getAllTradeVarieties() {
        ArrayList<HashMap<String, String>> tradeList = new ArrayList<>();
        try {
            List<Entity> dominants = SqlExecutor.query(conn, "select * from t_futures_dominant where is_trade = 1 and simulate_id = " + SimulateId + " order by money desc", new EntityListHandler());
            List<Entity> positions = SqlExecutor.query(conn, "select * from t_simulate_position where simulate_id = " + SimulateId, new EntityListHandler());
            Entity simulateMain = SqlExecutor.query(conn, "select * from t_simulate_main where id = " + SimulateId, new EntityListHandler()).get(0);
            int BuyLimit = BuyCounts - positions.size();
            if (BuyLimit <= 0) {
                log.info(tradeDateTime + "已达最大持仓数量，无法购买");
                return tradeList;
            }
            //可交易品种中去除已持仓品种
            Iterator<Entity> tradeVarieties = dominants.iterator();
            while (tradeVarieties.hasNext()) {
                Entity dominant = tradeVarieties.next();
                for (Entity position : positions)
                    if (dominant.getStr("dominant_code").equals(position.getStr("dominant_code"))) {
                        tradeVarieties.remove();
                        break;
                    }
            }

            //可交易品种中除去熔断中的交易品种，去除不在交易时间的品种
            tradeVarieties = dominants.iterator();
            List<Entity> breakers = SqlExecutor.query(conn, "select * from t_simulate_breaker where simulate_id = "
                    + SimulateId + " and breaker_to>'" + tradeDateTime + "'", new EntityListHandler());
            while (tradeVarieties.hasNext()) {
                Entity dominant = tradeVarieties.next();
                if (!isTradeTime(DateUtil.parseDateTime(tradeDateTime), dominant.getStr("dominant_code"))) {
                    tradeVarieties.remove();
                    continue;
                }
                for (Entity breaker : breakers)
                    if (dominant.getStr("dominant_code").equals(breaker.getStr("dominant_code"))) {
                        tradeVarieties.remove();
                        break;
                    }
            }

            //5分钟线 多头或者空头排列
            GetFinanceData getFinanceData = new GetFinanceData();
            IndicatorsUtil indicatorsUtil = new IndicatorsUtil();
            Core core = new Core();

            String LastTradeDay = tradeDateTime;
            LastTradeDay = getFinanceData.getTradeDayOffset(DateUtil.parseDate(tradeDateTime), Integer.parseInt(SelectUnit.get(0).get("offset")));
            for (Entity entity : dominants) {
                double dAmount = simulateMain.getDouble("total_amount") / (BuyCounts + 0.5) / Double.parseDouble(entity.getStr("margin"));
                int amount = (int) dAmount;
                if (amount == 0) continue;

                String dominant_code = entity.getStr("dominant_code");
                ArrayList<HashMap<String, String>> Price = getFinanceData.GetPricePeriod(dominant_code, 80, SelectUnit.get(0).get("unit"), LastTradeDay, tradeDateTime);
                double[] close = QuantUtil.getDataList(Price, "close");
                double[] open = QuantUtil.getDataList(Price, "open");

                double[] ma5 = indicatorsUtil.MA(close, 5, 5);
                double[] ma10 = indicatorsUtil.MA(close, 10, 5);
                double[] ma15 = indicatorsUtil.MA(close, 15, 5);
                double[] ma20 = indicatorsUtil.MA(close, 20, 5);
                double[] ma30 = indicatorsUtil.MA(close, 30, 5);

                if (ma5[0] > ma10[0] & ma10[0] > ma15[0] & ma15[0] > ma20[0]) {
                    //开仓基础条件：均线多头排列
                    //开仓条件1：ma5连续三天上升，ma10连续一天上升
                    //开仓条件2：k线在10日均线上方
                    //开仓条件3：5个K线前  MA5小于ma10
                    //开仓条件4：最近5个ma5的涨幅大于0.3%
                    //开仓条件5：最近5个ma不存在单根涨幅超过0.6%
                    //开仓条件6：MACD多头排列 且上升，diff大于0
                    boolean condition1 = ma5[0] > ma5[1] & ma5[1] > ma5[2] & ma5[2] > ma5[3] & ma10[0] > ma10[1];
                    boolean condition2 = open[0] > ma10[0] & close[0] > ma10[0];
                    boolean condition3 = close[0] > open[0] & close[1] > open[1];
                    boolean condition4 = ((ma5[0] - ma5[4]) / ma5[4]) > 0.002;
                    //boolean condition4 = ((ma5[0] - ma5[4]) / ma5[4]) > 0.003;
                    boolean condition5 = Math.abs((close[0] - open[0]) / close[0]) < 0.006 & Math.abs((close[1] - open[1]) / close[1]) < 0.006 & Math.abs((close[2] - open[2]) / close[2]) < 0.006 & Math.abs((close[3] - open[3]) / close[3]) < 0.006 & Math.abs((close[4] - open[4]) / close[4]) < 0.006;
                    if (condition1 & condition2 & condition4 & condition5) {
                        ArrayList<double[]> aMacd = indicatorsUtil.MACD(close, 12, 26, 9);
                        double[] diff = aMacd.get(0);
                        double[] dea = aMacd.get(1);
                        if (diff[0] > dea[0] & diff[0] > diff[1] & diff[0]>0) {
                            HashMap<String, String> hm = new HashMap<>();
                            hm.put("dominant_code", dominant_code);
                            hm.put("price", entity.getStr("price"));
                            hm.put("type", "BK");
                            hm.put("score", "0");
                            hm.put("vol", String.valueOf(amount));
                            tradeList.add(hm);
                        }
                    }
                } else if (ma5[0] < ma10[0] & ma10[0] < ma15[0] & ma15[0] < ma20[0]) {
                    boolean condition1 = ma5[0] < ma5[1] & ma5[1] < ma5[2] & ma5[2] < ma5[3] & ma10[0] < ma10[1];
                    boolean condition2 = open[0] < ma10[0] & close[0] < ma10[0];
                    boolean condition3 = close[0] < open[0] & close[1] < open[1];
                    boolean condition4 = Math.abs((ma5[0] - ma5[4]) / ma5[4]) > 0.002;
                    //boolean condition4 = Math.abs((ma5[0] - ma5[4]) / ma5[4]) > 0.003;
                    boolean condition5 = Math.abs((close[0] - open[0]) / close[0]) < 0.006 & Math.abs((close[1] - open[1]) / close[1]) < 0.006 & Math.abs((close[2] - open[2]) / close[2]) < 0.006 & Math.abs((close[3] - open[3]) / close[3]) < 0.006 & Math.abs((close[4] - open[4]) / close[4]) < 0.006;

                    if (condition1 & condition2 & condition4 & condition5) {
                        ArrayList<double[]> aMacd = indicatorsUtil.MACD(close, 12, 26, 9);
                        double[] diff = aMacd.get(0);
                        double[] dea = aMacd.get(1);
                        if (diff[0] < dea[0] & diff[0] < diff[1] & diff[0]<0) {
                            HashMap<String, String> hm = new HashMap<>();
                            hm.put("dominant_code", dominant_code);
                            hm.put("price", entity.getStr("price"));
                            hm.put("type", "SK");
                            hm.put("score", "0");
                            hm.put("vol", String.valueOf(amount));
                            tradeList.add(hm);
                        }
                    }
                }
            }

            if (tradeList.size() == 0) return tradeList;

            if (tradeList.size() > BuyLimit) {
                ArrayList<HashMap<String, String>> tradetemp = new ArrayList<>();
//                Collections.sort(tradeList, new ScoreComparator());
                List<HashMap<String, String>> lh = tradeList.subList(0, BuyLimit);
                for (HashMap<String, String> hm : lh) {
                    tradetemp.add(hm);
                }
                tradeList = tradetemp;
                //Collections.sort(tradeList, new ScoreComparator());
                //tradeList = (ArrayList<HashMap<String, String>>) tradeList.subList(0, BuyLimit - 1);
            }

            //循环执行买入操作
            SimulateTrade simulateTrade = new SimulateTrade(SimulateId, tradeDateTime);
            for (HashMap<String, String> hm : tradeList) {
                String dominant_code = hm.get("dominant_code");
                String type = hm.get("type");
                log.info(tradeDateTime + ": 买入：" + dominant_code + " 买入数量：" + hm.get("vol"));
                simulateTrade.OpenPosition(dominant_code, type, BigDecimal.valueOf(Double.valueOf(hm.get("vol"))));
            }
        } catch (SQLException e) {
            log.error(e);
        }
        return tradeList;
    }

}
