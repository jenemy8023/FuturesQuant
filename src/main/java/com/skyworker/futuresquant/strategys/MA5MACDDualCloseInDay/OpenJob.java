package com.skyworker.futuresquant.strategys.MA5MACDDualCloseInDay;

import cn.hutool.core.date.DateField;
import cn.hutool.core.date.DateTime;
import cn.hutool.core.date.DateUtil;
import cn.hutool.db.Entity;
import cn.hutool.db.sql.SqlExecutor;
import cn.hutool.log.Log;
import com.skyworker.futuresquant.quant.GetFinanceData;
import com.skyworker.futuresquant.quant.SimulateTrade;
import com.skyworker.futuresquant.utils.SingletonConn;
import com.skyworker.futuresquant.utils.SingletonLog;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class OpenJob {
    private static final Log log = SingletonLog.getInstance().log;
    private static final Connection conn = SingletonConn.getInstance().conn;
    String tradeDateTime = "";
    int SimulateId = 0;

    public OpenJob(String _tradeDateTime, int _SimulateId) {
        tradeDateTime = _tradeDateTime;
        SimulateId = _SimulateId;
    }

    public void runDayOpenJob() {
        log.info("开盘以及首根k线跳空处理");
        BreakerForJumpAndLongK();
    }

    public void BreakerForJumpAndLongK() {
        SimulateTrade simulateTrade = new SimulateTrade(SimulateId, tradeDateTime);
        GetFinanceData getFinanceData = new GetFinanceData();
        List<Entity> tradeDominants = simulateTrade.getTradeDominants();
        String LastTradeDay = getFinanceData.getTradeDayOffset(DateUtil.parseDate(tradeDateTime), -1);
        ArrayList<String> al = new ArrayList<>();
        for (Entity entity : tradeDominants) {
            ArrayList<HashMap<String, String>> ah = getFinanceData.GetPricePeriod(entity.getStr("dominant_code"),
                    2, "5m", LastTradeDay, tradeDateTime);
            double jump1 = Math.abs(Double.parseDouble(ah.get(0).get("open")) - Double.parseDouble(ah.get(0).get("close"))
                    / Double.parseDouble(ah.get(0).get("open")));
            double jump2 = Math.abs(Double.parseDouble(ah.get(1).get("close")) - Double.parseDouble(ah.get(0).get("open"))
                    / Double.parseDouble(ah.get(1).get("close")));
            if (jump1>0.006 | jump2>0.008 | (jump1+jump2)>0.01) {
                //执行暂停交易操作
                al.add(entity.getStr("dominant_code"));
            }
        }

        DateTime dtBreak = DateUtil.parseDateTime(tradeDateTime).offset(DateField.MINUTE, 30);
        try {
            SqlExecutor.execute(conn, "delete from t_simulate_breaker where simulate_id=" + SimulateId);
            for (String dominant_code : al) {
                SqlExecutor.execute(conn, "insert into t_simulate_breaker values(null," + SimulateId + ",'"
                        + dominant_code + "','openjump','" + tradeDateTime + "','" + dtBreak.toString() + "')");
            }
        } catch (SQLException e) {
            log.error(e);
        }
    }
}
