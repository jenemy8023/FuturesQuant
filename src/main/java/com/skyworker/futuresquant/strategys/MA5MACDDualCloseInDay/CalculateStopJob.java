package com.skyworker.futuresquant.strategys.MA5MACDDualCloseInDay;

import cn.hutool.core.date.DateUtil;
import cn.hutool.db.Entity;
import cn.hutool.db.handler.EntityListHandler;
import cn.hutool.db.sql.SqlExecutor;
import cn.hutool.log.Log;
import cn.hutool.setting.Setting;
import com.skyworker.futuresquant.quant.GetFinanceData;
import com.skyworker.futuresquant.utils.QuantUtil;
import com.skyworker.futuresquant.utils.SingletonConn;
import com.skyworker.futuresquant.utils.SingletonLog;
import com.tictactec.ta.lib.Core;
import com.tictactec.ta.lib.MInteger;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class CalculateStopJob {
    private static final Connection conn = SingletonConn.getInstance().conn;
    String tradeDateTime;
    int SimulateId;
    private Log log = SingletonLog.getInstance().log;
    public CalculateStopJob(String _tradeDateTime, int _SimulateId) {
        tradeDateTime = _tradeDateTime;
        SimulateId = _SimulateId;
    }
    public void runCalculateStopJob() {
        //log.info("启动止损线计算任务");
        CalculateStop();
    }
    private void CalculateStop() {

    }

}
