package com.skyworker.futuresquant.strategys.MA5MACDDual;

import cn.hutool.core.date.DateField;
import cn.hutool.core.date.DateTime;
import cn.hutool.core.date.DateUtil;
import cn.hutool.db.Entity;
import cn.hutool.db.handler.EntityListHandler;
import cn.hutool.db.sql.SqlExecutor;
import cn.hutool.log.Log;
import com.skyworker.futuresquant.quant.GetFinanceData;
import com.skyworker.futuresquant.quant.SimulateTrade;
import com.skyworker.futuresquant.utils.IndicatorsUtil;
import com.skyworker.futuresquant.utils.QuantUtil;
import com.skyworker.futuresquant.utils.SingletonConn;
import com.skyworker.futuresquant.utils.SingletonLog;
import com.tictactec.ta.lib.Core;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static com.skyworker.futuresquant.utils.QuantUtil.isTradeTime;

public class StopPositionJob {
    private static final Log log = SingletonLog.getInstance().log;
    private static final Connection conn = SingletonConn.getInstance().conn;
    String tradeDateTime;
    int SimulateId;
    SimulateTrade simulateTrade;

    public StopPositionJob(String _tradeDateTime, int _SimulateId) {
        tradeDateTime = _tradeDateTime;
        SimulateId = _SimulateId;
    }

    public ArrayList<HashMap<String, String>> runStopPositionJob() {
//        ExecutorService singleThreadExecutor = Executors.newSingleThreadExecutor();
//        singleThreadExecutor.submit(() -> {
        log.info("启动平仓任务");
        return StopPosition();
//        });
    }

    private ArrayList<HashMap<String, String>> StopPosition() {
        GetFinanceData getFinanceData = new GetFinanceData();
        IndicatorsUtil indicatorsUtil = new IndicatorsUtil();
        simulateTrade = new SimulateTrade(SimulateId, tradeDateTime);
        String LastTradeDay = getFinanceData.getTradeDayOffset(DateUtil.parseDate(tradeDateTime), -2);

        ArrayList<HashMap<String, String>> StopList = new ArrayList<>();
        Core core = new Core();
        try {
            List<Entity> positions = SqlExecutor.query(conn, "select * from t_simulate_position where simulate_id = " + SimulateId, new EntityListHandler());
            for (Entity position : positions) {
                String dominant_code = position.getStr("dominant_code");
                //如果当前品种不在交易时间范围内，则不考虑平仓
                if (!isTradeTime(DateUtil.parseDateTime(tradeDateTime), dominant_code))
                    continue;

                ArrayList<HashMap<String, String>> Price = getFinanceData.GetPricePeriod(dominant_code, 80, "5m", LastTradeDay, tradeDateTime);
                Entity afstop = SqlExecutor.query(conn, "select * from t_simulate_afstop where simulate_id = "
                        + SimulateId + " and dominant_code='" + dominant_code + "' limit 1", new EntityListHandler()).get(0);
                double[] close = QuantUtil.getDataList(Price, "close");
                double[] open = QuantUtil.getDataList(Price, "open");

                double[] ma5 = indicatorsUtil.MA(close, 5, 5);
                double[] ma10 = indicatorsUtil.MA(close, 10, 5);
                double[] ma15 = indicatorsUtil.MA(close, 15, 5);

                ArrayList<double[]> aMacd = indicatorsUtil.MACD(close, 12, 26, 9);
                double[] diff = aMacd.get(0);
                double[] dea = aMacd.get(1);
                double[] macd = aMacd.get(2);

                double tradePrice = Double.parseDouble(getFinanceData.getCurrentPrice(dominant_code,tradeDateTime));
                if (position.getStr("type").equals("BK")) {
                    boolean condition1 = ma5[0] < ma15[0]; //平仓条件1：5均线死叉15均线
                    boolean condition2 = (open[1] - tradePrice) / open[1] > 0.004;  //平仓条件2：最近2根K线暴跌0.6%
                    boolean condition3 = tradePrice < position.getDouble("price") * 0.998; //平仓条件4：开盘价损失超过0.4%
                    boolean condition4 = diff[0] < dea[0];
                    boolean condition5 = macd[0] < macd[1] & macd[1] < macd[2] & macd[2] < macd[3] & macd[0] < close[0] * 0.0007;
                    if (condition1 | condition2 | condition3 | condition4 | condition5) {
                        double profit = simulateTrade.ClosePosition(dominant_code);
                        HashMap<String, String> hm = new HashMap<>();
                        hm.put("dominant_code", dominant_code);
                        hm.put("price", String.valueOf(tradePrice));
                        hm.put("type", "SP");
                        hm.put("isYest", position.getStr("isYest"));
                        hm.put("vol", position.getStr("amount"));
                        hm.put("profit", String.valueOf(profit));
                        StopList.add(hm);
                        makeBreaker(dominant_code);
                        log.info("平仓：" + dominant_code);
                    }
                } else {
                    boolean condition1 = ma5[0] > ma15[0];
                    boolean condition2 = (tradePrice - open[1]) / open[1] > 0.004;
                    //boolean condition3 = close[0] > 1.006 * afstop.getDouble("peak");
                    //boolean condition5 = close[0] > position.getDouble("price") * 1.002 & afstop.getInt("count") > 5;
                    boolean condition3 = tradePrice > position.getDouble("price") * 1.002;
                    boolean condition4 = diff[0] > dea[0];
                    boolean condition5 = macd[0] > macd[1] & macd[1] > macd[2] & macd[2] > macd[3] & macd[0] > close[0] * -0.0007;
                    if (condition1 | condition2 | condition3 | condition4 | condition5) {
                        double profit = simulateTrade.ClosePosition(dominant_code);
                        HashMap<String, String> hm = new HashMap<>();
                        hm.put("dominant_code", dominant_code);
                        hm.put("price", String.valueOf(tradePrice));
                        hm.put("type", "SP");
                        hm.put("isYest", position.getStr("isYest"));
                        hm.put("vol", position.getStr("amount"));
                        hm.put("profit", String.valueOf(profit));
                        StopList.add(hm);
                        makeBreaker(dominant_code);
                        log.info("平仓：" + dominant_code);
                    }
                }
            }
        } catch (SQLException e) {
            log.error(e);
        }
        return StopList;
    }

    private void makeBreaker(String dominant_code) {
        try {
            //这里的熔断时间逻辑是有问题的，不能保证真的熔断，以后改
            //另外，反手应该是允许的，但是这里也没有做反手判断。不过在我目前的策略里，一般不会出现反手，先不考虑。
            DateTime dtBreak = DateUtil.parseDateTime(tradeDateTime).offset(DateField.MINUTE, 10);
            SqlExecutor.execute(conn, "insert into t_simulate_breaker values(null," + SimulateId + ",'"
                    + dominant_code + "','closestop','" + tradeDateTime + "','" + dtBreak.toString() + "')");
        } catch (SQLException e) {
            log.error(e);
        }
    }

}
