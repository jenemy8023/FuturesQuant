package com.skyworker.futuresquant.strategys.MA5MACDDual;


import cn.hutool.core.date.DateUtil;
import cn.hutool.db.Entity;
import cn.hutool.db.handler.EntityListHandler;
import cn.hutool.db.sql.SqlExecutor;
import cn.hutool.log.Log;
import com.skyworker.futuresquant.quant.GetFinanceData;
import com.skyworker.futuresquant.utils.SingletonConn;
import com.skyworker.futuresquant.utils.SingletonLog;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static java.math.BigDecimal.ROUND_HALF_DOWN;

public class BeforeOpenJob {
    private static final Log log = SingletonLog.getInstance().log;
    private static final Connection conn = SingletonConn.getInstance().conn;
    int SimulateId;
    private String TradeDay;

    public BeforeOpenJob(String _TradeDay, int _SimulateId) {
        TradeDay = _TradeDay;
        SimulateId = _SimulateId;
    }

    public void runBeforeOpenJob() {
//        ExecutorService singleThreadExecutor = Executors.newSingleThreadExecutor();
//        singleThreadExecutor.submit(() -> {
        log.info("启动盘前任务，当前工作日：" + TradeDay);
        log.info("更新主力期货，设置可交易期货");
        List<Entity> le = getDominantFutures();
        updateDominantFutures(le);
        log.info("完成更新设置");

//        });
    }

    private List<Entity> getDominantFutures() {
        GetFinanceData getFinanceData = new GetFinanceData();
        List<Entity> DominantList = new ArrayList<>();
        try {
            List<Entity> futures = SqlExecutor.query(conn, "select * from t_futures_varieties", new EntityListHandler());
            String LastTradeDay = getFinanceData.getLastTradeDay(DateUtil.parse(TradeDay));
            //Db.use().findAll("t_futures_varieties");
            for (Entity entity : futures) {
                String DominantCode = getFinanceData.getDominantFuture(entity.getStr("code"), TradeDay);
                HashMap<String, String> bar = getFinanceData.GetPrice(DominantCode, 1, "1d", LastTradeDay).get(0);
                BigDecimal money = new BigDecimal(bar.get("money"));
                BigDecimal price = new BigDecimal(bar.get("close"));
                BigDecimal lot_amount = new BigDecimal(entity.getStr("lot_amount"));
                BigDecimal margin = new BigDecimal(entity.getStr("margin"));
                BigDecimal all_margin = price.multiply(lot_amount).multiply(margin).divide(new BigDecimal(100), RoundingMode.HALF_DOWN);

                Entity entity1 = new Entity();
                entity1.put("code", entity.getStr("code"));
                entity1.put("dominant_code", DominantCode);
                entity1.put("price", price);
                entity1.put("money", money);
                entity1.put("margin", all_margin);
                entity1.put("date", TradeDay);
                entity1.put("is_trade", 0);
                DominantList.add(entity1);
            }
        } catch (SQLException e) {
            log.error(e);
        }
        return DominantList;
    }

    private void updateDominantFutures(List<Entity> le) {
        try {
            for (Entity entity : le) {
                List<Entity> entityList = SqlExecutor.query(conn,
                        "select * from t_futures_dominant where code = ? and simulate_id = ?", new EntityListHandler()
                        , entity.getStr("code"), SimulateId);
                String dominantchangeday = SqlExecutor.query(conn, "select * from t_sys_dominantchange where code = '"
                                + entity.getStr("code") + "' and tradeday<='" + TradeDay + "' order by tradeday desc limit 1"
                        , new EntityListHandler()).get(0).getStr("tradeday");

                //主力合约在记录表中不存在
                if (entityList.size() == 0) {
                    SqlExecutor.execute(conn, "insert into t_futures_dominant values(?,?,?,?,?,?,?,?)"
                            , entity.getStr("code"), entity.getStr("dominant_code"), entity.getStr("price")
                            , entity.getStr("money"), entity.getStr("margin"), dominantchangeday, 0, SimulateId);
                }
                //主力合约已变更
                else if (entityList.size() == 1 & !entityList.get(0).getStr("dominant_code").equals(entity.getStr("dominant_code"))) {
                    SqlExecutor.execute(conn, "update t_futures_dominant set dominant_code=?,price=?,money=?,margin=?,date=? where code=? and simulate_id=?"
                            , entity.getStr("dominant_code"), entity.getStr("price")
                            , entity.getStr("money"), entity.getStr("margin"), dominantchangeday, entity.getStr("code"), SimulateId);
                } else { //主力合约未变更
                    SqlExecutor.execute(conn, "update t_futures_dominant set price=?,money=?,margin=? where code=? and simulate_id=?"
                            , entity.getStr("price"), entity.getStr("money"), entity.getStr("margin"), entity.getStr("code"), SimulateId);
                }
            }
            //如果交易金额大于100亿且每手保证金小于10000 才列入可交易名单。
            SqlExecutor.execute(conn, "update t_futures_dominant set is_trade=0 where simulate_id=?", SimulateId);
            SqlExecutor.execute(conn, "update t_futures_dominant set is_trade=1 where margin<10000 and money>15000000000 and simulate_id=?", SimulateId);
            SqlExecutor.execute(conn, "update t_futures_dominant set is_trade=0 where code not in(select code from t_futures_varieties where night_time='0' or night_time='2300') and simulate_id=?", SimulateId);

            //刚移仓的不进交易
            GetFinanceData getFinanceData = new GetFinanceData();
            String LastTradeDay = getFinanceData.getLastTradeDay(DateUtil.parse(TradeDay));
            LastTradeDay = getFinanceData.getLastTradeDay(DateUtil.parse(LastTradeDay));
            SqlExecutor.execute(conn, "update t_futures_dominant set is_trade=0 where date>'" + LastTradeDay + "' and simulate_id=" + SimulateId);
        } catch (SQLException e) {
            log.error(e);
        }

    }
}
