package com.skyworker.futuresquant.strategys;

import cn.hutool.core.date.DateTime;
import com.skyworker.futuresquant.strategys.MA5MACDDualCloseInDay.BeforeOpenJob;

public class BeforeOpen {
    private DateTime tradeDate;
    private int SimulateId;
    private int StrategyId;

    public BeforeOpen(DateTime _tradeDate, int _SimulateId, int _StrategyId) {
        tradeDate = _tradeDate;
        SimulateId = _SimulateId;
        StrategyId = _StrategyId;
    }

    public void runBeforeOpen() {

        switch (StrategyId) {
            case 1: {
                com.skyworker.futuresquant.strategys.MultiPeriodMA.BeforeOpenJob beforeOpenJob;
                beforeOpenJob = new com.skyworker.futuresquant.strategys.MultiPeriodMA.BeforeOpenJob(tradeDate.toString("yyyy-MM-dd"), SimulateId);
                beforeOpenJob.runBeforeOpenJob();
                break;
            }
            case 2: {
                com.skyworker.futuresquant.strategys.MultiPeriodMALightStop.BeforeOpenJob beforeOpenJob;
                beforeOpenJob = new com.skyworker.futuresquant.strategys.MultiPeriodMALightStop.BeforeOpenJob(tradeDate.toString("yyyy-MM-dd"), SimulateId);
                beforeOpenJob.runBeforeOpenJob();
                break;
            }
            case 3: {
                com.skyworker.futuresquant.strategys.MA5minDropLight.BeforeOpenJob beforeOpenJob;
                beforeOpenJob = new com.skyworker.futuresquant.strategys.MA5minDropLight.BeforeOpenJob(tradeDate.toString("yyyy-MM-dd"), SimulateId);
                beforeOpenJob.runBeforeOpenJob();
                break;
            }
            case 4: {
                com.skyworker.futuresquant.strategys.MA5Based.BeforeOpenJob beforeOpenJob;
                beforeOpenJob = new com.skyworker.futuresquant.strategys.MA5Based.BeforeOpenJob(tradeDate.toString("yyyy-MM-dd"), SimulateId);
                beforeOpenJob.runBeforeOpenJob();
                break;
            }
            case 5: {
                com.skyworker.futuresquant.strategys.MA5MACDDual.BeforeOpenJob beforeOpenJob;
                beforeOpenJob = new com.skyworker.futuresquant.strategys.MA5MACDDual.BeforeOpenJob(tradeDate.toString("yyyy-MM-dd"), SimulateId);
                beforeOpenJob.runBeforeOpenJob();
                break;
            }
            case 6: {
                BeforeOpenJob beforeDayOpenJob;
                beforeDayOpenJob = new BeforeOpenJob(tradeDate.toString("yyyy-MM-dd"), SimulateId);
                beforeDayOpenJob.runBeforeOpenJob();
                break;
            }
        }

    }


}
