package com.skyworker.futuresquant.singlerun;

import cn.hutool.core.date.DateTime;
import cn.hutool.core.date.DateUtil;
import cn.hutool.db.Entity;
import cn.hutool.db.handler.EntityListHandler;
import cn.hutool.db.sql.SqlExecutor;
import cn.hutool.log.Log;
import com.skyworker.futuresquant.quant.GetFinanceData;
import com.skyworker.futuresquant.quant.SimulateTrade;
import com.skyworker.futuresquant.utils.IndicatorsUtil;
import com.skyworker.futuresquant.utils.QuantUtil;
import com.skyworker.futuresquant.utils.SingletonConn;
import com.skyworker.futuresquant.utils.SingletonLog;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static com.skyworker.futuresquant.utils.QuantUtil.isTradeTime;

public class SAStrategy {
    private static final Log log = SingletonLog.getInstance().log;
    private static final Connection conn = SingletonConn.getInstance().conn;
    private final String tradeDate;
    private final int SimulateId;
    private final String Code = "SA";
    private final GetFinanceData getFinanceData;
    int breakTimes = 0;
    int breakToday = 0;
    private String dominant_code;

    public SAStrategy(String _tradeDate, int _SimulateId) {
        tradeDate = _tradeDate;
        SimulateId = _SimulateId;
        getFinanceData = new GetFinanceData();
    }

    public void runSAStrategy() {
        try {
            log.info("开盘前处理");
            runBeforeDayOpen();
            log.info("开盘处理");
            runOpenStart(true);
            log.info("日盘交易");
            runDayTrade(true);
            log.info("夜盘开盘处理");
            runOpenStart(false);
            log.info("夜盘交易");
            runDayTrade(false);
        } catch (SQLException e) {
            log.error(e);
        }

    }

    /*
        日盘开盘前，获取主力期货
     */
    private void runBeforeDayOpen() throws SQLException {
        dominant_code = getFinanceData.getDominantFuture(Code, tradeDate);
    }

    /*
        日盘开盘，主力期货变更处理，平仓处理。
     */
    private void runOpenStart(Boolean isDay) throws SQLException {
        //主力期货是否变更
        String tradeDateTime;
        if (isDay) {
            tradeDateTime = tradeDate + " 09:00:02";
        } else {
            tradeDateTime = tradeDate + " 21:00:02";
        }
        String sql = "select * from t_simulate_position where simulate_id=" + SimulateId + " and code='" + Code + "'";
        List<Entity> positions = SqlExecutor.query(conn, sql, new EntityListHandler());
        if (positions.size() > 0) {
            String old_dominant_code = positions.get(0).getStr("dominant_code");
            if (!positions.get(0).getStr("dominant_code").equals(dominant_code)) {
                SimulateTrade simulateTrade = new SimulateTrade(SimulateId, tradeDateTime);
                if (positions.get(0).getStr("type").equals("BK"))
                    simulateTrade.ClosePosition(old_dominant_code, "SP", positions.get(0).getBigDecimal("amount"));
                else
                    simulateTrade.ClosePosition(old_dominant_code, "BP", positions.get(0).getBigDecimal("amount"));
            }
        } else {
            //如果有持仓，看是否需要平仓
            CheckClosePosition(tradeDateTime);
        }
    }

    private void runDayTrade(Boolean isDay) throws SQLException {
        String type;
        if (isDay) {
            type = "5mDay";
        } else {
            type = "5mNight";
        }
        List<Entity> le = SqlExecutor.query(conn, "select * from t_simulate_trigger where type='" + type + "' order by `trigger`", new EntityListHandler());
        for (Entity entity : le) {
            DateTime TriggerTime = DateUtil.parseDateTime(tradeDate + " " + entity.getStr("trigger"));
            log.info("当前处理时间：" + TriggerTime);
            String sql = "select * from t_simulate_position where simulate_id=" + SimulateId + " and code='" + Code + "'";
            List<Entity> positions = SqlExecutor.query(conn, sql, new EntityListHandler());
            if (positions.size() > 0) {
                CheckClosePosition(TriggerTime.toString("yyyy-MM-dd HH:mm:ss"));
            } else {
                if (breakTimes > 0) {
                    breakTimes = breakTimes - 1;
                } else {
                    CheckOpenPosition(TriggerTime.toString("yyyy-MM-dd HH:mm:ss"));
                }

            }
        }
    }

    private void CheckClosePosition(String tradeDateTime) throws SQLException {
        //如果当前品种不在交易时间范围内，则不考虑平仓
        String sql = "select * from t_simulate_position where simulate_id=" + SimulateId + " and code='" + Code + "'";
        List<Entity> positions = SqlExecutor.query(conn, sql, new EntityListHandler());
        if (positions.size() == 0)
            return;
        Entity position = positions.get(0);

        IndicatorsUtil indicatorsUtil = new IndicatorsUtil();
        String LastTradeDay = getFinanceData.getTradeDayOffset(DateUtil.parseDate(tradeDateTime), -2);

        if (!isTradeTime(DateUtil.parseDateTime(tradeDateTime), dominant_code))
            return;
        ArrayList<HashMap<String, String>> Price = getFinanceData.GetPricePeriod(dominant_code, 80, "5m"
                , LastTradeDay, tradeDateTime);
        double[] close = QuantUtil.getDataList(Price, "close");
        double[] open = QuantUtil.getDataList(Price, "open");
        double[] ma5 = indicatorsUtil.MA(close, 5, 5);
        double[] ma10 = indicatorsUtil.MA(close, 10, 5);
        double[] ma15 = indicatorsUtil.MA(close, 15, 5);

        ArrayList<double[]> aMacd = indicatorsUtil.MACD(close, 12, 26, 9);
        double[] diff = aMacd.get(0);
        double[] dea = aMacd.get(1);
        double[] macd = aMacd.get(2);
        double tradePrice = Double.parseDouble(getFinanceData.getCurrentPrice(dominant_code, tradeDateTime));

        if (position.getStr("type").equals("BK")) {
            boolean condition1 = ma5[0] < ma10[0]; //平仓条件1：5均线死叉15均线
            boolean condition2 = (open[1] - tradePrice) / open[1] > 0.004;  //平仓条件2：最近2根K线暴跌0.6%
            boolean condition3 = tradePrice < position.getDouble("price") * 0.998; //平仓条件4：开盘价损失超过0.4%
            boolean condition4 = diff[0] < dea[0];
            boolean condition5 = diff[0] < 0 & macd[0] < close[0] * 0.001;
            boolean condition6 = close[0] - close[1] < -0.006 * close[0] | close[0] - close[2] < -0.008 * close[0];

            if (condition4 | condition5) {
                SimulateTrade simulateTrade = new SimulateTrade(SimulateId, tradeDateTime);
                double profit = simulateTrade.ClosePosition(dominant_code);
                log.info("平仓：" + dominant_code + ",收益：" + profit);
            } else if (condition6 & close[0] < position.getDouble("price")) {
                SimulateTrade simulateTrade = new SimulateTrade(SimulateId, tradeDateTime);
                double profit = simulateTrade.ClosePosition(dominant_code);
                breakTimes = 5;
                log.info("平仓：" + dominant_code + ",收益：" + profit + " 熔断5次");
            }


        } else {
            boolean condition1 = ma5[0] > ma10[0];
            boolean condition2 = (tradePrice - open[1]) / open[1] > 0.004;
            //boolean condition3 = close[0] > 1.006 * afstop.getDouble("peak");
            //boolean condition5 = close[0] > position.getDouble("price") * 1.002 & afstop.getInt("count") > 5;
            boolean condition3 = tradePrice > position.getDouble("price") * 1.002;
            boolean condition4 = diff[0] > dea[0];
            boolean condition5 = diff[0] > 0 & macd[0] > close[0] * -0.001;
            boolean condition6 = close[0] - close[1] > 0.006 * close[0] | close[0] - close[2] > 0.008 * close[0];

            if (condition4 | condition5) {
                SimulateTrade simulateTrade = new SimulateTrade(SimulateId, tradeDateTime);
                double profit = simulateTrade.ClosePosition(dominant_code);
                log.info("平仓：" + dominant_code + ",收益：" + profit);
            } else if (condition6 & close[0] > position.getDouble("price")) {
                SimulateTrade simulateTrade = new SimulateTrade(SimulateId, tradeDateTime);
                double profit = simulateTrade.ClosePosition(dominant_code);
                breakTimes = 5;
                log.info("平仓：" + dominant_code + ",收益：" + profit + " 熔断5次");
            }

        }
    }

    private void CheckOpenPosition(String tradeDateTime) throws SQLException {
        IndicatorsUtil indicatorsUtil = new IndicatorsUtil();
        String LastTradeDay = tradeDateTime;
        LastTradeDay = getFinanceData.getTradeDayOffset(DateUtil.parseDate(tradeDateTime), -2);

        BigDecimal amount = getTradeVol(tradeDateTime);

        ArrayList<HashMap<String, String>> Price = getFinanceData.GetPricePeriod(dominant_code, 80, "5m"
                , LastTradeDay, tradeDateTime);
        double[] close = QuantUtil.getDataList(Price, "close");
        double[] open = QuantUtil.getDataList(Price, "open");

        double[] ma5 = indicatorsUtil.MA(close, 5, 5);
        double[] ma10 = indicatorsUtil.MA(close, 10, 5);
        double[] ma15 = indicatorsUtil.MA(close, 15, 5);
        double[] ma20 = indicatorsUtil.MA(close, 20, 5);

        ArrayList<double[]> aMacd = indicatorsUtil.MACD(close, 12, 26, 9);
        double[] diff = aMacd.get(0);
        double[] dea = aMacd.get(1);
        double[] macd = aMacd.get(2);

        if (ma5[0] > ma10[0] & ma5[0] > ma15[0]) {
            boolean condition1 = open[0] > ma10[0] & close[0] > ma10[0];
            boolean condition2 = ma5[0] > ma5[1] & ma5[1] > ma5[2] & ma10[0] > ma10[1];
            boolean condition3 = (Math.abs(ma5[0] - ma10[0]) + Math.abs(ma15[0] - ma10[0])
                    + Math.abs(ma15[0] - ma5[0])) / 3 > close[0] * 0.0007;
            boolean condition4 = Math.abs((close[0] - close[1]) / close[0]) < 0.005
                    & Math.abs((close[1] - close[2]) / close[1]) < 0.005
                    & Math.abs((close[2] - close[3]) / close[2]) < 0.005
                    & Math.abs((close[3] - close[4]) / close[3]) < 0.005
                    & Math.abs((close[4] - close[5]) / close[4]) < 0.005;
            if (condition1) {
                boolean condition5 = diff[0] > dea[0] & diff[0] > diff[1] & diff[1] > diff[2];
                boolean condition6 = macd[0] > macd[1] & macd[1] > macd[2];
                boolean condition7 = macd[0] > close[0] * 0.002 | (macd[0] > close[0] * 0.0005 & diff[0] > 0);
                if (condition5 & condition6 & condition7) {
//                    if (!hashLongKBeforeOpen(close, "BK")) {
                    SimulateTrade simulateTrade = new SimulateTrade(SimulateId, tradeDateTime);
                    log.info(tradeDateTime + ": 买入：" + dominant_code + " 买入数量：" + amount);
                    simulateTrade.OpenPosition(dominant_code, "BK", amount);
//                    }

                }
            }
        } else if (ma5[0] < ma10[0] & ma5[0] < ma15[0]) {
            boolean condition1 = open[0] < ma10[0] & close[0] < ma10[0];
            boolean condition2 = ma5[0] < ma5[1] & ma5[1] < ma5[2] & ma10[0] < ma10[1];
            boolean condition3 = (Math.abs(ma5[0] - ma10[0]) + Math.abs(ma15[0] - ma10[0])
                    + Math.abs(ma15[0] - ma5[0])) / 3 > close[0] * 0.0007;
            boolean condition4 = Math.abs((close[0] - close[1]) / close[0]) < 0.005
                    & Math.abs((close[1] - close[2]) / close[1]) < 0.005
                    & Math.abs((close[2] - close[3]) / close[2]) < 0.005
                    & Math.abs((close[3] - close[4]) / close[3]) < 0.005
                    & Math.abs((close[4] - close[5]) / close[4]) < 0.005;
            if (condition1) {
                boolean condition5 = diff[0] < dea[0] & diff[0] < diff[1] & diff[1] < diff[2];
                boolean condition6 = macd[0] < macd[1] & macd[1] < macd[2];
                boolean condition7 = macd[0] < close[0] * -0.002 | (macd[0] < close[0] * -0.0005 & diff[0] < 0);
                if (condition5 & condition6 & condition7) {
//                    if (!hashLongKBeforeOpen(close, "SK")) {
                    SimulateTrade simulateTrade = new SimulateTrade(SimulateId, tradeDateTime);
                    log.info(tradeDateTime + ": 买入：" + dominant_code + " 买入数量：" + amount);
                    simulateTrade.OpenPosition(dominant_code, "SK", amount);
//                    }
                }
            }
        }

    }

    private BigDecimal getTradeVol(String tradeDateTime) throws SQLException {
        String current_price = getFinanceData.getCurrentPrice(dominant_code, tradeDateTime);
        Entity future = SqlExecutor.query(conn, "select * from t_futures_varieties where code='" + Code + "'"
                , new EntityListHandler()).get(0);
        Entity simulateMain = SqlExecutor.query(conn, "select * from t_simulate_main where id = " + SimulateId
                , new EntityListHandler()).get(0);

        double all_margin = Double.parseDouble(current_price) * future.getDouble("lot_amount") * future.getDouble("margin") / 100;
        double amount = simulateMain.getDouble("total_amount") * 0.9 / all_margin;
        return BigDecimal.valueOf((int) amount);
    }

    private boolean hashLongKBeforeOpen(double[] close, String type) {
        //原理：最近3根K线中有任意一根超过0.006
        double range1 = (close[0] - close[1]) / close[1];
        double range2 = (close[1] - close[2]) / close[2];
        double range3 = (close[2] - close[3]) / close[3];

        double range4 = (close[0] - close[2]) / close[2];
        double range5 = (close[1] - close[3]) / close[3];
        double standard1 = 0.006;
        double standard2 = 0.008;
        if (type.equals("BK")) {
            return range1 > standard1 | range2 > standard1 | range3 > standard1 | range4 > standard2 | range5 > standard2;
        } else {
            return range1 * -1 > standard1 | range2 * -1 > standard1 | range3 * -1 > standard1 | range4 * -1 > standard2 | range5 * -1 > standard2;
        }
    }

}


