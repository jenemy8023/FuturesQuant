package com.skyworker.futuresquant.singlerun;

public class SingleRunRouter {
    private static int simulatedId;
    private static String variety;
    private static String tradeDate;


    public SingleRunRouter(int _simulatedId, String _Variety, String _TradeDate) {
        simulatedId = _simulatedId;
        variety = _Variety;
        tradeDate = _TradeDate;
    }

    public void SingleRun() {
        switch (variety) {
            case "SA":
                SAStrategy saStrategy = new SAStrategy(tradeDate, simulatedId);
                saStrategy.runSAStrategy();
                return;
            case "SA1m":
                SA1mStrategy sa1mStrategy = new SA1mStrategy(tradeDate, simulatedId);
                sa1mStrategy.runSA1mStrategy();

        }
    }
}
