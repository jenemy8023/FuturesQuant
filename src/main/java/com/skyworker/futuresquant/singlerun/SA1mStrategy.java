package com.skyworker.futuresquant.singlerun;

import cn.hutool.core.date.DateTime;
import cn.hutool.core.date.DateUtil;
import cn.hutool.db.Entity;
import cn.hutool.db.handler.EntityListHandler;
import cn.hutool.db.sql.SqlExecutor;
import cn.hutool.log.Log;
import com.skyworker.futuresquant.quant.GetFinanceData;
import com.skyworker.futuresquant.quant.SimulateTrade;
import com.skyworker.futuresquant.utils.IndicatorsUtil;
import com.skyworker.futuresquant.utils.QuantUtil;
import com.skyworker.futuresquant.utils.SingletonConn;
import com.skyworker.futuresquant.utils.SingletonLog;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static com.skyworker.futuresquant.utils.QuantUtil.isTradeTime;

public class SA1mStrategy {
    private static final Log log = SingletonLog.getInstance().log;
    private static final Connection conn = SingletonConn.getInstance().conn;
    private final String tradeDate;
    private final int SimulateId;
    private final String Code = "SA";
    private final GetFinanceData getFinanceData;

    int macdup = 0; //当前macdup上升为1 下降为-1
    int macdcount = 0; //当前颜色macd bar的数量，正数为多头，负数为空头。
    double macdPeak = 0; //当前颜色的macd极值，正数为最大值 负数最小值
    double avg = 0; //当前avg值，avg为diff和dea的平均值。
    int breakToTrendChange = 0; //是否停止开仓，直到avg反转。（包含未突破的调整）
    int countPositon = 0; //持仓到现在的bar的数量
    int macdCountPositon = 0; //持仓的macd到现在的数量，如果macd已经转方向，则为-1
    double maxAvgPosition = 0; //持仓到现在Avg的最大（BK）或最小值（SK）。
    double maxPricePosition = 0; //持仓到现在Avg的最大（BK）或最小值（SK）。
    int TrendType = 0;
    int longbar = 0;

    Entity position = null; //持仓的标的情况。

    private double openLine = 0.0004;
    private double openMacd = 0.0008;
    private double MiddleLine = 0.0025;
    private double HighLine = 0.0035;
    private double closeLine = 0.00015;
    private double closeMacd = 0.0018;
    private double LostStop3 = -0.005;
    private double LostStop2 = -0.003;
    private double LostStop1 = -0.0015;
    private int longbarCount = 3;//最近5个周期有超长bar 停止开仓
    private double longbarStandard = 0.008;//判断超长的标准：单根大约8% 暂定
    private int breakCount = 10;
    private String dominant_code;

    public SA1mStrategy(String _tradeDate, int _SimulateId) {
        tradeDate = _tradeDate;
        SimulateId = _SimulateId;
        getFinanceData = new GetFinanceData();
    }

    public void runSA1mStrategy() {
        try {
            log.info("开盘前处理");
            runBeforeDayStart();
            log.info("开盘处理");
            runOpenStart(true);
            log.info("日盘交易");
            runDayTrade(true);
            log.info("夜盘开盘处理");
            runOpenStart(false);
            log.info("夜盘交易");
            runDayTrade(false);
            log.info("收盘处理");
            runAfterDayEnd();
        } catch (SQLException e) {
            log.error(e);
        }
    }

    /*
    日盘开盘前，获取主力期货
 */
    private void runBeforeDayStart() throws SQLException {
        dominant_code = getFinanceData.getDominantFuture(Code, tradeDate);
        LoadRunTimeParam();
    }

    private void runAfterDayEnd() throws SQLException {
        SaveRunTimeParam();
    }

    /*
        日盘开盘，主力期货变更处理，平仓处理。
     */
    private void runOpenStart(Boolean isDay) throws SQLException {
        //主力期货是否变更

        String tradeDateTime;
        if (isDay) {
            tradeDateTime = tradeDate + " 09:00:02";
        } else {
            tradeDateTime = tradeDate + " 21:00:02";
        }
        String sql = "select * from t_simulate_position where simulate_id=" + SimulateId + " and code='" + Code + "'";
        List<Entity> positions = SqlExecutor.query(conn, sql, new EntityListHandler());
        if (positions.size() > 0) {
            String old_dominant_code = positions.get(0).getStr("dominant_code");
            if (!positions.get(0).getStr("dominant_code").equals(dominant_code)) {
                SimulateTrade simulateTrade = new SimulateTrade(SimulateId, tradeDateTime);
                if (positions.get(0).getStr("type").equals("BK"))
                    simulateTrade.ClosePosition(old_dominant_code, "SP", positions.get(0).getBigDecimal("amount"));
                else
                    simulateTrade.ClosePosition(old_dominant_code, "BP", positions.get(0).getBigDecimal("amount"));
            }
        }
    }

    private void runDayTrade(Boolean isDay) throws SQLException {
        String type;
        if (isDay) {
            type = "1mDay";
        } else {
            type = "1mNight";
        }
        List<Entity> le = SqlExecutor.query(conn, "select * from t_simulate_trigger where type='" + type + "' order by `trigger`", new EntityListHandler());
        for (Entity entity : le) {
            String sTriggerTime = tradeDate + " " + entity.getStr("trigger");
            DateTime TriggerTime = DateUtil.parseDateTime(sTriggerTime);
            if (!isTradeTime(TriggerTime, dominant_code))
                continue;
            log.info("当前处理时间：" + sTriggerTime);
            IndicatorsUtil indicatorsUtil = new IndicatorsUtil();
            String LastTradeDay = getFinanceData.getLastTradeDay(DateUtil.parse(tradeDate));
            ArrayList<HashMap<String, String>> Price = getFinanceData.GetPricePeriod(dominant_code, 100, "1m"
                    , LastTradeDay, sTriggerTime);
            double[] close = QuantUtil.getDataList(Price, "close");
            ArrayList<double[]> aMacd = indicatorsUtil.MACD(close, 12, 26, 9);
            String sql = "select * from t_simulate_position where simulate_id=" + SimulateId + " and code='" + Code + "'";
            List<Entity> positions = SqlExecutor.query(conn, sql, new EntityListHandler());
            if (positions.size() > 0) {
                position = positions.get(0);
            } else {
                position = null;
            }
            updateRuntimeParam(close, aMacd, Price);
            if (positions.size() > 0) {
                if (CheckClosePosition(sTriggerTime, Price, aMacd))
                    CheckOpenPosition(sTriggerTime, Price, aMacd);
            } else {
                CheckOpenPosition(sTriggerTime, Price, aMacd);
            }
        }
    }

    private void CheckOpenPosition(String tradeDateTime, ArrayList<HashMap<String, String>> Price, ArrayList<double[]> aMacd) throws SQLException {
        BigDecimal amount = getTradeVol(tradeDateTime);
        double[] close = QuantUtil.getDataList(Price, "close");
        double[] macd = aMacd.get(2);
        int buyType = 0;

        if (avg > close[0] * openLine & macdup == 1 & macd[0] > openMacd * close[0])
            buyType = 1;
        if (avg < close[0] * -1 * openLine & macdup == -1 & macd[0] < -1 * openMacd * close[0])
            buyType = -1;

        if (buyType == 1 & breakToTrendChange == 0) {
            SimulateTrade simulateTrade = new SimulateTrade(SimulateId, tradeDateTime);
            log.info(tradeDateTime + ": 买入：" + dominant_code + " 买入数量：" + amount);
            TrendType = 1;
            simulateTrade.OpenPosition(dominant_code, "BK", amount, "");
        } else if (buyType == -1 & breakToTrendChange == 0) {
            SimulateTrade simulateTrade = new SimulateTrade(SimulateId, tradeDateTime);
            log.info(tradeDateTime + ": 买入：" + dominant_code + " 买入数量：" + amount);
            simulateTrade.OpenPosition(dominant_code, "SK", amount, "");
            TrendType = -1;
        }
    }

    private boolean CheckClosePosition(String tradeDateTime, ArrayList<HashMap<String, String>> Price, ArrayList<double[]> aMacd) {
        //如果当前品种不在交易时间范围内，则不考虑平仓
        double[] close = QuantUtil.getDataList(Price, "close");
        boolean isClose = false;
        String note = "";
        double[] macd = aMacd.get(2);
        double profitRate;

        //基础avg止损
        if (position.getStr("type").equals("BK")) {
            profitRate = (close[0] - position.getDouble("price")) / position.getDouble("price");
            if (avg < close[0] * closeLine) {
                if (avg > 0) //防止重复开仓
                    breakToTrendChange = 1;
                isClose = true;
            }
            note = "TrendType=" + TrendType + ",avg=" + avg + ",profitRate=" + profitRate;
        } else {
            profitRate = (position.getDouble("price") - close[0]) / position.getDouble("price");
            if (avg > -1 * close[0] * closeLine) {
                if (avg < 0) //防止重复开仓
                    breakToTrendChange = -1;
                isClose = true;
            }
            note = "TrendType=" + TrendType + ",avg=" + avg + ",profitRate=" + profitRate;
        }

        if (position.getStr("type").equals("BK")) {
            if (countPositon < 20  & macdup==-1 & profitRate < -0.002) {
                if (avg > 0) //防止重复开仓
                    breakToTrendChange = 1;
                isClose = true;
                note = "TrendType=" + TrendType + ",avg=" + avg + ",profitRate=" + profitRate + ",countPositon=" + countPositon;
            }
        } else {
            if (countPositon < 20 & macdup==1 & profitRate < -0.002) {
                if (avg < 0) //防止重复开仓
                    breakToTrendChange = -1;
                isClose = true;
                note = "TrendType=" + TrendType + ",avg=" + avg + ",profitRate=" + profitRate + ",countPositon=" + countPositon;
            }
        }


        if (isClose) {
            SimulateTrade simulateTrade = new SimulateTrade(SimulateId, tradeDateTime);
            double profit = simulateTrade.ClosePosition(dominant_code, note);
            log.info("平仓：" + dominant_code + ",收益：" + profit);
            countPositon = 0;
            maxAvgPosition = 0;
            macdCountPositon = 0;
            TrendType = 0;
            maxPricePosition = 0;
            return true;
        } else
            return false;
    }

    private BigDecimal getTradeVol(String tradeDateTime) throws SQLException {
        String current_price = getFinanceData.getCurrentPrice(dominant_code, tradeDateTime);
        Entity future = SqlExecutor.query(conn, "select * from t_futures_varieties where code='" + Code + "'"
                , new EntityListHandler()).get(0);
        Entity simulateMain = SqlExecutor.query(conn, "select * from t_simulate_main where id = " + SimulateId
                , new EntityListHandler()).get(0);

        double all_margin = Double.parseDouble(current_price) * future.getDouble("lot_amount") * future.getDouble("margin") / 100;
        double amount = simulateMain.getDouble("total_amount") * 0.9 / all_margin;
        return BigDecimal.valueOf((int) amount);
    }

    private void updateRuntimeParam(double[] close, ArrayList<double[]> aMacd, ArrayList<HashMap<String, String>> Price) {
        double[] diff = aMacd.get(0);
        double[] dea = aMacd.get(1);
        avg = (2 * diff[0] + dea[0]) / 3;
        updateMacdAnalysis(aMacd);
        updatePositionStatus(close, aMacd);
        updateBreakToTrendChange();
        updateLongBar(Price);
    }

    private void updateMacdAnalysis(ArrayList<double[]> aMacd) {
        double[] macd = aMacd.get(2);
        if (macd[0] > macd[1] & macd[1] > macd[2] & macd[2] > macd[3]) {
            macdup = 1;
        } else if (macd[0] < macd[1] & macd[1] < macd[2] & macd[2] < macd[3]) {
            macdup = -1;
        } else
            macdup = 0;

        if (macd[0] > 0) {
            if (macdcount >= 0) {
                macdcount = macdcount + 1;
            } else
                macdcount = 1;
            if (macdPeak >= 0) {
                if (macdPeak < macd[0])
                    macdPeak = macd[0];
            } else {
                macdPeak = macd[0];
            }
        } else {
            if (macdcount <= 0) {
                macdcount = macdcount - 1;
            } else
                macdcount = -1;
            if (macdPeak <= 0) {
                if (macdPeak > macd[0])
                    macdPeak = macd[0];
            } else {
                macdPeak = macd[0];
            }
        }
    }

    private void updatePositionStatus(double[] close, ArrayList<double[]> aMacd) {
        double[] macd = aMacd.get(2);

        if (position != null) {
            if (position.getStr("type").equals("BK")) {
                if (countPositon >= 0)
                    countPositon = countPositon + 1;
                else
                    countPositon = 1;
                if (avg > maxAvgPosition)
                    maxAvgPosition = avg;
                if (close[0] > maxPricePosition)
                    maxPricePosition = close[0];
                if (macdCountPositon != -1) {
                    if (macd[0] > 0)
                        macdCountPositon = macdCountPositon + 1;
                    else
                        macdCountPositon = -1;
                }
                if (avg > MiddleLine * close[0] & TrendType <= 1) {
                    TrendType = 2;
                } else if (avg > HighLine * close[0] & TrendType <= 2) {
                    TrendType = 3;
                }
            } else {
                if (countPositon <= 0)
                    countPositon = countPositon - 1;
                else
                    countPositon = -1;
                if (avg < maxAvgPosition)
                    maxAvgPosition = avg;
                if (close[0] < maxPricePosition)
                    maxPricePosition = close[0];
                if (macdCountPositon != -1) {
                    if (macd[0] < 0)
                        macdCountPositon = macdCountPositon + 1;
                    else
                        macdCountPositon = -1;
                }
                if (avg < -1 * MiddleLine * close[0] & TrendType >= -1) {
                    TrendType = -2;
                } else if (avg < -1 * HighLine * close[0] & TrendType >= -2) {
                    TrendType = -3;
                }
            }
        } else {
            countPositon = 0;
            maxAvgPosition = 0;
            macdCountPositon = 0;
            TrendType = 0;
            maxPricePosition = 0;
        }
    }

    private void updateBreakToTrendChange() {
        if (breakToTrendChange > 0)
            breakToTrendChange = breakToTrendChange + 1;
        if (breakToTrendChange < 0)
            breakToTrendChange = breakToTrendChange - 1;

        if (avg > 0) {
            if (breakToTrendChange < 0 | breakToTrendChange >= breakCount) {
                breakToTrendChange = 0;
            }
        } else {
            if (breakToTrendChange > 0 | breakToTrendChange <= -1 * breakCount) {
                breakToTrendChange = 0;
            }
        }
    }

    private void SaveRunTimeParam() throws SQLException {
        String sParam = "breakToTrendChange=" + breakToTrendChange + ",macdup=" + macdup
                + ",macdcount=" + macdcount + ",macdPeak=" + macdPeak + ",avg=" + avg
                + ",countPositon=" + countPositon + ",macdCountPositon=" + macdCountPositon
                + ",maxAvgPosition=" + maxAvgPosition + ",maxPricePosition=" + maxPricePosition
                + ",TrendType=" + TrendType + ",longbar=" + longbar;

        String sql = "select * from t_simulate_param where param_type='runtime' and simulate_id=" + SimulateId;
        List<Entity> param = SqlExecutor.query(conn, sql, new EntityListHandler());
        if (param.size() == 0) {
            sql = "insert into t_simulate_param values(null," + SimulateId + ",'runtime','" + sParam + "')";
        } else {
            sql = "update t_simulate_param set param='" + sParam + "' where param_type='runtime' and simulate_id=" + SimulateId;
        }
        SqlExecutor.execute(conn, sql);
    }

    private void LoadRunTimeParam() throws SQLException {
        String sql = "select * from t_simulate_param where param_type='runtime' and simulate_id=" + SimulateId;
        List<Entity> param = SqlExecutor.query(conn, sql, new EntityListHandler());
        if (param.size() > 0) {
            String sParam = param.get(0).getStr("param");
            String[] Params = sParam.split(",");
            for (String sItem : Params) {
                String[] sContent = sItem.split("=");
                if (sContent[0].equals("macdup")) {
                    macdup = Integer.parseInt(sContent[1]);
                    continue;
                }
                if (sContent[0].equals("macdcount")) {
                    macdcount = Integer.parseInt(sContent[1]);
                    continue;
                }
                if (sContent[0].equals("macdPeak")) {
                    macdPeak = Double.parseDouble(sContent[1]);
                    continue;
                }
                if (sContent[0].equals("avg")) {
                    avg = Double.parseDouble(sContent[1]);
                    continue;
                }
                if (sContent[0].equals("breakToTrendChange")) {
                    breakToTrendChange = Integer.parseInt(sContent[1]);
                    continue;
                }
                if (sContent[0].equals("countPositon")) {
                    countPositon = Integer.parseInt(sContent[1]);
                    continue;
                }
                if (sContent[0].equals("macdCountPositon")) {
                    macdCountPositon = Integer.parseInt(sContent[1]);
                    continue;
                }
                if (sContent[0].equals("maxAvgPosition")) {
                    maxAvgPosition = Double.parseDouble(sContent[1]);
                    continue;
                }
                if (sContent[0].equals("TrendType")) {
                    TrendType = Integer.parseInt(sContent[1]);
                    continue;
                }
                if (sContent[0].equals("longbar")) {
                    longbar = Integer.parseInt(sContent[1]);
                    continue;
                }
                if (sContent[0].equals("maxPricePosition")) {
                    maxPricePosition = Double.parseDouble(sContent[1]);
                }


            }
        }
    }

    private void updateLongBar(ArrayList<HashMap<String, String>> Price) {
        double[] close = QuantUtil.getDataList(Price, "close");
        double[] open = QuantUtil.getDataList(Price, "open");
        if ((close[0] - open[0]) / open[0] > longbarStandard) {
            longbar = 1;
            return;
        } else if ((open[0] - close[0]) / open[0] > longbarStandard) {
            longbar = -1;
            return;
        }
        if (longbar > 0) {
            if (longbar >= longbarCount)
                longbar = 0;
            else
                longbar++;
        } else if (longbar < 0) {
            if (longbar <= -1 * longbarCount)
                longbar = 0;
            else
                longbar--;
        }
    }
}
