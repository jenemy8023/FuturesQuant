package com.skyworker.futuresquant.quant;

import cn.hutool.core.date.DateTime;
import cn.hutool.core.date.DateUtil;
import cn.hutool.db.Entity;
import cn.hutool.db.handler.EntityListHandler;
import cn.hutool.db.sql.SqlExecutor;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import cn.hutool.log.Log;
import com.skyworker.futuresquant.utils.HttpUtil;
import com.skyworker.futuresquant.utils.SingletonConn;
import com.skyworker.futuresquant.utils.SingletonDataAccount;
import com.skyworker.futuresquant.utils.SingletonLog;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import static com.skyworker.futuresquant.utils.QuantUtil.LE2AH;

public class GetFinanceData {
    private static final Log log = SingletonLog.getInstance().log;
    private static final Connection conn = SingletonConn.getInstance().conn;
    private final String url = "https://dataapi.joinquant.com/apis";
    private String mob = "";
    private String pwd = "";
    private String token = "";
    private String tokenDate = "";

    public GetFinanceData() {
//        Setting setting = new Setting("system.setting");
//        mob = setting.getByGroup("mob", "joinquant");
//        pwd = setting.getByGroup("pwd", "joinquant");
        mob = SingletonDataAccount.getInstance().username;
        pwd = SingletonDataAccount.getInstance().password;
    }

    public GetFinanceData(String _mob, String _pwd) {
        mob = _mob;
        pwd = _pwd;
        getToken();
    }

    public void getToken() {
        String _date = DateUtil.today();
        if (!token.equals("") & tokenDate.equals(_date)) return;
        JSONObject jo1 = JSONUtil.createObj();
        jo1.putOnce("method", "get_current_token");
        jo1.putOnce("mob", mob);
        jo1.putOnce("pwd", pwd);
        token = HttpUtil.HttpUtilPost(jo1.toString(), url);
        tokenDate = _date;
    }

    /**
     * 获取股价，时间从大到小排列
     *
     * @param code
     * @param count
     * @param unit
     * @param date
     * @return
     */
    public ArrayList<HashMap<String, String>> GetPrice(String code, int count, String unit, String date) {
        ArrayList<HashMap<String, String>> price = new ArrayList<>();
        if (unit.equals("5m")) {
            try {
                String sql;
                sql = "select * from t_futures_marketdata where dominant_code='" + code + "' and market_time>='" + date
                        + "' order by market_time desc limit " + count + "";
                List<Entity> le = SqlExecutor.query(conn, sql, new EntityListHandler());
                price = LE2AH(le);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        } else if (unit.equals("1m")) {
            try {
                String sql;
                sql = "select * from t_futures_marketdata_1m where dominant_code='" + code + "' and market_time>='" + date
                        + "' order by market_time desc limit " + count + "";
                List<Entity> le = SqlExecutor.query(conn, sql, new EntityListHandler());
                price = LE2AH(le);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        } else {
            price = GetPriceReverse(code, count, unit, date);
            Collections.reverse(price);
        }
        return price;
    }

    /**
     * 获取股价，时间从小到大排列
     *
     * @param code
     * @param count
     * @param unit
     * @param date
     * @return
     */
    public ArrayList<HashMap<String, String>> GetPriceReverse(String code, int count, String unit, String date) {
        ArrayList<HashMap<String, String>> price = new ArrayList<>();
        if (unit.equals("5m")) {
            try {
                String sql;
                sql = "select * from t_futures_marketdata where dominant_code='" + code + "' and market_time>='" + date
                        + "' order by market_time asc limit " + count + "";
                List<Entity> le = SqlExecutor.query(conn, sql, new EntityListHandler());
                price = LE2AH(le);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        } else if (unit.equals("1m")) {
            try {
                String sql;
                sql = "select * from t_futures_marketdata_1m where dominant_code='" + code + "' and market_time>='" + date
                        + "' order by market_time asc limit " + count + "";
                List<Entity> le = SqlExecutor.query(conn, sql, new EntityListHandler());
                price = LE2AH(le);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        } else {
            getToken();
            JSONObject jo1 = JSONUtil.createObj();
            jo1.putOnce("method", "get_bars");
            jo1.putOnce("token", token);
            jo1.putOnce("code", code);
            jo1.putOnce("count", count);
            jo1.putOnce("unit", unit);
            jo1.putOnce("end_date", date);
            String priceList = HttpUtil.HttpUtilPost(jo1.toString(), url);
            String[] priceListItem = priceList.split("\n");
            String[] title = priceListItem[0].split(",");
            for (int i = 1; i < priceListItem.length; i++) {
                HashMap<String, String> Item = new HashMap<>();
                String[] StringItem = priceListItem[i].split(",");
                for (int j = 0; j < StringItem.length; j++) {
                    Item.put(title[j], StringItem[j]);
                }
                price.add(Item);
            }

        }
        return price;
    }

    public ArrayList<HashMap<String, String>> GetPricePeriod(String code, int count, String unit, String date, String endDate) {
        ArrayList<HashMap<String, String>> price = new ArrayList<>();
        if (unit.equals("5m")) {
            try {
                String sql;
                sql = "select * from t_futures_marketdata where dominant_code='" + code + "' and market_time>='" + date
                        + "' and market_time<='" + endDate + "' order by market_time desc limit " + count;
                List<Entity> le = SqlExecutor.query(conn, sql, new EntityListHandler());
                price = LE2AH(le);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        } else if (unit.equals("1m")) {
            try {
                String sql;
                sql = "select * from t_futures_marketdata_1m where dominant_code='" + code + "' and market_time>='" + date
                        + "' and market_time<='" + endDate + "' order by market_time desc limit " + count;
                List<Entity> le = SqlExecutor.query(conn, sql, new EntityListHandler());
                price = LE2AH(le);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        } else {
            ArrayList<HashMap<String, String>> alh = GetPricePeriod(code, unit, date, endDate);
            for (int i = 0; i < count; i++) {
                price.add(alh.get(i));
            }
        }
        return price;
    }

    public ArrayList<HashMap<String, String>> GetPricePeriod(String code, String unit, String date, String endDate) {
        ArrayList<HashMap<String, String>> price = new ArrayList<>();
        if (unit.equals("5m")) {
            try {
                String sql;
                sql = "select * from t_futures_marketdata where dominant_code='" + code + "' and market_time>='" + date
                        + "' and market_time<='" + endDate + "' order by market_time desc ";
                List<Entity> le = SqlExecutor.query(conn, sql, new EntityListHandler());
                price = LE2AH(le);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        } else if (unit.equals("1m")) {
            try {
                String sql;
                sql = "select * from t_futures_marketdata_1m where dominant_code='" + code + "' and market_time>='" + date
                        + "' and market_time<='" + endDate + "' order by market_time desc ";
                List<Entity> le = SqlExecutor.query(conn, sql, new EntityListHandler());
                price = LE2AH(le);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        } else {
            getToken();
            JSONObject jo1 = JSONUtil.createObj();
            jo1.putOnce("method", "get_price_period");
            jo1.putOnce("token", token);
            jo1.putOnce("code", code);
            jo1.putOnce("unit", unit);
            jo1.putOnce("date", date);
            jo1.putOnce("end_date", endDate);
            String priceList = HttpUtil.HttpUtilPost(jo1.toString(), url);
            String[] priceListItem = priceList.split("\n");
            String[] title = priceListItem[0].split(",");
            for (int i = 1; i < priceListItem.length; i++) {
                HashMap<String, String> Item = new HashMap<>();
                String[] StringItem = priceListItem[i].split(",");
                for (int j = 0; j < StringItem.length; j++) {
                    Item.put(title[j], StringItem[j]);
                }
                price.add(Item);
            }
            Collections.reverse(price);
        }
        return price;
    }

    public String getCurrentPrice(String code) {
        getToken();
        JSONObject jo1 = JSONUtil.createObj();
        jo1.putOnce("method", "get_current_price");
        jo1.putOnce("token", token);
        jo1.putOnce("code", code);
        String priceList = HttpUtil.HttpUtilPost(jo1.toString(), url);
        String sPrice = priceList.split("\n")[1].split(",")[1];
        return sPrice;
    }

    public String getCurrentPrice(String code, String date) {
        String price = "0";
        try {
            String sql;
            sql = "select open from t_futures_marketdata_1m where dominant_code='" + code + "' and market_time>='" + date
                    + "' order by market_time asc limit 1 ";
            List<Entity> le = SqlExecutor.query(conn, sql, new EntityListHandler());
            price = le.get(0).getStr("open");
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return price;
    }

    public BigDecimal getCurrentPriceDecimal(String code, String date) {
        String sPrice = getCurrentPrice(code, date);
        return new BigDecimal(sPrice);
    }

    public BigDecimal getCurrentPriceDecimal(String code) {
        String sPrice = getCurrentPrice(code);
        return new BigDecimal(sPrice);
    }

    public ArrayList<HashMap<String, String>> getSecuritiesInfo(String date) {
        getToken();
        String[] codes = {"stock", "fund", "index", "futures", "etf", "options"};
        ArrayList<HashMap<String, String>> result = new ArrayList<>();
        for (String code : codes) {
            JSONObject jo1 = JSONUtil.createObj();
            jo1.putOnce("method", "get_all_securities");
            jo1.putOnce("token", token);
            jo1.putOnce("code", code);
            jo1.putOnce("date", date);
            String info = HttpUtil.HttpUtilPost(jo1.toString(), url);
            String[] infoItem = info.split("\n");
            String[] title = infoItem[0].split(",");
            for (int i = 1; i < infoItem.length; i++) {
                HashMap<String, String> Item = new HashMap<>();
                String[] StringItem = infoItem[i].split(",");
                for (int j = 0; j < StringItem.length; j++) {
                    Item.put(title[j], StringItem[j]);
                }
                result.add(Item);
            }
        }
        return result;
    }

    public String getAvailableCount() {
        getToken();
        JSONObject jo1 = JSONUtil.createObj();
        jo1.putOnce("method", "get_query_count");
        jo1.putOnce("token", token);
        return HttpUtil.HttpUtilPost(jo1.toString(), url);
    }

    public boolean isTradeDay(DateTime date) {
        try {
            String sql;
            sql = "select * from t_sys_tradeday where trade_day='" + date.toString("yyyy-MM-dd") + "'";
            List<Entity> le = SqlExecutor.query(conn, sql, new EntityListHandler());
            if (le.size() == 0)
                return false;
            else
                return true;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    public String[] getTradeDays(DateTime date, DateTime endDate) {
        getToken();
        JSONObject jo1 = JSONUtil.createObj();
        jo1.putOnce("method", "get_trade_days");
        jo1.putOnce("token", token);
        jo1.putOnce("date", date.toString("yyyy-MM-dd"));
        jo1.putOnce("end_date", endDate.toString("yyyy-MM-dd"));
        String result = HttpUtil.HttpUtilPost(jo1.toString(), url);
        return result.split("\n");
    }

    public String getNextTradeDay(DateTime date) {
        try {
            String sql;
            sql = "select * from t_sys_tradeday where trade_day>'" + date.toString("yyyy-MM-dd") + "' order by trade_day asc limit 1";
            List<Entity> le = SqlExecutor.query(conn, sql, new EntityListHandler());
            return le.get(0).getStr("trade_day");
        } catch (SQLException e) {
            e.printStackTrace();
            return "";
        }
    }

    public String getLastTradeDay(DateTime date) {
        try {
            String sql;
            sql = "select * from t_sys_tradeday where trade_day<'" + date.toString("yyyy-MM-dd") + "' order by trade_day desc limit 1";
            List<Entity> le = SqlExecutor.query(conn, sql, new EntityListHandler());
            return le.get(0).getStr("trade_day");
        } catch (SQLException e) {
            e.printStackTrace();
            return "";
        }
    }

    public String getTradeDayOffset(DateTime date, int offset) {
        String returnDate = "";
        try {
            String sql;
            if (offset > 0) {
                sql = "select * from t_sys_tradeday where trade_day>'" + date.toString("yyyy-MM-dd") + "' order by trade_day asc limit " + offset;
            } else if (offset < 0) {
                sql = "select * from t_sys_tradeday where trade_day<'" + date.toString("yyyy-MM-dd") + "' order by trade_day desc limit " + (offset * -1);
            } else {
                sql = "select * from t_sys_tradeday where trade_day<='" + date.toString("yyyy-MM-dd") + "' order by trade_day desc limit 1";
            }
            List<Entity> le = SqlExecutor.query(conn, sql, new EntityListHandler());
            Entity entity = le.get(le.size() - 1);
            returnDate = entity.getStr("trade_day");
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return returnDate;
    }


    public String getDominantFuture(String code, String date) {
        getToken();
        JSONObject jo1 = JSONUtil.createObj();
        jo1.putOnce("method", "get_dominant_future");
        jo1.putOnce("token", token);
        jo1.putOnce("code", code);
        jo1.putOnce("date", date);
        return HttpUtil.HttpUtilPost(jo1.toString(), url);
    }

    public boolean isTradeTime(DateTime date) {
        if (isTradeDay(DateUtil.offsetDay(date, -1))) {
            if (date.hour(true) < 2 | (date.hour(true) == 2 & date.minute() < 30)) return true;
        }

        if (isTradeDay(date)) {
            int hour = date.hour(true);
            int minute = date.minute();
            if (hour == 9)
                return true;
            else if (hour == 10 & minute < 15)
                return true;
            else if (hour == 10 & minute >= 30)
                return true;
            else if (hour == 11 & minute < 30)
                return true;
            else if (hour == 13 & minute >= 30)
                return true;
            else if (hour == 14 | hour == 21 | hour == 22 | hour == 23)
                return true;
            return false;
        } else
            return false;
    }


    /**
     * 判断当前时间是否交易时间
     *
     * @param date
     * @return
     */
    public boolean isTradeTime(DateTime date, String code, String exchange) {
        if (!isTradeDay(date)) {
            if (code.equals("AG") | code.equals("AU")) if (!isTradeDay(DateUtil.offsetDay(date, -1))) return false;
            else if (date.hour(true) < 2 | (date.hour(true) == 2 & date.minute() < 30)) return true;
            else return false;
//            铜、铝、铅、锌、镍、锡、不锈钢
            if (code.equals("CU") | code.equals("AL") | code.equals("PB") | code.equals("ZN") | code.equals("NI") | code.equals("SN") | code.equals("SS"))
                if (!isTradeDay(DateUtil.offsetDay(date, -1))) return false;
                else if (date.hour(true) == 0) return true;
                else return false;
            return false;
        }
        int hour = date.hour(true);
        int minute = date.minute();
        if (hour == 9)
            return true;
        else if (hour == 10 & minute < 15)
            return true;
        else if (hour == 10 & minute >= 30)
            return true;
        else if (hour == 11 & minute < 30)
            return true;
        else if (hour == 13 & minute >= 30)
            return true;
        else if (hour == 14 | hour == 21 | hour == 22)
            return true;
        else if (hour == 23 & minute < 30 & (exchange == "XZCE" | exchange == "XDCE"))
            return true;
        return false;
    }

    //郑商所：普麦、强麦、苹果、红枣、尿素、粳稻、硅铁、锰硅、菜籽、花生；
    //上期所：线材；
    //大商所：鸡蛋、纤维板、胶合板、生猪以及金融期货，都是没有夜盘的。

    //        交易所交易时间：
//        (一)大连、上海、能源、郑州交易所
//        集合竞价申报时间：08：55—08：59
//        集合竞价撮合时间：08：59—09：00
//        正常开盘交易时间：09：00－11：30 （小节休息10：15－10：30）
//        13：30－15：00
//        提示：客户下单时间为集合竞价时间和正常交易时间。在8：59—9：00竞价结束时间和交易所小节休息时间（上午10:15-10:30）下单，交易系统将不接受指令，并视之为废单。（时间以交易所时钟报时为准）
//        (二)上期所夜盘
//        集合竞价申报时间：20：55—20：59
//        集合竞价撮合时间：20：59—21：00
//        正常开盘交易时间：21：00－02：30 （黄金、白银）
//        21：00－01：00 （铜、铝、铅、锌、镍、锡、不锈钢）
//        21：00－23：00（螺纹钢、热轧卷板、石油沥青、天然橡胶、燃料油、纸浆）
//        提示：法定节假日的前一日没有夜盘交易。
//（三）大商所夜盘
//        集合竞价申报时间：20：55—20：59
//        集合竞价撮合时间：20：59—21：00
//        正常开盘交易时间：21：00—23：30 （豆一、豆二、豆油、豆粕、焦煤、焦炭、棕榈油、铁矿石、塑料、PVC、聚丙烯、乙二醇、玉米、玉米淀粉、粳米、苯乙烯）
//        提示：法定节假日的前一日没有夜盘交易。
//（四）郑商所夜盘
//        集合竞价申报时间：20：55—20：59
//        集合竞价撮合时间：20：59—21：00
//        正常开盘交易时间：21：00－23：30 （白糖、棉花、棉纱、菜粕、甲醇、PTA、菜籽油、玻璃、动力煤、纯碱）
//        提示：法定节假日的前一日没有夜盘交易。
//        (五)中金所
//        股指:集合竞价时间：9：25—9：30
//        正常开盘交易时间：9：30-11：30（第一节）；13：00-15：00（第二节）
//        国债：
//        集合竞价时间：9：10-9：15
//        正常开盘交易时间：9：15-11：30（第一节）；13：00-15：15（第二节）
//        最后交易日交易时间：9：15-11：30
//        (六)上海国际能源交易中心夜盘
//        集合竞价申报时间：20：55—20：59
//        集合竞价撮合时间：20：59—21：00
//        正常开盘交易时间：21：00—23：30 （20号胶）
//        21：00—次日2:30 （原油）

}