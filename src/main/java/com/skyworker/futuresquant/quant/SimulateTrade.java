package com.skyworker.futuresquant.quant;

import cn.hutool.core.date.DateUtil;
import cn.hutool.db.Entity;
import cn.hutool.db.handler.EntityListHandler;
import cn.hutool.db.sql.SqlExecutor;
import cn.hutool.log.Log;
import com.skyworker.futuresquant.utils.SingletonConn;
import com.skyworker.futuresquant.utils.SingletonLog;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static com.skyworker.futuresquant.utils.QuantUtil.getCode;
import static com.skyworker.futuresquant.utils.QuantUtil.isTradeTime;

public class SimulateTrade {
    private static final Log log = SingletonLog.getInstance().log;
    private static final Connection conn = SingletonConn.getInstance().conn;
    List<Entity> positions;
    List<Entity> tradeDominants;
    Entity simulate_main;
    private int SimulateId;
    //private Connection conn = null;
    private GetFinanceData getFinanceData;
    private String tradeDate;

    //目前假定所有的开仓都是新开，不是加仓。所有的平仓都是全平，不含部分平仓。  后面慢慢改。

    public SimulateTrade(int _SimulateId, String _date) {
        SimulateId = _SimulateId;
        tradeDate = _date;
        getFinanceData = new GetFinanceData();
        flashData();

    }

    private void flashData() {
        try {
            positions = SqlExecutor.query(conn, "select * from t_simulate_position where simulate_id=" + SimulateId, new EntityListHandler());
            simulate_main = SqlExecutor.query(conn, "select * from t_simulate_main where id=" + SimulateId, new EntityListHandler()).get(0);
            tradeDominants = SqlExecutor.query(conn, "select * from t_futures_dominant where is_trade=1 and simulate_id=" + SimulateId, new EntityListHandler());
        } catch (SQLException e) {
            log.error(e);
        }
    }

    public List<Entity> getPositions() {
        return positions;
    }

    public List<Entity> getTradeDominants() {
        return tradeDominants;
    }

    public BigDecimal getAvailableAmount() {
        return simulate_main.getBigDecimal("available_amount");
    }

    public String getName(String code) {
        String name = "";
        try {
            Entity varietie = SqlExecutor.query(conn, "select * from t_futures_varieties where code='" + code + "'", new EntityListHandler()).get(0);
            name = varietie.getStr("name");
        } catch (SQLException e) {
            log.error(e);
        }
        return name;
    }


    public BigDecimal getPositionAmount(String dominant_code) {
        BigDecimal amount = BigDecimal.valueOf(0);
        for (Entity entity : positions) {
            if (entity.getStr("dominant_code").equals(dominant_code)) {
                amount = entity.getBigDecimal("amount");
                break;
            }
        }
        return amount;
    }

    public Entity getPosition(String dominant_code) {
        Entity entity1 = null;
        for (Entity entity : positions) {
            if (entity.getStr("dominant_code").equals(dominant_code)) {
                entity1 = entity;
                break;
            }
        }
        return entity1;
    }


    public BigDecimal getMargin(String code, BigDecimal price, BigDecimal amount) {
        BigDecimal margin = BigDecimal.valueOf(0);
        try {
            Entity varietie = SqlExecutor.query(conn, "select * from t_futures_varieties where code='" + code + "'", new EntityListHandler()).get(0);
            //保证金
            BigDecimal lot_amount = varietie.getBigDecimal("lot_amount");
            BigDecimal marginRate = varietie.getBigDecimal("margin");
            margin = price.multiply(amount).multiply(lot_amount).multiply(marginRate).divide(new BigDecimal(100));
        } catch (SQLException e) {
            log.error(e);
        }
        return margin;
    }

    public BigDecimal getMargin(String code, String dominant_code, BigDecimal amount) {
        BigDecimal price = getFinanceData.getCurrentPriceDecimal(dominant_code, tradeDate);
        return getMargin(code, price, amount);
    }

    public BigDecimal getOpenFee(String code, BigDecimal price, BigDecimal amount) {
        BigDecimal fee = BigDecimal.valueOf(0);
        try {
            Entity varietie = SqlExecutor.query(conn, "select * from t_futures_varieties where code='" + code + "'", new EntityListHandler()).get(0);
            if (varietie.getInt("fee_type").equals(1)) {
                fee = amount.multiply(varietie.getBigDecimal("fee_amount"));
            } else {
                BigDecimal lot_amount = varietie.getBigDecimal("lot_amount");
                BigDecimal fee_rate = varietie.getBigDecimal("fee_rate");
                fee = price.multiply(amount).multiply(lot_amount).multiply(fee_rate).divide(new BigDecimal(10000));
            }
        } catch (SQLException e) {
            log.error(e);
        }
        return fee;
    }

    public BigDecimal getCloseFee(String code, BigDecimal price, BigDecimal amount) {
        BigDecimal fee = BigDecimal.valueOf(0);
        try {
            Entity varietie = SqlExecutor.query(conn, "select * from t_futures_varieties where code='" + code + "'", new EntityListHandler()).get(0);
            if (varietie.getInt("fee_type").equals(1)) {
                fee = amount.multiply(varietie.getBigDecimal("fee_close"));
            } else {
                BigDecimal lot_amount = varietie.getBigDecimal("lot_amount");
                BigDecimal fee_rate = varietie.getBigDecimal("fee_close");
                fee = price.multiply(amount).multiply(lot_amount).multiply(fee_rate).divide(new BigDecimal(10000));
            }
        } catch (SQLException e) {
            log.error(e);
        }
        return fee;
    }

    public List<BigDecimal> getProfit(String dominant_code, String type, BigDecimal price, BigDecimal amount) {
        BigDecimal GrossProfit = new BigDecimal(0);
        BigDecimal NetProfit = new BigDecimal(0);
        String code = getCode(dominant_code);
        try {
            Entity varietie = SqlExecutor.query(conn, "select * from t_futures_varieties where code='" + code + "'", new EntityListHandler()).get(0);
            Entity position = SqlExecutor.query(conn, "select * from t_simulate_position where simulate_id=" + SimulateId
                    + " and dominant_code='" + dominant_code + "'", new EntityListHandler()).get(0);
            if (type == "SP") {
                GrossProfit = price.subtract(position.getBigDecimal("price")).multiply(varietie.getBigDecimal("lot_amount")).multiply(amount);
                NetProfit = GrossProfit.subtract(position.getBigDecimal("open_fee"));
            } else {
                GrossProfit = position.getBigDecimal("price").subtract(price).multiply(varietie.getBigDecimal("lot_amount")).multiply(amount);
                NetProfit = GrossProfit.subtract(position.getBigDecimal("open_fee"));
            }
        } catch (SQLException e) {
            log.error(e);
        }
        List<BigDecimal> lb = new ArrayList<>();
        lb.add(GrossProfit);
        lb.add(NetProfit);
        return lb;
    }

    public void updateMargin() {
        BigDecimal totalMargin = BigDecimal.valueOf(0);
        try {
            for (Entity entity : positions) {
                BigDecimal newMargin = BigDecimal.valueOf(0);
                if (isTradeTime(DateUtil.parseDateTime(tradeDate), entity.getStr("dominant_code"))) {
                    newMargin = getMargin(entity.getStr("code"), entity.getStr("dominant_code"), entity.getBigDecimal("amount"));
                    SqlExecutor.execute(conn, "update t_simulate_position set margin=? where id=?", newMargin, entity.getStr("id"));
                } else {
                    newMargin = new BigDecimal(entity.getStr("margin"));
                }
                totalMargin = totalMargin.add(newMargin);
            }
            SqlExecutor.execute(conn, "update t_simulate_main set occupy_amount=? where id=?", totalMargin, SimulateId);
            SqlExecutor.execute(conn, "update t_simulate_main set available_amount = total_amount - occupy_amount where id=?", SimulateId);
        } catch (SQLException e) {
            log.error(e);
        }

    }

    public void OpenPosition(String dominant_code, String type, BigDecimal amount) {
        OpenPosition(dominant_code, type, amount, getFinanceData.getCurrentPriceDecimal(dominant_code, tradeDate), "");
    }

    public void OpenPosition(String dominant_code, String type, BigDecimal amount, String note) {
        OpenPosition(dominant_code, type, amount, getFinanceData.getCurrentPriceDecimal(dominant_code, tradeDate), note);
    }

    public void OpenPosition(String dominant_code, String type, BigDecimal amount, BigDecimal price, String note) {
        if (amount.equals(0)) {
            return;
        }
        String code = getCode(dominant_code);
        updateMargin();
        flashData();
        BigDecimal availableAmount = getAvailableAmount();
        BigDecimal margin = getMargin(code, price, amount);
        BigDecimal fee = getOpenFee(code, price, amount);
        if (availableAmount.compareTo(margin.add(fee)) == -1) {
            log.error("购买资金不足：" + dominant_code + type + amount + price);
            return;
        }
        BigDecimal currentamount = getPositionAmount(dominant_code);
        String name = getName(code);
        String sql = "";
        try {
            if (currentamount.compareTo(BigDecimal.valueOf(0)) == 0) {
                Entity varietie = SqlExecutor.query(conn, "select * from t_futures_varieties where code='" + code + "'", new EntityListHandler()).get(0);

                sql = "insert into t_simulate_position values(null," + SimulateId + ",'" + code + "','" + dominant_code
                        + "','" + name + "'," + amount + "," + price + ",'" + type + "'," + margin + "," + fee + ",'"
                        + tradeDate + "',0," + varietie.getStr("lot_amount") + ",0)";
            } else {
                // 这里有bug，先不用这种情况。
                sql = "update t_simulate_position set amount = amount + " + amount + ","
                        + "margin = margin + " + margin + ", open_fee = open_fee + " + fee
                        + " where simulate_id = " + SimulateId + " and dominant_code='" + dominant_code + "'";
            }
            SqlExecutor.execute(conn, sql);
            sql = "insert into t_simulate_traderecord values(null," + SimulateId + ",'" + code + "','" + dominant_code
                    + "','" + name + "','" + type + "'," + amount + "," + price + "," + fee + "," + margin
                    + ",'" + tradeDate + "',0,'" + note + "')";
            SqlExecutor.execute(conn, sql);
            sql = "update t_simulate_main set total_amount = total_amount - " + fee + " where id=" + SimulateId;
            SqlExecutor.execute(conn, sql);
            flashData();
            updateMargin();
            flashData();
            //
        } catch (SQLException e) {
            log.error(e);
        }
    }

    public double ClosePosition(String dominant_code, String type, BigDecimal amount) {
        return ClosePosition(dominant_code, type, amount, getFinanceData.getCurrentPriceDecimal(dominant_code, tradeDate), "");
    }

    public double ClosePosition(String dominant_code) {
        return ClosePosition(dominant_code, "");
    }

    public double ClosePosition(String dominant_code, String note) {
        String type = "";
        Entity position = getPosition(dominant_code);
        if (position.getStr("type").equals("BK")) {
            type = "SP";
        } else {
            type = "BP";
        }
        return ClosePosition(dominant_code, type, position.getBigDecimal("amount"), getFinanceData.getCurrentPriceDecimal(dominant_code, tradeDate), note);
    }

    public double ClosePosition(String dominant_code, String type, BigDecimal amount, BigDecimal price, String note) {
        String code = getCode(dominant_code);
        String name = getName(code);
        BigDecimal positionAmount = getPositionAmount(dominant_code);
        if (positionAmount.equals(BigDecimal.valueOf(0))) {
            log.error("?持仓不存在", dominant_code);
            return 0;
        } else if (positionAmount.compareTo(amount) == -1) {
            log.error("?持仓小于卖出数量", dominant_code);
            return 0;
        }
        BigDecimal fee = getCloseFee(code, price, amount);
        List<BigDecimal> lb = getProfit(dominant_code, type, price, amount);
        BigDecimal grossProfit = lb.get(0);
        BigDecimal netProfit = lb.get(1).subtract(fee);
        BigDecimal income = grossProfit.subtract(fee);
        try {
            String sql = "";
            sql = "insert into t_simulate_traderecord values(null," + SimulateId + ",'" + code + "','" + dominant_code
                    + "','" + name + "','" + type + "'," + amount.toPlainString() + "," + price.toPlainString() + "," + fee.toPlainString() + ",0,'" + tradeDate + "'," + netProfit.toPlainString() + ",'" + note + "')";
            SqlExecutor.execute(conn, sql);
            SqlExecutor.execute(conn, "update t_simulate_main set total_amount = total_amount + " + income + " where id=" + SimulateId);
            SqlExecutor.execute(conn, "delete from t_simulate_position where simulate_id=" + SimulateId + " and dominant_code='" + dominant_code + "'");
            flashData();
            updateMargin();
            flashData();
        } catch (SQLException e) {
            log.error(e);
        }
        return netProfit.doubleValue();
    }
}
