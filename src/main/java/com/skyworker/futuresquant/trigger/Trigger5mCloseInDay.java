package com.skyworker.futuresquant.trigger;

import cn.hutool.core.date.DateTime;
import cn.hutool.core.date.DateUtil;
import cn.hutool.db.Entity;
import cn.hutool.db.handler.EntityListHandler;
import cn.hutool.db.sql.SqlExecutor;
import cn.hutool.log.Log;
import com.skyworker.futuresquant.strategys.*;
import com.skyworker.futuresquant.utils.SingletonConn;
import com.skyworker.futuresquant.utils.SingletonLog;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Trigger5mCloseInDay {
    private static final Log log = SingletonLog.getInstance().log;
    private static final Connection conn = SingletonConn.getInstance().conn;
    private final DateTime tradeDate;
    private final int SimulateId;
    private final int StrategyId;

    public Trigger5mCloseInDay(DateTime _tradeDate, int _SimulateId, int _StrategyId) {
        tradeDate = _tradeDate;
        SimulateId = _SimulateId;
        StrategyId = _StrategyId;
    }

    public void runTrigger5mCloseInDay() {
        log.info("日盘开盘前处理");
        runBeforeDayOpen();
        log.info("日盘开盘处理");
        runDayOpen();
        log.info("日盘交易");
        runDayTrade();
        log.info("日终平仓");
        CloseDay();

        log.info("夜盘开盘前处理");
        runBeforeNightOpen();
        log.info("夜盘开盘处理");
        runNightOpen();
        log.info("夜盘交易");
        runNightTrade();
        log.info("日终平仓");
        CloseNight();
    }

    private void runBeforeDayOpen() {
        BeforeOpen beforeOpen = new BeforeOpen(tradeDate, SimulateId, StrategyId);
        beforeOpen.runBeforeOpen();
    }

    private void runDayOpen() {
        DateTime OpenJobTime = DateUtil.parseDateTime(tradeDate.toString("yyyy-MM-dd") + " 09:05:02");
        Open open = new Open(OpenJobTime, SimulateId, StrategyId);
        open.runOpen();
    }

    private void runDayTrade() {
        try {
            List<Entity> le = SqlExecutor.query(conn, "select * from t_simulate_trigger where type='5mDay' order by add_day,`trigger`", new EntityListHandler());
            ArrayList<HashMap<String, String>> SelectUnit = new ArrayList<>();
            SelectUnit.add(new HashMap<String, String>() {
                {
                    put("unit", "5m");
                    put("offset", "-2");
                }
            });
            SelectUnit.add(new HashMap<String, String>() {
                {
                    put("unit", "30m");
                    put("offset", "-3");
                }
            });
            SelectUnit.add(new HashMap<String, String>() {
                {
                    put("unit", "1d");
                    put("offset", "-22");
                }
            });

            for (Entity entity : le) {
                DateTime TriggerTime = DateUtil.parseDateTime(tradeDate.toString("yyyy-MM-dd") + " " + entity.getStr("trigger"));
                log.info("当前处理时间：" + TriggerTime);
                StopPosition stopPosition = new StopPosition(TriggerTime, SimulateId, StrategyId);
                stopPosition.runStopPosition();
                if (!entity.getStr("trigger").equals("11:20:05") | !entity.getStr("trigger").equals("11:25:05")) {
                    OpenPosition openPosition = new OpenPosition(TriggerTime, SimulateId, StrategyId, SelectUnit);
                    openPosition.runOpenPosition();
                }
                CalculateStop calculateStop = new CalculateStop(TriggerTime, SimulateId, StrategyId);
                calculateStop.runCalculateStop();
            }
        } catch (SQLException e) {
            log.error(e);
        }
    }

    private void CloseDay() {
        CloseAll closeAll = new CloseAll(SimulateId
                , tradeDate.toString("yyyy-MM-dd") + " 11:25:02");
        closeAll.runCloseAll();
    }

    private void runBeforeNightOpen() {
        BeforeOpen beforeOpen = new BeforeOpen(tradeDate, SimulateId, StrategyId);
        beforeOpen.runBeforeOpen();
    }

    private void runNightOpen() {
        DateTime OpenJobTime = DateUtil.parseDateTime(tradeDate.toString("yyyy-MM-dd") + " 21:05:02");
        Open open = new Open(OpenJobTime, SimulateId, StrategyId);
        open.runOpen();
    }

    private void runNightTrade() {
        try {
            List<Entity> le = SqlExecutor.query(conn, "select * from t_simulate_trigger where type='5mNight' order by add_day,`trigger`", new EntityListHandler());
            ArrayList<HashMap<String, String>> SelectUnit = new ArrayList<>();
            SelectUnit.add(new HashMap<String, String>() {
                {
                    put("unit", "5m");
                    put("offset", "-2");
                }
            });
            SelectUnit.add(new HashMap<String, String>() {
                {
                    put("unit", "30m");
                    put("offset", "-3");
                }
            });
            SelectUnit.add(new HashMap<String, String>() {
                {
                    put("unit", "1d");
                    put("offset", "-22");
                }
            });

            for (Entity entity : le) {
                DateTime TriggerTime = DateUtil.parseDateTime(tradeDate.toString("yyyy-MM-dd") + " " + entity.getStr("trigger"));
                log.info("当前处理时间：" + TriggerTime);
                StopPosition stopPosition = new StopPosition(TriggerTime, SimulateId, StrategyId);
                stopPosition.runStopPosition();
                if (!entity.getStr("trigger").equals("22:45:05") | !entity.getStr("trigger").equals("22:50:05")) {
                    OpenPosition openPosition = new OpenPosition(TriggerTime, SimulateId, StrategyId, SelectUnit);
                    openPosition.runOpenPosition();
                }
                CalculateStop calculateStop = new CalculateStop(TriggerTime, SimulateId, StrategyId);
                calculateStop.runCalculateStop();
            }
        } catch (SQLException e) {
            log.error(e);
        }
    }

    private void CloseNight() {
        CloseAll closeAll = new CloseAll(SimulateId
                , tradeDate.toString("yyyy-MM-dd") + " 22:55:02");
        closeAll.runCloseAll();
    }


}
