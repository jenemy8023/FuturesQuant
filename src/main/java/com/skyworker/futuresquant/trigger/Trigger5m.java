package com.skyworker.futuresquant.trigger;

import cn.hutool.core.date.DateField;
import cn.hutool.core.date.DateTime;
import cn.hutool.core.date.DateUtil;
import cn.hutool.db.Entity;
import cn.hutool.db.handler.EntityListHandler;
import cn.hutool.db.sql.SqlExecutor;
import cn.hutool.log.Log;
import com.skyworker.futuresquant.strategys.*;
import com.skyworker.futuresquant.utils.SingletonConn;
import com.skyworker.futuresquant.utils.SingletonLog;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Trigger5m {
    private static final Log log = SingletonLog.getInstance().log;
    private static final Connection conn = SingletonConn.getInstance().conn;
    private final DateTime tradeDate;
    private final int SimulateId;
    private final int StrategyId;

    public Trigger5m(DateTime _tradeDate, int _SimulateId, int _StrategyId) {
        tradeDate = _tradeDate;
        SimulateId = _SimulateId;
        StrategyId = _StrategyId;
    }

    public void runTrigger5m() {
        log.info("开盘前处理");
        runBeforeOpen();
        log.info("处理开盘问题");
        runOpen();
        log.info("开始处理日中交易");
        runTrade();
    }

    private void runBeforeOpen() {
        BeforeOpen beforeOpen = new BeforeOpen(tradeDate, SimulateId, StrategyId);
        beforeOpen.runBeforeOpen();
    }

    private void runOpen() {
        DateTime OpenJobTime = DateUtil.parseDateTime(tradeDate.toString("yyyy-MM-dd") + " 09:00:05");
        Open open = new Open(OpenJobTime, SimulateId, StrategyId);
        open.runOpen();
    }

    private void runTrade() {
        try {
            List<Entity> le = SqlExecutor.query(conn, "select * from t_simulate_trigger where type='5m' order by add_day,`trigger`", new EntityListHandler());
            ArrayList<HashMap<String, String>> SelectUnit = new ArrayList<>();
            SelectUnit.add(new HashMap<String, String>() {
                {
                    put("unit", "5m");
                    put("offset", "-2");
                }
            });
            SelectUnit.add(new HashMap<String, String>() {
                {
                    put("unit", "30m");
                    put("offset", "-3");
                }
            });
            SelectUnit.add(new HashMap<String, String>() {
                {
                    put("unit", "1d");
                    put("offset", "-22");
                }
            });

            for (Entity entity : le) {
                DateTime TriggerTime;
                DateTime tempTime;
                if (entity.getInt("add_day") == 0) {
                    TriggerTime = DateUtil.parseDateTime(tradeDate.toString("yyyy-MM-dd") + " " + entity.getStr("trigger"));
                } else {
                    tempTime = DateUtil.parseDate(tradeDate.toString("yyyy-MM-dd"));
                    tempTime = tempTime.offset(DateField.DAY_OF_YEAR, 1);
                    TriggerTime = DateUtil.parseDateTime(tempTime.toString("yyyy-MM-dd") + " " + entity.getStr("trigger"));
                }
                log.info("当前处理时间：" + TriggerTime.toString());
                StopPosition stopPosition = new StopPosition(TriggerTime, SimulateId, StrategyId);
                stopPosition.runStopPosition();
                OpenPosition openPosition = new OpenPosition(TriggerTime, SimulateId, StrategyId, SelectUnit);
                openPosition.runOpenPosition();
                CalculateStop calculateStop = new CalculateStop(TriggerTime, SimulateId, StrategyId);
                calculateStop.runCalculateStop();
            }
        } catch (SQLException e) {
            log.error(e);
        }
    }


}
