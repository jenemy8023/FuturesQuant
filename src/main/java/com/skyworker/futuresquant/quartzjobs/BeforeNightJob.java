package com.skyworker.futuresquant.quartzjobs;

import cn.hutool.core.date.DateTime;
import cn.hutool.core.date.DateUtil;
import com.skyworker.futuresquant.quant.GetFinanceData;
import com.skyworker.futuresquant.strategys.BeforeNight;
import com.skyworker.futuresquant.utils.QuantUtil;
import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import java.util.HashMap;

public class BeforeNightJob implements Job {
    @Override
    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        GetFinanceData getFinanceData = new GetFinanceData();
        JobDataMap data = jobExecutionContext.getJobDetail().getJobDataMap();
        HashMap<String, Object> hmo = (HashMap<String, Object>) data.get("data");
        DateTime tradeDate = DateUtil.date();
        int StrategyId = Integer.valueOf(hmo.get("StrategyId").toString());
        int SimulateId = Integer.valueOf(hmo.get("SimulateId").toString());

        if (getFinanceData.isTradeDay(tradeDate)) {
            QuantUtil.exeCmd("C://CTPCommand//CTPSettlement.exe");
            BeforeNight beforeNight = new BeforeNight(tradeDate, SimulateId, StrategyId);
            beforeNight.runBeforeNight();
        }
    }
}
