package com.skyworker.futuresquant.quartzjobs;

import cn.hutool.core.date.DateTime;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.URLUtil;
import cn.hutool.log.Log;
import com.skyworker.futuresquant.quant.GetFinanceData;
import com.skyworker.futuresquant.strategys.CalculateStop;
import com.skyworker.futuresquant.strategys.OpenPosition;
import com.skyworker.futuresquant.strategys.StopPosition;
import com.skyworker.futuresquant.utils.HttpUtil;
import com.skyworker.futuresquant.utils.QuantUtil;
import com.skyworker.futuresquant.utils.SingletonLog;
import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;

public class TradeTimeJob implements Job {
    private static final Log log = SingletonLog.getInstance().log;

    @Override
    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        GetFinanceData getFinanceData = new GetFinanceData();
        JobDataMap data = jobExecutionContext.getJobDetail().getJobDataMap();
        HashMap<String,Object> hmo = (HashMap<String, Object>) data.get("data");
        DateTime tradeDate = DateUtil.date();
        int StrategyId = Integer.valueOf(hmo.get("StrategyId").toString());
        int SimulateId =  Integer.valueOf(hmo.get("SimulateId").toString());
        if (getFinanceData.isTradeTime(tradeDate)) {
            log.info("当前处理时间：" + tradeDate.toString());
            ArrayList<HashMap<String, String>> SelectUnit = new ArrayList<>();
            SelectUnit.add(new HashMap<String, String>() {
                {
                    put("unit", "5m");
                    put("offset", "-2");
                }
            });
            SelectUnit.add(new HashMap<String, String>() {
                {
                    put("unit", "30m");
                    put("offset", "-3");
                }
            });
            SelectUnit.add(new HashMap<String, String>() {
                {
                    put("unit", "1d");
                    put("offset", "-22");
                }
            });

            StopPosition stopPosition = new StopPosition(tradeDate, SimulateId, StrategyId);
            ArrayList<HashMap<String, String>> ahStop = stopPosition.runStopPosition();
            SendSMS(ahStop);
            CTPTrade(ahStop);
            OpenPosition openPosition = new OpenPosition(tradeDate, SimulateId, StrategyId, SelectUnit);
            ArrayList<HashMap<String, String>> ahOpen = openPosition.runOpenPosition();
            SendSMS(ahOpen);
            CTPTrade(ahStop);
            CalculateStop calculateStop = new CalculateStop(tradeDate, SimulateId, StrategyId);
            calculateStop.runCalculateStop();
        }
    }

    private void SendSMS(ArrayList<HashMap<String, String>> ah) {
        if (ah.size() > 0) {
            //发短信
            String url = "https://api.smsbao.com/sms";
            String Username = "jenemy"; //在短信宝注册的用户名
            String Password = "41593ea192414b889a41551f2f3e2b90";//SecureUtil.md5("jenemy19521005"); //在短信宝注册的密码
            String Phone = "18616896212,13817383985";
            for (HashMap<String, String> hm : ah) {
                String code = hm.get("dominant_code");
                String price = hm.get("price");
                String type = hm.get("type");
                String vol = hm.get("vol");
                String profit = "0";
                if (hm.get("type").equals("SP") | hm.get("type").equals("BP")) {
                    profit = hm.get("profit");
                }

                String Content = "【量化投资实验室】量化交易提醒：品种：" + code + "，类型：" + type + "，价格：" + price
                        + "，交易量：" + vol + "，收益：" + profit;
                Content = URLUtil.encode(Content, StandardCharsets.UTF_8);
                url = url + "?u=" + Username + "&p=" + Password + "&m=" + Phone + "&c=" + Content;
                String returnCode = HttpUtil.HttpUtilGet(url);
                if (!returnCode.equals("0")) {
                    switch (returnCode) {
                        case "-1":
                            log.error("-1:参数不全");
                            break;
                        case "-2":
                            log.error("-2:服务器空间不支持,请确认支持curl或者fsocket，联系您的空间商解决或者更换空间！");
                            break;
                        case "30":
                            log.error("30:密码错误");
                            break;
                        case "40":
                            log.error("40:账号不存在");
                            break;
                        case "41":
                            log.error("41:余额不足");
                            break;
                        case "42":
                            log.error("42:帐户已过期");
                            break;
                        case "43":
                            log.error("43:IP地址限制");
                            break;
                        case "50":
                            log.error("50:内容含有敏感词");
                            break;
                    }
                }
            }
        }
    }

    private void CTPTrade(ArrayList<HashMap<String, String>> ah) {
        if (ah.size() > 0) {
            for (HashMap<String, String> hm : ah) {
                String price = hm.get("price");
                String type = hm.get("type");
                String vol = hm.get("vol");
                String dominant_code = hm.get("dominant_code");
                String codes[] = dominant_code.split(".");
                String exchange = "";
                String code = "";
                long tradeprice = Math.round(Double.valueOf(price) * 1.05);
                price = String.valueOf(tradeprice);

                switch (codes[1]) {
                    //上期所
                    case "XSGE":
                        exchange = "SHFE";
                        code = codes[0].toLowerCase();
                        if (type.equals("BP") | type.equals("SP")) {
                            if (hm.get("isYest").equals("0")) {
                                type = type + "T";
                            }
                        }
                        break;
                    //大商所
                    case "XDCE":
                        exchange = "DCE";
                        code = codes[0].toLowerCase();
                        break;
                    //郑商所
                    case "XZCE":
                        exchange = "CZCE";
                        code = codes[0];
                        break;
                }
                String commStr = "C://CTPCommand//CTPOpen.exe " + exchange + " " + code + " " + type +  " " + price + " " + vol;
                QuantUtil.exeCmd(commStr);
            }
        }
    }
}
