package com.skyworker.futuresquant.controller;

import com.skyworker.futuresquant.quartzjobs.BeforeNightJob;
import com.skyworker.futuresquant.quartzjobs.BeforeOpenJob;
import com.skyworker.futuresquant.quartzjobs.OpenJob;
import com.skyworker.futuresquant.quartzjobs.TradeTimeJob;
import com.skyworker.futuresquant.utils.SingletonDataAccount;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.Trigger;
import org.quartz.impl.StdSchedulerFactory;

import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;

import static org.quartz.CronScheduleBuilder.cronSchedule;
import static org.quartz.TriggerBuilder.newTrigger;

public class RealTimeTrade implements Initializable {
    @FXML
    private ComboBox mCombSimulateId;
    @FXML
    private ComboBox mCombStrategys;
    @FXML
    private ComboBox mCombTrigger;
    @FXML
    private ComboBox mCombDataAccount;
    @FXML
    private Button mBtnStartRealTimeTrade;
    @FXML
    private Button mBtnStopRealTimeTrade;
    private Scheduler schedulerBeforeOpen;
    private Scheduler schedulerOpen;
    private Scheduler schedulerTradeTime;
    private Scheduler schedulerBeforeNight;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        mCombTrigger.getItems().addAll("15分钟触发器-1", "5分钟触发器-2");
        mCombTrigger.setValue("15分钟触发器-1");
        mCombStrategys.getItems().addAll("基础策略-1", "吊灯止损-2", "5分钟MA吊灯止损-3", "5分钟进阶分析策略-4");
        mCombStrategys.setValue("基础策略-1");
        mCombSimulateId.getItems().addAll("开发测试-1", "单日测试-2", "周期测试-3", "近日测试-4", "收益率测试-5", "生产模拟-800");
        mCombSimulateId.setValue("开发测试-1");
        mCombDataAccount.getItems().addAll("18616896212-1", "15300455238-2", "13817383985-3", "15618365471-4", "13046660586-5");
        mCombDataAccount.setValue("18616896212-1");
    }

    @FXML
    public void onButtonClick(ActionEvent event) throws Exception {
        SingletonDataAccount.getInstance().DataAccountChange(Integer.valueOf(mCombDataAccount.getValue().toString().split("-")[1]));
        Button CurrentBtn = (Button) event.getSource();
        if (CurrentBtn == mBtnStartRealTimeTrade) {
            int SimulateId = Integer.valueOf(mCombSimulateId.getValue().toString().split("-")[1]);
            int StrategyId = Integer.valueOf(mCombStrategys.getValue().toString().split("-")[1]);
            int TriggerId = Integer.valueOf(mCombTrigger.getValue().toString().split("-")[1]);

            Map param = new HashMap();
            param.put("SimulateId", SimulateId);
            param.put("StrategyId", StrategyId);
            param.put("TriggerId", TriggerId);

            //盘前任务
            schedulerBeforeOpen = StdSchedulerFactory.getDefaultScheduler();
            Trigger triggerBeforeOpen = newTrigger()
                    .withIdentity("triggerBeforOpen", "triggergroup")
                    .withSchedule(cronSchedule("0 30 8 ? * 2,3,4,5,6 *"))
                    .build();
            JobDetail jobBeforeOpen = JobBuilder.newJob(BeforeOpenJob.class)
                    .withIdentity("jobBeforOpen", "jobgroup").build();
            jobBeforeOpen.getJobDataMap().put("data", param);
            schedulerBeforeOpen.scheduleJob(jobBeforeOpen, triggerBeforeOpen);
            schedulerBeforeOpen.start();
            //开盘任务
            schedulerOpen = StdSchedulerFactory.getDefaultScheduler();
            Trigger triggerOpen = newTrigger()
                    .withIdentity("triggerOpen", "triggergroup")
                    .withSchedule(cronSchedule("3 0 9 ? * 2,3,4,5,6 *"))
                    .build();
            JobDetail jobOpen = JobBuilder.newJob(OpenJob.class)
                    .withIdentity("jobOpen", "jobgroup").build();
            jobOpen.getJobDataMap().put("data", param);
            schedulerOpen.scheduleJob(jobOpen, triggerOpen);
            schedulerOpen.start();

            //交易任务
            schedulerTradeTime = StdSchedulerFactory.getDefaultScheduler();
            Trigger triggerTradeTime = newTrigger()
                    .withIdentity("triggerTradeTime", "triggergroup")
                    .withSchedule(cronSchedule("5 5,10,15,20,25,30,35,40,45,50,55 0,1,2,9,10,11,13,14,21,22,23 ? * 2,3,4,5,6,7 *"))
                    .build();
            JobDetail jobTradeTime = JobBuilder.newJob(TradeTimeJob.class)
                    .withIdentity("jobTradeTime", "jobgroup").build();
            jobTradeTime.getJobDataMap().put("data", param);
            schedulerTradeTime.scheduleJob(jobTradeTime, triggerTradeTime);
            schedulerTradeTime.start();

            //夜盘前任务
            schedulerBeforeNight = StdSchedulerFactory.getDefaultScheduler();
            Trigger triggerBeforeNight = newTrigger()
                    .withIdentity("triggerBeforNight", "triggergroup")
                    .withSchedule(cronSchedule("0 30 20 ? * 2,3,4,5,6 *"))
                    .build();
            JobDetail jobBeforeNight = JobBuilder.newJob(BeforeNightJob.class)
                    .withIdentity("jobBeforNight", "jobgroup").build();
            jobBeforeNight.getJobDataMap().put("data", param);
            schedulerBeforeNight.scheduleJob(jobBeforeNight, triggerBeforeNight);
            schedulerBeforeNight.start();


        }
        if (CurrentBtn == mBtnStopRealTimeTrade) {
            schedulerBeforeOpen.shutdown();
            schedulerOpen.shutdown();
            schedulerTradeTime.shutdown();
            schedulerBeforeNight.shutdown();
        }

    }
}
