package com.skyworker.futuresquant.controller;

import cn.hutool.core.date.DateField;
import cn.hutool.core.date.DateTime;
import cn.hutool.core.date.DateUnit;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.io.FileUtil;
import cn.hutool.core.io.file.FileReader;
import cn.hutool.core.io.file.FileWriter;
import cn.hutool.db.sql.SqlExecutor;
import cn.hutool.log.Log;
import com.skyworker.futuresquant.quant.GetFinanceData;
import com.skyworker.futuresquant.singlerun.SingleRunRouter;
import com.skyworker.futuresquant.trigger.Trigger15m;
import com.skyworker.futuresquant.trigger.Trigger5m;
import com.skyworker.futuresquant.trigger.Trigger5mCloseInDay;
import com.skyworker.futuresquant.utils.SingletonConn;
import com.skyworker.futuresquant.utils.SingletonDataAccount;
import com.skyworker.futuresquant.utils.SingletonLog;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.stage.Stage;

import java.net.URL;
import java.sql.Connection;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class MainController implements Initializable {
    private static final Log log = SingletonLog.getInstance().log;
    private static final Connection conn = SingletonConn.getInstance().conn;
    @FXML
    private Button mBtnGetTradeDay;
    @FXML
    private Button mBtnSimulateTrade;
    @FXML
    private Button mBtnOpenRealTimeTrade;
    @FXML
    private Button mBtnClearData;
    @FXML
    private Button mBtnClearLog;
    @FXML
    private Button mBtnClearProfitFile;
    @FXML
    private Button mBtnGetDataAccount;
    @FXML
    private Button mBtnSingleTest;
    @FXML
    private DatePicker mDPSimulateTrade;
    @FXML
    private DatePicker mDPSimulateTradeEnd;
    @FXML
    private ComboBox<String> mCombSimulateId;
    @FXML
    private ComboBox<String> mCombStrategys;
    @FXML
    private ComboBox<String> mCombTrigger;
    @FXML
    private ComboBox<String> mCombDataAccount;
    @FXML
    private TextArea mTextLog;
    @FXML
    private TextField mTextVariety;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        mCombTrigger.getItems().addAll("15分钟触发器-1", "5分钟触发器-2", "5分钟触发器日内平仓-3");
        mCombTrigger.setValue("5分钟触发器-2");
        mCombStrategys.getItems().addAll("基础策略-1", "吊灯止损-2", "5分钟MA吊灯止损-3", "5分钟进阶分析策略-4", "MA和MACD双策略-5", "MA和MACD双策略日内平仓-6");
        mCombStrategys.setValue("MA和MACD双策略-5");
        mCombSimulateId.getItems().addAll("22年1月回测-1", "21年12月到22年1月回测-2", "21年10月到22年1月回测-3"
                , "22年1月修改后回测-4", "21年12月到22年1月修改回测-5", "修改对比参考-6", "修改后回测-7", "生产模拟-800"
                , "单一品种测试1-901", "单一品种测试2-902", "单一品种测试3-903", "单一品种测试4-904", "单一品种测试5-905"
                , "单一品种测试6-906", "单一品种测试7-907", "单一品种测试8-908");
        mCombSimulateId.setValue("22年1月回测-1");
        mCombDataAccount.getItems().addAll("18616896212-1", "15300455238-2", "13817383985-3", "15618365471-4", "13046660586-5");
        mCombDataAccount.setValue("15300455238-2");
    }

    @FXML
    public void onButtonClick(ActionEvent event) throws Exception {
        Button CurrentBtn = (Button) event.getSource();
        if (CurrentBtn == mBtnGetTradeDay) {
            runBtnGetTradeDay();
            return;
        }
        if (CurrentBtn == mBtnOpenRealTimeTrade) {
            runBtnOpenRealTimeTrade();
            return;
        }
        if (CurrentBtn == mBtnSimulateTrade) {
            runBtnSimulateTrade();
            return;
        }
        if (CurrentBtn == mBtnClearData) {
            runBtnClearData();
            return;
        }
        if (CurrentBtn == mBtnClearLog) {
            runBtnClearLog();
            return;
        }
        if (CurrentBtn == mBtnClearProfitFile) {
            runBtnClearProfitFile();
            return;
        }
        if (CurrentBtn == mBtnGetDataAccount) {
            runBtnGetDataAccount();
            return;
        }

        if (CurrentBtn == mBtnSingleTest) {
            runSingleTest();
        }
    }

    private void runBtnGetTradeDay() {
        SingletonDataAccount.getInstance().DataAccountChange(Integer.parseInt(mCombDataAccount.getValue().split("-")[1]));
        GetFinanceData getFinanceData = new GetFinanceData();
        String[] tradeDays = getFinanceData.getTradeDays(DateUtil.parseDate("2022-01-01"), DateUtil.parseDate("2022-12-31"));
        try {
            for (String tradeDay : tradeDays)
                SqlExecutor.execute(conn, "insert into t_sys_tradeday values('" + tradeDay + "')");
        } catch (SQLException e) {
            log.error(e);
        }
    }

    private void runBtnGetDataAccount() {
        SingletonDataAccount.getInstance().DataAccountChange(Integer.parseInt(mCombDataAccount.getValue().split("-")[1]));
        GetFinanceData getFinanceData = new GetFinanceData();
        Alert alert = new Alert(Alert.AlertType.INFORMATION); // 创建一个消息对话框
        alert.setHeaderText("完成"); // 设置对话框的头部文本
        alert.setContentText("当前可用交易数量：" + getFinanceData.getAvailableCount());  // 设置对话框的内容文本
        alert.show(); // 显示对话框

    }

    private void runBtnOpenRealTimeTrade() throws Exception {
        Parent root = FXMLLoader.load(Objects.requireNonNull(getClass().getClassLoader().getResource("fxml/RealTimeTrade.fxml")));
        Stage RealTimeTradeWindow = new Stage();
        Scene scene = new Scene(root, 800, 600);
        RealTimeTradeWindow.setTitle("启动实时交易");
        RealTimeTradeWindow.setScene(scene);
        RealTimeTradeWindow.show();
    }

    private void runBtnSimulateTrade() {
        SingletonDataAccount.getInstance().DataAccountChange(Integer.parseInt(mCombDataAccount.getValue().split("-")[1]));
        ExecutorService singleThreadExecutor = Executors.newSingleThreadExecutor();
        Timer timer = new Timer();
        FileReader fileReader = new FileReader("C:\\logs\\info.log");

        timer.schedule(new TimerTask() {
            public void run() {
                //List<String> ls = fileReader.readLines();
                String fileContent = fileReader.readString();
                int length = fileContent.length();
                if (length > 4000) {
                    fileContent = fileContent.substring(length - 4000, length);
                    length = fileContent.length();
                    fileContent = fileContent.substring(fileContent.indexOf("\n") + 1, length);
                }
                mTextLog.setText(fileContent);
                mTextLog.setScrollTop(Double.MAX_VALUE);
            }
        }, 0, 6000);

        singleThreadExecutor.submit(() -> {
            long start = DateUtil.current();

            LocalDate ldt = mDPSimulateTrade.getValue();
            DateTime tradeDate = new DateTime(ldt.toString());
            DateTime tradeDate2 = null;
            if (mDPSimulateTradeEnd.getValue() != null) {
                LocalDate ldt2 = mDPSimulateTradeEnd.getValue();
                tradeDate2 = new DateTime(ldt2.toString());
            }
            int SimulateId = Integer.parseInt(mCombSimulateId.getValue().split("-")[1]);
            int StrategyId = Integer.parseInt(mCombStrategys.getValue().split("-")[1]);
            int TriggerId = Integer.parseInt(mCombTrigger.getValue().split("-")[1]);
            GetFinanceData getFinanceData = new GetFinanceData();
            switch (TriggerId) {
                case 1:
                    if (tradeDate2 == null) {
                        if (getFinanceData.isTradeDay(tradeDate)) {
                            Trigger15m trigger15m = new Trigger15m(tradeDate, SimulateId, StrategyId);
                            trigger15m.runTrigger15m();
                        }
                    } else {
                        long betweenDay = DateUtil.between(tradeDate, tradeDate2, DateUnit.DAY);
                        for (int i = 0; i <= betweenDay; i++) {
                            DateTime dtTemp = DateUtil.parseDate(tradeDate.toString());
                            dtTemp.offset(DateField.DAY_OF_YEAR, i);
                            if (getFinanceData.isTradeDay(dtTemp)) {
                                Trigger15m trigger15m = new Trigger15m(dtTemp, SimulateId, StrategyId);
                                trigger15m.runTrigger15m();
                            }
                        }
                    }
                    break;
                case 2:
                    if (tradeDate2 == null) {
                        if (getFinanceData.isTradeDay(tradeDate)) {
                            Trigger5m trigger5m = new Trigger5m(tradeDate, SimulateId, StrategyId);
                            trigger5m.runTrigger5m();
                        }
                    } else {
                        long betweenDay = DateUtil.between(tradeDate, tradeDate2, DateUnit.DAY);
                        for (int i = 0; i <= betweenDay; i++) {
                            DateTime dtTemp = DateUtil.parseDate(tradeDate.toString());
                            dtTemp.offset(DateField.DAY_OF_YEAR, i);
                            if (getFinanceData.isTradeDay(dtTemp)) {
                                Trigger5m trigger5m = new Trigger5m(dtTemp, SimulateId, StrategyId);
                                trigger5m.runTrigger5m();
                            }
                        }
                    }
                    break;
                case 3:
                    if (tradeDate2 == null) {
                        if (getFinanceData.isTradeDay(tradeDate)) {
                            Trigger5mCloseInDay trigger5mCloseInDay = new Trigger5mCloseInDay(tradeDate, SimulateId, StrategyId);
                            trigger5mCloseInDay.runTrigger5mCloseInDay();
                        }
                    } else {
                        long betweenDay = DateUtil.between(tradeDate, tradeDate2, DateUnit.DAY);
                        for (int i = 0; i <= betweenDay; i++) {
                            DateTime dtTemp = DateUtil.parseDate(tradeDate.toString());
                            dtTemp.offset(DateField.DAY_OF_YEAR, i);
                            if (getFinanceData.isTradeDay(dtTemp)) {
                                Trigger5mCloseInDay trigger5mCloseInDay = new Trigger5mCloseInDay(dtTemp, SimulateId, StrategyId);
                                trigger5mCloseInDay.runTrigger5mCloseInDay();
                            }
                        }
                    }
                    break;
            }
            long end = DateUtil.current();
            timer.cancel();
//            Alert alert = new Alert(Alert.AlertType.INFORMATION); // 创建一个消息对话框
//            alert.setHeaderText("完成"); // 设置对话框的头部文本
//            alert.setContentText("当前任务已完成，耗时" + (end - start) / 1000);  // 设置对话框的内容文本
//            alert.show(); // 显示对话框
        });
        //singleThreadExecutor.shutdown();
    }

    private void runBtnClearData() {
        int SimulateId = Integer.parseInt(mCombSimulateId.getValue().split("-")[1]);
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION); // 创建一个消息对话框
        alert.setHeaderText("确认"); // 设置对话框的头部文本
        alert.setContentText("是否清除" + mCombSimulateId.getValue() + "的数据");  // 设置对话框的内容文本
        Optional<ButtonType> buttonType = alert.showAndWait();

        if (buttonType.get().getButtonData().equals(ButtonBar.ButtonData.OK_DONE)) {
            try {
                SqlExecutor.execute(conn, "delete from t_futures_dominant where simulate_id=" + SimulateId);
                SqlExecutor.execute(conn, "delete from t_simulate_afstop where simulate_id=" + SimulateId);
                SqlExecutor.execute(conn, "delete from t_simulate_breaker where simulate_id=" + SimulateId);
                SqlExecutor.execute(conn, "delete from t_simulate_position where simulate_id=" + SimulateId);
                SqlExecutor.execute(conn, "delete from t_simulate_traderecord where simulate_id=" + SimulateId);
                SqlExecutor.execute(conn, "update t_simulate_main set total_amount=100000,available_amount=100000,occupy_amount=0 where id=" + SimulateId);

            } catch (SQLException e) {
                log.error(e);
            }
        }

    }

    private void runBtnClearLog() {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION); // 创建一个消息对话框
        alert.setHeaderText("确认"); // 设置对话框的头部文本
        alert.setContentText("是否清理info日志");  // 设置对话框的内容文本
        Optional<ButtonType> buttonType = alert.showAndWait();

        if (buttonType.get().getButtonData().equals(ButtonBar.ButtonData.OK_DONE)) {
            String filePath = "c://logs//info.log";
            FileWriter writer = new FileWriter(filePath);
            writer.write("");
            filePath = "c://logs//warn.log";
            writer = new FileWriter(filePath);
            writer.write("");
        }
    }

    private void runBtnClearProfitFile() {
        int SimulateId = Integer.parseInt(mCombSimulateId.getValue().split("-")[1]);
        String sDate = mDPSimulateTrade.getValue().toString();

        Alert alert = new Alert(Alert.AlertType.CONFIRMATION); // 创建一个消息对话框
        alert.setHeaderText("确认"); // 设置对话框的头部文本
        alert.setContentText("是否清理" + sDate + "记录的，" + mCombSimulateId.getValue() + "的收益计算文件");  // 设置对话框的内容文本
        Optional<ButtonType> buttonType = alert.showAndWait();

        if (buttonType.get().getButtonData().equals(ButtonBar.ButtonData.OK_DONE)) {
            String filePath = "c://logs//profit//" + sDate + "profit" + SimulateId + ".xlsx";
            FileUtil.del(filePath);
        }
    }

    private void runSingleTest() {
        SingletonDataAccount.getInstance().DataAccountChange(Integer.parseInt(mCombDataAccount.getValue().split("-")[1]));
        ExecutorService singleThreadExecutor = Executors.newSingleThreadExecutor();
        Timer timer = new Timer();
        FileReader fileReader = new FileReader("C:\\logs\\info.log");

        timer.schedule(new TimerTask() {
            public void run() {
                //List<String> ls = fileReader.readLines();
                String fileContent = fileReader.readString();
                int length = fileContent.length();
                if (length > 4000) {
                    fileContent = fileContent.substring(length - 4000, length);
                    length = fileContent.length();
                    fileContent = fileContent.substring(fileContent.indexOf("\n") + 1, length);
                }
                mTextLog.setText(fileContent);
                mTextLog.setScrollTop(Double.MAX_VALUE);
            }
        }, 0, 6000);

        singleThreadExecutor.submit(() -> {
            int SimulateId = Integer.parseInt(mCombSimulateId.getValue().split("-")[1]);
            GetFinanceData getFinanceData = new GetFinanceData();

            LocalDate ldt = mDPSimulateTrade.getValue();
            DateTime tradeDate = new DateTime(ldt.toString());
            DateTime tradeDate2 = null;
            if (mDPSimulateTradeEnd.getValue() != null) {
                LocalDate ldt2 = mDPSimulateTradeEnd.getValue();
                tradeDate2 = new DateTime(ldt2.toString());
            }

            if (tradeDate2 == null) {
                if (getFinanceData.isTradeDay(tradeDate)) {
                    SingleRunRouter singleRunRouter = new SingleRunRouter(SimulateId, mTextVariety.getText(), tradeDate.toString("yyyy-MM-dd"));
                    singleRunRouter.SingleRun();
                }
            } else {
                long betweenDay = DateUtil.between(tradeDate, tradeDate2, DateUnit.DAY);
                for (int i = 0; i <= betweenDay; i++) {
                    DateTime dtTemp = DateUtil.parseDate(tradeDate.toString());
                    dtTemp.offset(DateField.DAY_OF_YEAR, i);
                    if (getFinanceData.isTradeDay(dtTemp)) {
                        SingleRunRouter singleRunRouter = new SingleRunRouter(SimulateId, mTextVariety.getText(), dtTemp.toString("yyyy-MM-dd"));
                        singleRunRouter.SingleRun();
                    }
                }
            }
            timer.cancel();
        });

    }

}
