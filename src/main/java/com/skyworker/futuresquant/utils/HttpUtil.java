package com.skyworker.futuresquant.utils;

import cn.hutool.http.HttpRequest;

public class HttpUtil {
    public static String HttpUtilPost(String paramJsonString, String url) {
        String result = HttpRequest.post(url)
                .header("Content-Type", "application/json")
                .body(paramJsonString)
                .execute().body();
        return result;
    }

    public static String HttpUtilGet( String url) {
        String result = HttpRequest.get(url)
                .execute().body();
        return result;
    }



}
