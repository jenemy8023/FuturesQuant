package com.skyworker.futuresquant.utils;

import cn.hutool.core.date.DateTime;
import cn.hutool.core.date.DateUtil;
import cn.hutool.db.Entity;
import cn.hutool.db.handler.EntityListHandler;
import cn.hutool.db.sql.SqlExecutor;
import cn.hutool.log.Log;
import com.skyworker.futuresquant.quant.GetFinanceData;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.*;

public class QuantUtil {

    private static final Log log = SingletonLog.getInstance().log;
    private static final Connection conn = SingletonConn.getInstance().conn;

    /**
     * 将字符串的价格转换成 ArrayList<BigDecimal>
     *
     * @param Price
     * @param FieldName
     * @return
     */

    public static ArrayList<BigDecimal> getDataListDecimal(ArrayList<HashMap<String, String>> Price, String FieldName) {
        ArrayList<BigDecimal> ArrayReturn = new ArrayList<>();
        for (HashMap<String, String> hm : Price) {
            ArrayReturn.add(new BigDecimal(hm.get(FieldName)));
        }
        return ArrayReturn;
    }

    /**
     * 将字符串的价格转换成double[]
     *
     * @param Price
     * @param FieldName
     * @return
     */
    public static double[] getDataList(ArrayList<HashMap<String, String>> Price, String FieldName) {
        double[] dReturn = new double[Price.size()];
        for (int i = 0; i < Price.size(); i++) {
            dReturn[i] = Double.parseDouble(Price.get(i).get(FieldName));
        }
        return dReturn;
    }

    public static double[] getDataList(ArrayList<HashMap<String, String>> Price, String FieldName, int Count) {
        double[] dReturn = new double[Price.size()];
        for (int i = 0; i < Count - 1; i++) {
            dReturn[i] = Double.parseDouble(Price.get(i).get(FieldName));
        }
        return dReturn;
    }

    public static String getCode(String dominant_code) {
        String temp = dominant_code.split("[.]")[0];
        return temp.substring(0, temp.length() - 4);
    }

    public static boolean isTradeDay(DateTime date) {
        try {
            String sDate = date.toString("yyyy-MM-dd");
            List<Entity> positions = SqlExecutor.query(conn, "select trade_day from t_sys_tradeday where trade_day = '" + sDate + "'", new EntityListHandler());
            if (positions.size() == 0)
                return false;
            else
                return true;
        } catch (SQLException e) {
            log.error(e);
            return false;
        }
    }

    public static boolean isTradeTime(DateTime date, String dominant_code) {
        int hour = date.hour(true);
        int minute = date.minute();
        int night_time = 0;
        String code = getCode(dominant_code);

        try {
            List<Entity> varieties = SqlExecutor.query(conn, "select * from t_futures_varieties where code='" + code + "'", new EntityListHandler());
            night_time = varieties.get(0).getInt("night_time");
        } catch (SQLException e) {
            log.error(e);
            return false;
        }

        if (!isTradeDay(date)) {
            if (!isTradeDay(DateUtil.offsetDay(date, -1))) {
                return false;
            } else if (hour > 2) {
                return false;
            } else if (hour == 2 & minute >= 30) {
                return false;
            } else if (hour == 2 & minute < 30 | hour == 1) {
                if (night_time == 230)
                    return true;
                else
                    return false;
            } else if (hour == 0) {
                if (night_time < 230)
                    return true;
                else
                    return false;
            }
        }

        if (hour == 0) {
            if (night_time < 230)
                return true;
            else
                return false;
        }

        if (hour == 2 & minute < 30 | hour == 1) {
            if (night_time == 230)
                return true;
            else
                return false;
        }

        if (hour == 9)
            return true;
        else if (hour == 10 & minute < 15)
            return true;
        else if (hour == 10 & minute >= 30)
            return true;
        else if (hour == 11 & minute < 30)
            return true;
        else if (hour == 13 & minute >= 30)
            return true;
        else if (hour == 14)
            return true;

        if (hour > 15 & night_time == 0)
            return false;

        if (hour == 21 | hour == 22)
            return true;

        if (hour == 23 & minute < 30 & (night_time == 2330 | night_time < 230))
            return true;

        if (hour == 23 & minute >= 30 & night_time < 230)
            return true;

        return false;
    }

    public static void updateTradeDays(String startDate, String endDate) {
        GetFinanceData getFinanceData = new GetFinanceData();
        String[] tradeDays = getFinanceData.getTradeDays(DateUtil.parseDate(startDate), DateUtil.parseDate(endDate));
        try {
            for (String tradeDay : tradeDays)
                SqlExecutor.execute(conn, "insert into t_sys_tradeday values('" + tradeDay + "')");

        } catch (SQLException e) {
            log.error(e);
        }
    }

    public static void updateTradeDays(String endDate) {
        try {
            List<Entity> le = SqlExecutor.query(conn, "select trade_day t_sys_tradeday order by trade_day desc limit 1", new EntityListHandler());
            updateTradeDays(le.get(0).getStr("trade_day"), endDate);
        } catch (SQLException e) {
            log.error(e);
        }
    }

    public static void updateTradeDays() {
        updateTradeDays(DateUtil.today());
    }

    public static double[] MaxMin(double[] values) {
        double max = values[0];
        double min = values[0];
        for (int i = 1; i < values.length; i++) {
            if (values[i] > max) max = values[i];
            if (values[i] < min) min = values[i];
        }
        return new double[]{max, min};
    }

    public static void exeCmd(String commandStr) {
        BufferedReader br = null;
        try {
            Process p = Runtime.getRuntime().exec(commandStr);
            br = new BufferedReader(new InputStreamReader(p.getInputStream()));
            String line = null;
            StringBuilder sb = new StringBuilder();
            while ((line = br.readLine()) != null) {
                sb.append(line + "\n");
            }
            log.info(sb.toString());
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public static ArrayList<HashMap<String, String>> LE2AH(List<Entity> le) {
        ArrayList<HashMap<String, String>> ah = new ArrayList<>();
        for (Entity e : le) {
            HashMap<String, String> hm = new HashMap<>();
            Iterator it = e.entrySet().iterator();
            while (it.hasNext()) {
                Map.Entry entry = (Map.Entry) it.next();
                hm.put(entry.getKey().toString(), entry.getValue().toString());
            }
            ah.add(hm);
        }
        return ah;
    }
}
