package com.skyworker.futuresquant.utils;

import cn.hutool.db.ds.DSFactory;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;

public class SingletonConn {
    private static SingletonConn singletonConn = new SingletonConn();
    public Connection conn;
    private DataSource ds;

    private SingletonConn() {
        ds = DSFactory.get();
        try {
            conn = ds.getConnection();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static SingletonConn getInstance() {
        return singletonConn;
    }
}
