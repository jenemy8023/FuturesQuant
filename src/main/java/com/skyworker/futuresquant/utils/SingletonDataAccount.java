package com.skyworker.futuresquant.utils;

import cn.hutool.db.Entity;
import cn.hutool.db.handler.EntityListHandler;
import cn.hutool.db.sql.SqlExecutor;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

public class SingletonDataAccount {
    private static final Connection conn = SingletonConn.getInstance().conn;
    private static SingletonDataAccount singletonDataAccount = new SingletonDataAccount();
    public String username;
    public String password;

    private SingletonDataAccount() {
        try {
            List<Entity> le = SqlExecutor.query(conn, "select * from t_sys_dict where type_code='jq_info' and key_name='jq_username' order by key_name", new EntityListHandler());
            Entity entity = le.get(0);
            username = entity.getStr("key_code");
            password = entity.getStr("value_code");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static SingletonDataAccount getInstance() {
        return singletonDataAccount;
    }

    public void DataAccountChange(int i) {
        try {
            List<Entity> le = SqlExecutor.query(conn, "select * from t_sys_dict where type_code='jq_info' order by key_name", new EntityListHandler());
            Entity entity = le.get(i - 1);
            username = entity.getStr("key_code");
            password = entity.getStr("value_code");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
