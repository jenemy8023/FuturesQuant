package com.skyworker.futuresquant.utils;

import com.tictactec.ta.lib.Core;
import com.tictactec.ta.lib.MInteger;

import java.util.ArrayList;
import java.util.HashMap;

public class IndicatorsUtil {
    Core core;

    public IndicatorsUtil() {
        core = new Core();
    }

    public double[] MA_talib(double[] data, int period) {
        double[] ma = new double[data.length];
        core.sma(0, data.length - 1, data, period, new MInteger(), new MInteger(), ma);
        return ma;
    }

    public double[] EMA_talib(double[] data, int period) {
        double[] ema = new double[data.length];
        core.ema(0, data.length - 1, data, period, new MInteger(), new MInteger(), ema);
        return ema;
    }

    public double[] MA(double[] data, int period) {
        return MA(data, period, data.length);
    }

    public double[] MA(double[] data, int period, int count) {
        double[] result = new double[count];
        double sum = 0;
        for (int i = 0; i < result.length; i++) {
            if ((i + period) > data.length)
                result[i] = 0;
            else {
                sum = 0;
                for (int j = 0; j < period; j++) {
                    sum = sum + data[i + j];
                }
                result[i] = sum / period;
            }
        }
        return result;
    }

    public double[] SMA(double[] data, int period, int weight) {
        double[] sma = new double[data.length];
        double LastSma = 0;
        double ThisSma = 0;
        for (int i = data.length - 1; i >= 0; i--) {
            if (LastSma == 0) {
                ThisSma = data[i];
            } else {
                ThisSma = (data[i] * weight + (period - weight) * LastSma) / period;
            }
            sma[i] = ThisSma;
            LastSma = ThisSma;
        }
        return sma;
    }

    public double[] EMA(double[] data, int period) {
        double[] ema = new double[data.length];
        double LastEma = 0;
        double ThisEma = 0;
        for (int i = data.length - 1; i >= 0; i--) {
            if (LastEma == 0) {
                ThisEma = data[i];
            } else {
                ThisEma = (data[i] * 2 + (period - 1) * LastEma) / (period + 1);
            }
            ema[i] = ThisEma;
            LastEma = ThisEma;
        }
        return ema;
    }

    public ArrayList<double[]> MACD(double[] close, int FastPeriod, int SlowPeriod, int SignalPeriod) {
        double[] EMAFast = EMA(close, FastPeriod);
        double[] EMASlow = EMA(close, SlowPeriod);
        double[] Diff = new double[EMAFast.length];
        int i;
        for (i = 0; i < EMAFast.length; i++) {
            Diff[i] = EMAFast[i] - EMASlow[i];
        }
        double[] Dea = EMA(Diff, SignalPeriod);
        double[] MACD = new double[Diff.length];
        for (i = 0; i < Diff.length; i++) {
            MACD[i] = 2 * (Diff[i] - Dea[i]);
        }
        ArrayList<double[]> aReturn = new ArrayList<>();
        aReturn.add(Diff);
        aReturn.add(Dea);
        aReturn.add(MACD);
        return aReturn;
    }

    private double[] TR(ArrayList<HashMap<String, String>> price) {
        //TR : MAX(MAX((HIGH-LOW),ABS(REF(CLOSE,1)-HIGH)),ABS(REF(CLOSE,1)-LOW));
        double[] close = QuantUtil.getDataList(price, "close");
        double[] high = QuantUtil.getDataList(price, "high");
        double[] low = QuantUtil.getDataList(price, "low");
        double[] tr = new double[close.length];
        for (int i = 0; i < close.length; i++) {
            if (i + 1 < close.length) {
                double max1 = high[i] - low[i];
                double max2 = Math.abs(close[i + 1] - high[i]);
                double max3 = Math.abs(close[i + 1] - low[i]);
                tr[i] = Math.max(Math.max(max1, max2), max3);
            } else {
                tr[i] = 0.0;
            }
        }
        return tr;
    }

    public double[] ATR(ArrayList<HashMap<String, String>> price, int period) {
        double[] tr = TR(price);
        return MA(tr, period);
    }

    public ArrayList<double[]> DMI(ArrayList<HashMap<String, String>> price, int period) {
        double[] close = QuantUtil.getDataList(price, "close");
        double[] high = QuantUtil.getDataList(price, "high");
        double[] low = QuantUtil.getDataList(price, "low");
        double[] tr = EMA(TR(price), period);
        double[] HD = new double[close.length];
        double[] LD = new double[close.length];
        int i;
        for (i = 0; i < close.length; i++) {
            if (i + 1 < close.length) {
                HD[i] = high[i] - high[i + 1];
                LD[i] = low[i + 1] - low[i];
            } else {
                HD[i] = 0;
                LD[i] = 0;
            }
        }
        double[] MP = new double[close.length];
        double[] MM = new double[close.length];
        for (i = 0; i < close.length; i++) {
            MP[i] = (HD[i] > 0 & HD[i] > LD[i]) ? HD[i] : 0;
            MM[i] = (LD[i] > 0 & LD[i] > HD[i]) ? LD[i] : 0;
        }
        double[] DMP = EMA(MP, period);
        double[] DMM = EMA(MM, period);

        double[] PDI = new double[close.length];
        double[] MDI = new double[close.length];
        double[] DX = new double[close.length];
        for (i = 0; i < close.length; i++) {
            PDI[i] = DMP[i] * 100 / tr[i];
            MDI[i] = DMM[i] * 100 / tr[i];
            DX[i] = Math.abs(MDI[i] - PDI[i]) / (MDI[i] + PDI[i]) * 100;
        }

        double[] ADX = EMA(DX, period);
        double[] ADXR = EMA(ADX, period);

        ArrayList<double[]> dmi = new ArrayList<>();
        dmi.add(PDI);
        dmi.add(MDI);
        dmi.add(ADX);
        dmi.add(ADXR);
        return dmi;
        //       TR:=EMA(MAX(MAX(HIGH-LOW,ABS(HIGH-REF(CLOSE,1))),ABS(REF(CLOSE,1)-LOW)),7);
//    　　HD :=HIGH-REF(HIGH,1);
//    　　LD :=REF(LOW,1)-LOW;
//    　　DMP:=EMA(IF(HD>0 AND HD>LD,HD,0),7);
//    　　DMM:=EMA(IF(LD>0 AND LD>HD,LD,0),7);
//    　　PDI: DMP*100/TR;
//    　　MDI: DMM*100/TR;
//    　　ADX: EMA(ABS(MDI-PDI)/(MDI+PDI)*100,7);
//    　　ADXR:EMA(ADX,7);
    }
}
