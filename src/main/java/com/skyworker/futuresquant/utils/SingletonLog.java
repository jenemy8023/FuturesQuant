package com.skyworker.futuresquant.utils;

import cn.hutool.log.Log;
import cn.hutool.log.LogFactory;

public class SingletonLog {
    private static SingletonLog singletonLog = new SingletonLog();
    public Log log;

    private SingletonLog() {
        log = LogFactory.get();
    }

    public static SingletonLog getInstance() {
        return singletonLog;
    }

}